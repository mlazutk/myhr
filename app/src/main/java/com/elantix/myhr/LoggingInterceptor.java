package com.elantix.myhr;


import com.google.gson.Gson;

import java.io.IOException;
import java.net.SocketTimeoutException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

import com.elantix.myhr.beenResponses.ErrorResponse;

/**
 * Created by misha on 25.01.2016.
 */
public class LoggingInterceptor implements Interceptor {
    Gson mGson = new Gson();

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Response response;

        try {
            response = chain.proceed(request);
            L.i("response.code = " + response.code());
            L.i("response.message() = " + response.message());

            if (!response.isSuccessful()) {
                //response = trackError(request,response);
            } else {
                L.i("response = " + response.body().string());
            }
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            L.i("SocketTimeoutException ... ");
            trackError(request, "Timeout", e.getMessage());
            throw e;
        } catch (IOException e) {
            trackError(request, "Device Connection Failure", e.getMessage());
            throw e;
        }
        return response;
    }

    private Response trackError(Request request, Response response) throws IOException {
        L.i("response = " + response);
        //L.i("response.body() = "+response.body());
        String responseBodyString = response.body().string();
        String cause = String.valueOf(response.code());
        L.i("cause = " + cause);
        String errorMessage = "errorMessage is empty";
        final Response newResponse;

        try {
            ErrorResponse errorResponse = mGson.fromJson(responseBodyString, ErrorResponse.class);
            errorMessage = errorResponse.getErrorMessage();
        } catch (NullPointerException e) {
            e.printStackTrace();
            errorMessage = "Unknown error";
        } finally {
            ResponseBody body = ResponseBody.create(response.body().contentType(), responseBodyString);
            newResponse = response.newBuilder()
                    .body(body)
                    .build();
        }
        trackError(request, cause, errorMessage);
        return newResponse;
    }

    private void trackError(Request request, String cause, String message) {
        String url = request.url().toString();
        String method = request.method();
        L.i("url =  " + url);
        L.i("method =  " + method);
        // Do something with url, method, cause and message
    }
}