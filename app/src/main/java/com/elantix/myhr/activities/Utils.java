package com.elantix.myhr.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.support.v7.app.AlertDialog;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import com.elantix.myhr.Configs;
import com.elantix.myhr.L;
import com.elantix.myhr.R;
import com.elantix.myhr.classes.MyHRPreferences;
import com.elantix.myhr.retrofitApiServices.MyPosApiInterface;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by misha on 16.02.2016.
 */
public class Utils {
    public static final String EVENT_FINISHED = "finished";
    public static final String EVENT_PROGRESS = "progress";
    public static final String EVENT_COLOR_GREEN = "green";

    public static final String IDENTIFICATOR_TICK = "tick";
    public static final String IDENTIFICATOR_MEETING = "meeting";
    public static final String IDENTIFICATOR_BREAK_TIME = "breaktime";

    public static final String EVENT_PROGRESS_BREAK = "progress";
    public static final String EVENT_DONE_BREAK = "done";
    public static final String EVENT_COLOR_RED = "red";
    public static final String EVENT_COLOR_ORANGE = "orange";
    public static final String EVENT_COLOR_PURPLE = "purple";

    public static final String TITLE_START_DAY = "Start Work Day";
    public static final String TITLE_END_DAY = "End Work Day";
    public static final String TITLE_BREEK = "Break Time";
    //    public static final String TITLE_START_MEETING = "Start Meeting";
//    public static final String TITLE_END_MEETING = "End Meeting";
    public static final String TITLE_MEETING = "eMeeting";

    public String cutQuotaBallance(Context context, float ballance) {
        String strinQuota_ballance[] = String.valueOf(ballance).split("\\.");
        if (strinQuota_ballance[1].equals("0")) {
            return strinQuota_ballance[0];
        } else {
            return String.valueOf(ballance);
        }
    }

    public static MyPosApiInterface getMyPosService() {
        // try {
        OkHttpClient okClient = new OkHttpClient.Builder().build();//addInterceptor(new LoggingInterceptor()).
        Retrofit client = new Retrofit.Builder()
                .baseUrl(Configs.baseUrl)
                .client(okClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        MyPosApiInterface service = client.create(MyPosApiInterface.class);

//        }catch (NoClassDefFoundError ex){
//            ex.printStackTrace();
//            return service;
//        }
        return service;
    }

    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static boolean isNewDayToday(Context context) {
        String lastDate = MyHRPreferences.getInstance(context).getDate();
        SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
        String formatedString = myFormat.format(new Date());
        boolean isNewDay = !formatedString.equalsIgnoreCase(lastDate);
        return isNewDay;
    }

    public static String getCurrDate() {
        SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
        String formatedString = myFormat.format(new Date());
        return formatedString;
    }

    public static String getCompleteAddressString(Context context, double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append(", ");
                }
                strAdd = strReturnedAddress.toString();
            } else {
                L.i("My Current loction address: No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strAdd;
    }

    public static boolean isEmpty(String string) {
        if (string != null) {
            if ((string.length() > 0) && !"null".equalsIgnoreCase(string)) {
                return false;
            } else return true;
        } else return true;
    }

    public static String[] getAMCurrentTime() {
        String delegate = "hh:mm aaa";
        String time = (String) DateFormat.format(delegate, Calendar.getInstance().getTime());
        String splitted[] = time.split("\\s");
        return splitted;
    }

    public static String getAMPMCurrentTime() {
        String delegate = "hh:mm ";
        return (String) DateFormat.format(delegate, Calendar.getInstance().getTime());
//
//        Calendar now = Calendar.getInstance();
//        now.setTime(new Date());
//
//        int a = now.get(Calendar.AM_PM);
//        if(a == Calendar.AM)
//            System.out.println("AM"+now.get(Calendar.HOUR));
//        else
//            System.out.println("PM"+now.get(Calendar.HOUR));
//    }
    }

    // input must hav format "yyyy-MM-dd HH:mm:ss"
//    public static String convertAMPM(String input) {
//        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        SimpleDateFormat outputFormat = new SimpleDateFormat("KK:mm a");
//        String output = "";
//        try {
//            output = outputFormat.format(inputFormat.parse(input));
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//
//        return output;
//    }

    public static String convertAMPM(String date) {
        if (date == null) return "";
        String splitedDate[] = date.split("\\s");
        String splited[] = splitedDate[1].split(":");
        String hour = splited[0];
        String minutes = splited[1];
        if ((hour.length() == 2) && ((hour.substring(0, 1)).equals("0")))
            hour = hour.substring(1, 2);
        if ((minutes.length() == 2) && ((minutes.substring(0, 1)).equals("0")))
            minutes = minutes.substring(1, 2);

        Calendar mCalen = Calendar.getInstance();

        mCalen.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hour));
        mCalen.set(Calendar.MINUTE, Integer.parseInt(minutes));

        int hour12format_local = mCalen.get(Calendar.HOUR);
        int hourOfDay_local = mCalen.get(Calendar.HOUR_OF_DAY);
        int minute_local = mCalen.get(Calendar.MINUTE);
        int ampm = mCalen.get(Calendar.AM_PM);
        String minute1;
        if (minute_local < 10) {

            minute1 = "0" + minute_local;
        } else
            minute1 = "" + minute_local;

        String ampmStr = (ampm == 0) ? "AM" : "PM";
        // Set the Time String in Button

        if (hour12format_local == 0)
            hour12format_local = 12;


        String selecteTime = hour12format_local + ":" + minute1 + " " + ampmStr;

        return selecteTime;
    }

    public static void showNotificationDialog(Context context,String message,int number_mess ) {
        if (context == null) return;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(R.layout.custom_dialog_notification);
//        builder.setMessage("");
        builder.setCancelable(true);

        final AlertDialog dialog = builder.create();
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0x00ffffff));

        TextView editText = (TextView) dialog.findViewById(R.id.back);
        TextView textViewMess = (TextView) dialog.findViewById(R.id.texyview_body);
        TextView textViewSubMess = (TextView) dialog.findViewById(R.id.texyview_sub_body);
        if(number_mess==1){
            textViewSubMess.setText("Check Name And Try ");
        }else if(number_mess==2){
            textViewSubMess.setText("Check Amount And Try ");
        }else if(number_mess==3){
            textViewSubMess.setText("Check Description And Try ");
        }
        textViewMess.setText(message);
        editText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });//
    }

    public static void showNotificationBreakDialog(Context context ) {
        if (context == null) return;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(R.layout.custom_break_time_dialog);
        builder.setMessage("");
        builder.setCancelable(true);

        final AlertDialog dialog = builder.create();
        dialog.show();
        TextView endBreak = (TextView) dialog.findViewById(R.id.tv_end_break_time);
        TextView cancel = (TextView) dialog.findViewById(R.id.tv_cancel_end_break_time);

        //textViewMess.setText(message);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        endBreak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

//
    }

    public static String theMonth(int month){
         String s = String.valueOf(month);
         //String s = "08";

        if(s.length()==2){
            if(s.substring(0,1).equalsIgnoreCase("0")){
                s=s.substring(1,2);
                L.i("s = "+s);
            }
        }
        final int month_final = Integer.parseInt(s);
        String[] monthNames = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"};
        return monthNames[month_final-1];
    }

}
