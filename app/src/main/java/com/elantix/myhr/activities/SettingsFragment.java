package com.elantix.myhr.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.elantix.myhr.R;
import com.elantix.myhr.beenResponses.AuthorizedUser;
import com.elantix.myhr.classes.MyHRPreferences;

/**
 * Created by misha on 13.02.2016.
 */
public class SettingsFragment extends Fragment {
    private String mStringUserId;
    private AuthorizedUser.User_info mAuthorizedUserInfo;
    private TextView mTextViewSettingName;
    private TextView mTextViewSettingPosition;
    private TextView mTextViewSettingCompany;
    private TextView mTextViewSettingAddress;
    private TextView mTextViewSettingBranch;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.content_settings, null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAuthorizedUserInfo = MyHRPreferences.getInstance(getActivity()).getUser().getUser_info();
        mTextViewSettingName = (TextView) view.findViewById(R.id.tv_settings_name);
        mTextViewSettingPosition = (TextView) view.findViewById(R.id.tv_settings_position);
        mTextViewSettingCompany = (TextView) view.findViewById(R.id.tv_settings_company);
        mTextViewSettingAddress = (TextView) view.findViewById(R.id.tv_settings_address);
        mTextViewSettingBranch = (TextView) view.findViewById(R.id.tv_settings_branch);

        String name = mAuthorizedUserInfo.getName();
        String position = mAuthorizedUserInfo.getDesignation_name();
        String companyName = mAuthorizedUserInfo.getCompany_name();
        String address1 = mAuthorizedUserInfo.getAddress1();
        String branchName = mAuthorizedUserInfo.getBranch_name();
        mTextViewSettingName.setText(name);
        mTextViewSettingPosition.setText(position);
        mTextViewSettingCompany.setText(companyName);
        mTextViewSettingAddress.setText(address1);
        mTextViewSettingBranch.setText(branchName);
    }
}