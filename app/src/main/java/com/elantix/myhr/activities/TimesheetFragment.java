package com.elantix.myhr.activities;

/**
 * Created by misha on 13.02.2016.
 */

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.elantix.myhr.CallResponseBody;
import com.elantix.myhr.L;
import com.elantix.myhr.R;
import com.elantix.myhr.adapters.HistoryAdapter;
import com.elantix.myhr.beenResponses.BreektimeFinishResponce;
import com.elantix.myhr.beenResponses.MeetingResponce;
import com.elantix.myhr.beenResponses.MeetingTodayResponce;
import com.elantix.myhr.beenResponses.TickResponce;
import com.elantix.myhr.beenResponses.TickStatusResponce;
import com.elantix.myhr.beenResponses.TimeSheetTableResponce;
import com.elantix.myhr.classes.Data;
import com.elantix.myhr.classes.Event;
import com.elantix.myhr.classes.LockTimer;
import com.elantix.myhr.classes.MyHRPreferences;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;


import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class TimesheetFragment extends Fragment implements View.OnClickListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    private static TextView mTextViewPM;
    private static TextView mTextViewClockTime;
    public static Activity sInstance;
    private static String sUserId;

    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;

    public static String sLat;
    public static String sLng;
    public static String sStringLocation;
    public static String sStringDescr;
    public static String sStringDate;


    private static TextView mTextViewDateHeader;
    public static RecyclerView mRecyclerView;
    public static final int REQUEST_CODE_START_EVENT = 1;
    //public static DBhelper mDBhelper;
    public static HistoryAdapter sHistoryAdapter;
    public static int numbTick = 0;

    LinearLayoutManager mLayoutManagerMeeting;

    private static FloatingActionMenu mFloatingActionMenu;
    private static FloatingActionButton mFloatingActionStartWorkDay;
    private static FloatingActionButton mFloatingActionEndWorkDay;
    private static FloatingActionButton mFloatingActionButtonStartBreak;
    private static FloatingActionButton mFloatingActionButtonEndBreak;
    //mFloatingActionMenu.OnMenuToggleListener(new FloatingActionMenu.OnMenuToggleListener)
    //--------------------------------------


    @Override
    public void onResume() {
        super.onResume();
//        if (mFloatingActionMenu.isOpened()) {
//            mFloatingActionMenu.open(true);
//        }
        LockTimer.getInstance(getActivity()).reset();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LockTimer.getInstance(getActivity()).stop();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.meeting_timesheet_layout, null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        sInstance = getActivity();
        sUserId = MyHRPreferences.getInstance(sInstance).getUser().getUser_info().getEmployee_id();
        buildGoogleApiClient();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        } else
            Toast.makeText(sInstance, "Not connected...", Toast.LENGTH_SHORT).show();
        mFloatingActionMenu = (FloatingActionMenu) view.findViewById(R.id.menu);
        mFloatingActionStartWorkDay = (FloatingActionButton) view.findViewById(R.id.menu_item_start_work_day);
        mFloatingActionEndWorkDay = (FloatingActionButton) view.findViewById(R.id.menu_item_end_work_day);
        mFloatingActionButtonStartBreak = (FloatingActionButton) view.findViewById(R.id.menu_item_start_break);
        mFloatingActionButtonEndBreak = (FloatingActionButton) view.findViewById(R.id.menu_item_end_break);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.meetings_timesheet_recycler);

        mLayoutManagerMeeting = new LinearLayoutManager(sInstance);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(mLayoutManagerMeeting);
        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        // mFloatingActionMenu.close(false);
                        getActivity().startActivity(new Intent(getActivity(), MeetingMapActivity.class)
                                        .putExtra(MeetingMapActivity.POSITION_ADAPTER_EXTRA, position)
                                        .putExtra(MeetingMapActivity.WHAT_FRAGMENT, 1)
                        );
                    }
                })
        );

        // mFloatingActionMenu.setOnClickListener(this);
        mFloatingActionStartWorkDay.setOnClickListener(this);
        mFloatingActionEndWorkDay.setOnClickListener(this);
        mFloatingActionButtonStartBreak.setOnClickListener(this);
        mFloatingActionButtonEndBreak.setOnClickListener(this);
        mTextViewDateHeader = (TextView) view.findViewById(R.id.meeting_tv_title_data);
        //---------------------------------------------------------
        mTextViewPM = (TextView) view.findViewById(R.id.clock_pm_meeting);
        mTextViewClockTime = (TextView) view.findViewById(R.id.clock_time_meeting);
        Typeface robotoMediumTypeface = Typeface
                .createFromAsset(getActivity().getAssets(), "fonts/Roboto-Medium.ttf");
        Typeface robotoRegularTypeface = Typeface
                .createFromAsset(getActivity().getAssets(), "fonts/Roboto-Regular.ttf");
        mTextViewClockTime.setTypeface(robotoMediumTypeface);
        mTextViewPM.setTypeface(robotoRegularTypeface);
        setCurrentTimeHeader();
        sHistoryAdapter = new HistoryAdapter(new LinkedList<Event>(), getActivity());
        mFloatingActionMenu.close(false);

        setMenuEnabled(false);
        callTableTodayEvents();
    }

    @Override
    public void onPause() {
        super.onPause();
//        if (mFloatingActionMenu.isOpened()) {
//            mFloatingActionMenu.open(true);
//        }
    }

    @Override
    public void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    public static void timeSheetTableTodayCallback(String responseBody) {
        L.i("responseBody in timeSheetTableTodayCallback:" + responseBody);
        TimeSheetTableResponce timeSheetTableResponce = new Gson().fromJson(responseBody, TimeSheetTableResponce.class);
        L.i("timeSheetTableResponce after timeSheetTableTodayCallback:" + timeSheetTableResponce);
        mFloatingActionMenu.close(false);
        if (timeSheetTableResponce == null) {
            setMenuEnabled(false);
            return;
        }
        if (!timeSheetTableResponce.isStatus()) {
            callTickStatus();
            return;
        }
        mFloatingActionStartWorkDay.setVisibility(View.GONE);
        List<Data> dataList = timeSheetTableResponce.getData();
        if (dataList == null) return;
        List<Event> eventList = new ArrayList<>();
        Event event = null;
        Data data = null;
        Data adjustedData = null;
        for (int i = 0; i < dataList.size(); i++) {
            event = new Event();
            data = dataList.get(i);
            adjustedData = adjustData(data);
            event.setData(adjustedData);
            eventList.add(event);
        }
        numbTick = 0;
        setMenuVisibility(adjustedData);

        sHistoryAdapter.setmEventList(eventList);
        mRecyclerView.setAdapter(sHistoryAdapter);
        sHistoryAdapter.notifyDataSetChanged();
        mRecyclerView.scrollToPosition(eventList.size() - 1);

        if (StartEventMeetActivity.sInstance != null) {
            StartEventMeetActivity.sInstance.finish();
        }
    }


    public static void setMenuVisibility(Data data) {
        if (data == null) return;
        if (Utils.EVENT_COLOR_GREEN.equalsIgnoreCase(data.getColor())) {
            mFloatingActionStartWorkDay.setVisibility(View.GONE);
            mFloatingActionButtonStartBreak.setVisibility(View.VISIBLE);
            mFloatingActionButtonEndBreak.setVisibility(View.GONE);
            mFloatingActionEndWorkDay.setVisibility(View.VISIBLE);
        } else if (Utils.EVENT_COLOR_RED.equalsIgnoreCase(data.getColor())) {
            //  mFloatingActionStartWorkDay.setVisibility(View.VISIBLE);
            mFloatingActionButtonStartBreak.setVisibility(View.GONE);
            mFloatingActionButtonEndBreak.setVisibility(View.GONE);
            mFloatingActionEndWorkDay.setVisibility(View.GONE);
        } else if (Utils.EVENT_COLOR_ORANGE.equalsIgnoreCase(data.getColor())) {
            final String started_at = data.getStarted_at();
            final String finished_at = data.getFinished_at();

            if (!Utils.isEmpty(started_at)) {
                if (!Utils.isEmpty(finished_at)) {
                    // finished breaktime
                    mFloatingActionStartWorkDay.setVisibility(View.GONE);
                    mFloatingActionButtonStartBreak.setVisibility(View.VISIBLE);
                    mFloatingActionButtonEndBreak.setVisibility(View.GONE);
                    mFloatingActionEndWorkDay.setVisibility(View.VISIBLE);
                } else {
                    mFloatingActionStartWorkDay.setVisibility(View.GONE);
                    mFloatingActionButtonStartBreak.setVisibility(View.GONE);
                    mFloatingActionButtonEndBreak.setVisibility(View.VISIBLE);
                    mFloatingActionEndWorkDay.setVisibility(View.VISIBLE);
                }
            }
        } else if (Utils.EVENT_COLOR_PURPLE.equalsIgnoreCase(data.getColor())) {
            final String started_at = data.getStarted_at();
            final String finished_at = data.getFinished_at();
            if (!Utils.isEmpty(started_at)) {
                if (Utils.isEmpty(finished_at)) {
                    mFloatingActionStartWorkDay.setVisibility(View.GONE);
                    mFloatingActionButtonStartBreak.setVisibility(View.GONE);
                    mFloatingActionButtonEndBreak.setVisibility(View.GONE);
                    mFloatingActionEndWorkDay.setVisibility(View.GONE);
                } else {
                    mFloatingActionStartWorkDay.setVisibility(View.GONE);
                    mFloatingActionButtonStartBreak.setVisibility(View.VISIBLE);
                    mFloatingActionButtonEndBreak.setVisibility(View.GONE);
                    mFloatingActionEndWorkDay.setVisibility(View.VISIBLE);
                }
            }
        }
    }

//    public static void meetingTodayCallback(String responseBody) {
//        MeetingTodayResponce meetingsTodayResponce = new Gson().fromJson(responseBody, MeetingTodayResponce.class);
//        L.i("meetingTodayResponce.isStatus():" + meetingsTodayResponce.isStatus());
//        if ((meetingsTodayResponce != null) && (meetingsTodayResponce.isStatus())) {
//            Data arrayData[] = meetingsTodayResponce.getData();
//            Data lastData = arrayData[arrayData.length - 1];
//            if (Utils.EVENT_PROGRESS.equalsIgnoreCase(lastData.getStatus())) {
//                mFloatingActionStartWorkDay.setVisibility(View.GONE);
//                mFloatingActionButtonStartBreak.setVisibility(View.GONE);
//                mFloatingActionButtonEndBreak.setVisibility(View.GONE);
//                mFloatingActionEndWorkDay.setVisibility(View.GONE);
//            } else {
//                mFloatingActionStartWorkDay.setVisibility(View.GONE);
//                mFloatingActionButtonStartBreak.setVisibility(View.VISIBLE);
//                mFloatingActionButtonEndBreak.setVisibility(View.VISIBLE);
//                mFloatingActionEndWorkDay.setVisibility(View.VISIBLE);
//            }
//        }
//
//    }

    public static void breaktimeCallback(String responseBody) {
        L.i("breaktimeCallback responseBody: " + responseBody);

        callTableTodayEvents();
    }

    public static void tickCallback(String responseBody) {
        callTableTodayEvents();
    }

    public static void tickStatusCallback(String responseBody) {
        TickStatusResponce tickStatusResponce = new Gson().fromJson(responseBody, TickStatusResponce.class);
        if(tickStatusResponce==null) return;
        L.i("tickStatusResponce: " + tickStatusResponce.getData().getStatus());
        if (Utils.EVENT_PROGRESS.equalsIgnoreCase(tickStatusResponce.getData().getStatus())) {
            mFloatingActionStartWorkDay.setVisibility(View.GONE);
            mFloatingActionEndWorkDay.setVisibility(View.VISIBLE);
            mFloatingActionButtonStartBreak.setVisibility(View.GONE);
            mFloatingActionButtonEndBreak.setVisibility(View.GONE);
            //  mFloatingActionMenu.close(false);
        } else if (Utils.EVENT_FINISHED.equalsIgnoreCase(tickStatusResponce.getData().getStatus())) {
            mFloatingActionStartWorkDay.setVisibility(View.VISIBLE);
            mFloatingActionEndWorkDay.setVisibility(View.GONE);
            mFloatingActionButtonStartBreak.setVisibility(View.GONE);
            mFloatingActionButtonEndBreak.setVisibility(View.GONE);
            // mFloatingActionMenu.close(false);
        }
    }

    //=================================================================================================
    //=================================================================================================
    //=================================================================================================
    public static Data adjustData(Data data) {
        Data adjustData = data;
        String started_at = data.getStarted_at();
        String finished_at = data.getFinished_at();
        //String finished_at = data.getFinished_at();

        if (Utils.IDENTIFICATOR_TICK.equalsIgnoreCase(data.getIdentificator())) {
            numbTick = numbTick + 1;
            if ((numbTick & 1) == 1) {
                adjustData.setDatetime(started_at);
                adjustData.setColor(Utils.EVENT_COLOR_GREEN);
                adjustData.setTitle(Utils.TITLE_START_DAY);
                adjustData.setStatus(Utils.EVENT_PROGRESS);
            } else {
                adjustData.setDatetime(started_at);
                adjustData.setColor(Utils.EVENT_COLOR_RED);
                adjustData.setTitle(Utils.TITLE_END_DAY);
                adjustData.setStatus(Utils.EVENT_FINISHED);
            }

        } else if (Utils.IDENTIFICATOR_BREAK_TIME.equalsIgnoreCase(data.getIdentificator())) {
            if (!Utils.isEmpty(started_at)) {
                if (Utils.isEmpty(finished_at)) {
                    adjustData.setColor(Utils.EVENT_COLOR_ORANGE);
                    adjustData.setTitle(Utils.TITLE_BREEK);
                    adjustData.setStatus(Utils.EVENT_PROGRESS_BREAK);
                } else {
                    adjustData.setColor(Utils.EVENT_COLOR_ORANGE);
                    adjustData.setTitle(Utils.TITLE_BREEK);
                    adjustData.setStatus(Utils.EVENT_DONE_BREAK);
                }
            }

        } else if (Utils.IDENTIFICATOR_MEETING.equalsIgnoreCase(data.getIdentificator())) {
            if (!Utils.isEmpty(started_at)) {
                if (Utils.isEmpty(finished_at)) {
                    adjustData.setColor(Utils.EVENT_COLOR_PURPLE);
                    adjustData.setTitle(Utils.TITLE_MEETING);
                    adjustData.setStatus(Utils.EVENT_PROGRESS);
                } else {
                    adjustData.setColor(Utils.EVENT_COLOR_PURPLE);
                    adjustData.setTitle(Utils.TITLE_MEETING);
                    adjustData.setStatus(Utils.EVENT_FINISHED);
                }
            }
        }
        return adjustData;
    }

    public void showNotificationWorkDayDialog(Context context) {
        if (context == null) return;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(R.layout.custom_end_work_day_dialog);
        builder.setCancelable(true);
        final AlertDialog dialog = builder.create();
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0x00ffffff));
        TextView endDay = (TextView) dialog.findViewById(R.id.tv_end_break_time);
        TextView cancel = (TextView) dialog.findViewById(R.id.tv_cancel_end_break_time);
        //textViewMess.setText(message);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        endDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CallResponseBody callResponseBody = new CallResponseBody();
                Call<ResponseBody> eventCall = Utils.getMyPosService().tickEnd(sUserId, sLat, sLng, sStringLocation, "");
                callResponseBody.setClazz(TickResponce.class);
                eventCall.enqueue(callResponseBody);
                dialog.dismiss();
            }
        });

//
    }

    public static void showNotificationBreakDialog(Context context) {
        if (context == null) return;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(R.layout.custom_break_time_dialog);
        builder.setCancelable(true);
        final AlertDialog dialog = builder.create();
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0x00ffffff));
        TextView endBreak = (TextView) dialog.findViewById(R.id.tv_end_break_time);
        TextView cancel = (TextView) dialog.findViewById(R.id.tv_cancel_end_break_time);

        //textViewMess.setText(message);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        endBreak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CallResponseBody callResponseBody = new CallResponseBody();
                Call<ResponseBody> eventCall = Utils.getMyPosService().breaktimeFinish(sUserId, "finished");
                callResponseBody.setClazz(BreektimeFinishResponce.class);
                eventCall.enqueue(callResponseBody);
                dialog.dismiss();
            }
        });

//
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(getActivity(), StartEventMeetActivity.class);
        switch (v.getId()) {
            case R.id.menu_item_start_break:
                intent.putExtra(
                        StartEventMeetActivity.EVENT_EXTRA, StartEventMeetActivity.REQUEST_START_BREEK);
                startActivity(intent);
                break;
            case R.id.menu_item_end_break:

                showNotificationBreakDialog(sInstance);
                break;
            case R.id.menu_item_start_work_day:
                intent.putExtra(
                        StartEventMeetActivity.EVENT_EXTRA, StartEventMeetActivity.REQUEST_START_DAY);
                startActivity(intent);
                break;
            case R.id.menu_item_end_work_day:
                showNotificationWorkDayDialog(sInstance);
                break;
//            case R.id.menu:
////                mFloatingActionMenu.setEnabled(false);
//                //callTableTodayEvents();
//                break;

            default:
                break;
        }
    }

    public static void callTableTodayEvents() {
        Call<ResponseBody> timeSheetTableCall = Utils.getMyPosService().timeSheetTable(sUserId);
        timeSheetTableCall.enqueue(new retrofit2.Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                L.i("timeSheetTable : " + response);

                if (response == null) return;
                try {
                    // L.i("timeSheetTableResponce: " + response.body().string().toString());
                    timeSheetTableTodayCallback(response.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                setMenuEnabled(false);
            }
        });
    }

    public static void callTickStatus() {
        CallResponseBody callResponseBody = new CallResponseBody();
        Call<ResponseBody> eventCall = Utils.getMyPosService().tickStatus(MyHRPreferences.getInstance(sInstance).getUser().getData().getUserId());
        callResponseBody.setClazz(TickStatusResponce.class);
        eventCall.enqueue(callResponseBody);
    }

    public static void setCurrentTimeHeader() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, d MMM, yyyy", Locale.ENGLISH);
        String dateFormated = dateFormat.format(new Date());
        String formSplit[] = dateFormated.split(",");
        mTextViewDateHeader.setText(formSplit[0] + "," + formSplit[1]);
        String splitedTime[] = Utils.getAMCurrentTime();
        mTextViewClockTime.setText(splitedTime[0]);
        mTextViewPM.setText(splitedTime[1]);
    }

    public static void setMenuEnabled(boolean enable) {
        if (enable) {
        } else {
            mFloatingActionMenu.close(false);
            mFloatingActionStartWorkDay.setVisibility(View.GONE);
            mFloatingActionEndWorkDay.setVisibility(View.GONE);
            mFloatingActionButtonStartBreak.setVisibility(View.GONE);
            mFloatingActionButtonEndBreak.setVisibility(View.GONE);
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult arg0) {
        Toast.makeText(sInstance, "Failed to connect...", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onConnected(Bundle arg0) {
        if (ActivityCompat.checkSelfPermission(sInstance, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(sInstance, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);

        if (mLastLocation != null) {
            sLat = String.valueOf(mLastLocation.getLatitude());
            sLng = String.valueOf(mLastLocation.getLongitude());
            sStringLocation = Utils.getCompleteAddressString(sInstance, mLastLocation.getLatitude(), mLastLocation.getLongitude());

        } else L.i("mLastLocation = null");

    }

    @Override
    public void onConnectionSuspended(int arg0) {
        Toast.makeText(sInstance, "Connection suspended...", Toast.LENGTH_SHORT).show();
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(sInstance)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }
}