package com.elantix.myhr.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.support.v4.app.DialogFragment;

import com.elantix.myhr.CallResponseBody;
import com.elantix.myhr.beenResponses.LeaveHistoryResponce;
import com.google.gson.Gson;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import com.elantix.myhr.Configs;
import com.elantix.myhr.L;
import com.elantix.myhr.R;
import com.elantix.myhr.beenResponses.CreateLeaveResponse;
import com.elantix.myhr.beenResponses.LeaveTypesResponse;
import com.elantix.myhr.classes.MyHRPreferences;

import android.support.v4.app.Fragment;

import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class CreateLeaveActivity extends AppCompatActivity {
    public static String sStringStartDateToServer = "18/06/2016";
    public static String sStringEndDateToServer = "19/06/2016";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_leave_main);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }

    public static class PlaceholderFragment extends Fragment implements View.OnClickListener {
        public static FragmentActivity sInstance = null;
        public static final String LEAVE_POSITION_ITEM_EXTRA = "position_item_extra";
        public static final String LEAVE_JSON_EXTRA = "leave_json_extra";
        public static final int REQUEST_CODE_START_DATE = 1;
        public static final int REQUEST_CODE_END_DATE = 2;


        private static Gson mGson = new Gson();
        private String mStringTitleLeave;
        private String mStringTitlePaidUnpaid;
        private TextView mTextViewTitleLeave;
        private TextView mTextViewTitlePaidUnpaid;
        private TextView mTextViewBallance;
        private TextView mTextViewRequest;
        private EditText mEditTextReason;
        private static TextView mTextViewStartDate;
        private static TextView mTextViewEndDate;
        private ImageView mImageViewClose;
        private RadioButton mRadioButtonFullDay;
        private RadioButton mRadioButtonHalfDay;
        private String mStringJson_responce;
        private int mPositionItem;
        private LeaveTypesResponse mLeaveTypesResponse;
        private LeaveTypesResponse.Data data[];
        private static CreateLeaveResponse mCreateLeaveResponse;
        private static String mStringUserId;
        private static Gson sGson = new Gson();
        public static final MediaType JSON
                = MediaType.parse("application/json; charset=utf-8");
        public static final MediaType MEDIA_TYPE_MARKDOWN = MediaType.parse("text/x-markdown; charset=utf-8");

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.activity_create_leave, container, false);
            return rootView;
        }

        @Override
        public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
            mStringUserId = MyHRPreferences.getInstance(getActivity()).getUser().getData().getUserId();
            sInstance = getActivity();
            mStringUserId = MyHRPreferences.getInstance(getActivity()).getUser().getData().getUserId();
            mTextViewTitleLeave = (TextView) view.findViewById(R.id.textView_create_leave_title_type);
            mTextViewTitlePaidUnpaid = (TextView) view.findViewById(R.id.textView_create_leave_title_paidUnpaid);
            mTextViewBallance = (TextView) view.findViewById(R.id.textView_create_leave_title_ballance_day);
            mEditTextReason = (EditText) view.findViewById(R.id.editText_create_leave_reason);
            mTextViewStartDate = (TextView) view.findViewById(R.id.textView_create_leave_start_date);
            mTextViewEndDate = (TextView) view.findViewById(R.id.textView_create_leave_end_date);
            mTextViewRequest = (TextView) view.findViewById(R.id.textView_create_leave_request);
            mRadioButtonFullDay = (RadioButton) view.findViewById(R.id.radioButton_create_leave_full_day);
            mRadioButtonHalfDay = (RadioButton) view.findViewById(R.id.radioButton_create_leave_half_day);
            mImageViewClose = (ImageView) view.findViewById(R.id.imageView_create_leave_close);
            mImageViewClose.setOnClickListener(this);
            mTextViewRequest.setOnClickListener(this);
            mTextViewStartDate.setOnClickListener(this);
            mTextViewEndDate.setOnClickListener(this);
            mStringJson_responce = getActivity().getIntent().getStringExtra(LEAVE_JSON_EXTRA);
            mPositionItem = getActivity().getIntent().getIntExtra(LEAVE_POSITION_ITEM_EXTRA, 0);
            mLeaveTypesResponse = mGson.fromJson(mStringJson_responce, LeaveTypesResponse.class);
            data = mLeaveTypesResponse.getData();
            mTextViewTitlePaidUnpaid.setText(("-1".equals(data[mPositionItem].getPaid()) ? Configs.UNPAID : Configs.PAID));
            mTextViewTitleLeave.setText(data[mPositionItem].getLeave_name());
            // get ballance -----------------------
            mTextViewBallance.setText(new Utils().cutQuotaBallance(getActivity(), data[mPositionItem].getQuota_ballance()) + " Days");
            initBlankDates();
        }
        public static void initBlankDates(){
            //--------------------------------------------------------------------
            SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, d MMM, yyyy", Locale.ENGLISH);
            Calendar c = Calendar.getInstance();
            mTextViewStartDate.setText(dateFormat.format(c.getTime()));
            sStringStartDateToServer = new SimpleDateFormat("dd/MMM/yyyy").format(c.getTime());
           //--------------------------------------------------------------------------------
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            c.set(year, month, day + 1);
            mTextViewEndDate.setText(dateFormat.format(c.getTime()));
            sStringEndDateToServer = new SimpleDateFormat("dd/MMM/yyyy").format(c.getTime());
        }
        protected Dialog onCreateDialog(int n) {
            DialogFragment newFragment = DateTimeFragment.newInstance(n);
            newFragment.show(this.getFragmentManager(), "datePicker");
            return null;
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.imageView_create_leave_close:
                    getActivity().finish();
                    break;
                case R.id.textView_create_leave_request:
                    requestLeave();
                    break;
                case R.id.textView_create_leave_start_date:
                    //startActivityForResult(new Intent(this, CalendarViewActivity.class), REQUEST_CODE_START_DATE);
                    onCreateDialog(1);
                    break;
                case R.id.textView_create_leave_end_date:
                    //startActivityForResult(new Intent(this, CalendarViewActivity.class), REQUEST_CODE_END_DATE);
                    onCreateDialog(2);
                    break;
                default:
                    break;
            }
        }

        public void requestLeave() {
            Call<ResponseBody> requestLeave = Utils.getMyPosService().createLeave(
                    mStringUserId, String.valueOf(data[mPositionItem].getId()), sStringStartDateToServer,
                    sStringEndDateToServer, mEditTextReason.getText().toString().trim());//
            CallResponseBody callResponseBody = new CallResponseBody();
            callResponseBody.setClazz(CreateLeaveResponse.class);
            requestLeave.enqueue(callResponseBody);
        }


        public static void createLeaveCallback(String responceBody) {
            mCreateLeaveResponse = sGson.fromJson(responceBody, CreateLeaveResponse.class);

            //Toast.makeText(sInstance, responceBody, Toast.LENGTH_SHORT).show();
//----------------------------------- refresh leave history---------------------------------------
            Call<ResponseBody> leavesHistory = Utils.getMyPosService().getLeavesHistory(mStringUserId);
            CallResponseBody callResponseBodyHistory = new CallResponseBody();
            callResponseBodyHistory.setClazz(LeaveHistoryResponce.class);
            leavesHistory.enqueue(callResponseBodyHistory);
            sInstance.finish();
        }

//        @Override
//        public void finish() {
//            super.finish();
//            sInstance = null;
//        }

//        public void makeOkhttp() {
//            RequestBody formBody = new FormEncodingBuilder()
//                    .add("leave_type_id", String.valueOf(data[mPositionItem].getId()))
//                    .add("start_date", sStringStartDateToServer)
//                    .add("end_date", sStringEndDateToServer)
//                    .add("reason", mEditTextReason.getText().toString().trim())
//                    .build();
//            OkHttpClient client = new OkHttpClient();
//
//            Request request = new Request.Builder()
//                    .url("http://dev.mypos.com.sg/api/employee/" + mStringUserId + "/leave")
//                    .post(formBody)
//                    .addHeader("Connection", "Keep-Alive")
//                    .addHeader("Content-Type", "application/x-www-form-urlencoded")
//                    .addHeader("Transfer-Encoding", "chunked")
//                    .build();
//
//            Call call = client.newCall(request);
//            call.enqueue(new Callback() {
//                @Override
//                public void onFailure(Request request, IOException e) {
//                    L.i("onFailure() Request was: " + request);
//
//                    e.printStackTrace();
//                }
//
//                @Override
//                public void onResponse(Response r) throws IOException {
//                    //Utils.showToast(CreateLeaveActivity.this,"Error ");
//                    getActivity().finish();
//
//
//                }
//            });
//        }


        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            if (resultCode == RESULT_OK) {
                String raw_data = data.getStringExtra(CalendarViewActivity.DATE_EXTRA).trim();
                String date[] = raw_data.split("/");
                SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, d MMM. yyyy", Locale.ENGLISH);
                Calendar c = Calendar.getInstance();
                c.set(Integer.parseInt(date[2]), Integer.parseInt(date[1]), Integer.parseInt(date[0]));

                if (date[0].length() == 1) date[0] = "0" + date[0];
                if (date[1].length() == 1) date[1] = "0" + date[1];


                switch (requestCode) {
                    case REQUEST_CODE_START_DATE:
                        sStringStartDateToServer = date[0] + "/" + date[1] + "/" + date[2];
                        mTextViewStartDate.setText(dateFormat.format(c.getTime()));
                        break;
                    case REQUEST_CODE_END_DATE:
                        sStringEndDateToServer = date[0] + "/" + date[1] + "/" + date[2];
                        mTextViewEndDate.setText(dateFormat.format(c.getTime()));
                        break;
                }

            }
        }
    }

}
