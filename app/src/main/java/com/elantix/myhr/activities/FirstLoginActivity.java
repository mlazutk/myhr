package com.elantix.myhr.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Space;
import android.widget.TextView;
import android.widget.Toast;


import com.google.gson.Gson;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.ResponseBody;
import retrofit2.Call;

import com.elantix.myhr.CallResponseBody;
import com.elantix.myhr.L;
import com.elantix.myhr.R;
import com.elantix.myhr.beenResponses.AuthorizedUser;
import com.elantix.myhr.beenResponses.LoginResponse;
import com.elantix.myhr.beenResponses.RenewPasswordResponce;
import com.elantix.myhr.classes.MyHRPreferences;

import org.json.JSONObject;

public class FirstLoginActivity extends AppCompatActivity implements View.OnClickListener {
    public static FirstLoginActivity sInstance;
    private static Button sButtonSigIn;
    private static EditText mEditTextDomain;
    private EditText mEditTextEmail;
    private EditText mEditTextPassword;
    private static TextView sTextViewSingleForgotPassword;
    private TextView mTextViewContainerForgotPassword;
    private TextView mTextViewContainerForgotEmail;
    private TextView mTextViewYourEmail;
    private TextView mTextViewHeaderTitle;
    private Space mSpace;
    private LinearLayout mLinearLayoutBottomContainerForgot;
    private static final String EMAIL_REGEX =
            "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static Gson sGson = new Gson();
    private static AuthorizedUser sAuthorizedUser;
    private static AuthorizedUser sPreferencesUser;
    private static RenewPasswordResponce sRenewPasswordResponce;
    private static String mStringFalseLogin;
    private static String mStringTrueRenewPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_login);
        sInstance = this;
        sPreferencesUser = MyHRPreferences.getInstance(this).getUser();
        L.i("sPreferencesUser = " + sPreferencesUser);
        mEditTextDomain = (EditText) findViewById(R.id.textViewDomain_first_login);
        mEditTextEmail = (EditText) findViewById(R.id.textViewEmaiFirstLogin);
        mEditTextPassword = (EditText) findViewById(R.id.textViewPasswordFirstLogin);
        sTextViewSingleForgotPassword = (TextView) findViewById(R.id.single_forgot_password);
        mTextViewContainerForgotPassword = (TextView) findViewById(R.id.textView_login_container_forgot_password);
        mTextViewContainerForgotEmail = (TextView) findViewById(R.id.textView_login_container_notYourEmail);
        mTextViewYourEmail = (TextView) findViewById(R.id.textView_login_your_email);
        mTextViewHeaderTitle = (TextView) findViewById(R.id.textViewHumanResManed_first_login);
        mSpace = (Space) findViewById(R.id.space_between_email_password);
        mLinearLayoutBottomContainerForgot = (LinearLayout) findViewById(R.id.footer_container_password_email);

        sButtonSigIn = (Button) findViewById(R.id.buttonHumanResManedFirstLogin);
        mStringFalseLogin = getResources().getString(R.string.alert_mess_false_login);
        mStringTrueRenewPassword = getResources().getString(R.string.alert_mess_true_renew_password);
        sTextViewSingleForgotPassword.setOnClickListener(this);
        mTextViewContainerForgotPassword.setOnClickListener(this);
        mTextViewContainerForgotEmail.setOnClickListener(this);
        mTextViewYourEmail.setOnClickListener(this);

        sButtonSigIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Call<ResponseBody> loginCall;
                if (isFirstLogin()) {
                    loginCall = Utils.getMyPosService().login(
                            mEditTextEmail.getText().toString(), mEditTextPassword.getText().toString());

                } else {
                    loginCall = Utils.getMyPosService().login(
                            sPreferencesUser.getUser_info().getEmail(), mEditTextPassword.getText().toString());

                }
                if (!validateMail()) return;

                CallResponseBody callResponseBody = new CallResponseBody();
                callResponseBody.setClazz(LoginResponse.class);
                loginCall.enqueue(callResponseBody);
                //sButtonSigIn.setEnabled(false);
            }
        });
        if (isFirstLogin()) {
            mLinearLayoutBottomContainerForgot.setVisibility(View.GONE);
            mTextViewYourEmail.setVisibility(View.GONE);
            sTextViewSingleForgotPassword.setVisibility(View.VISIBLE);
            mEditTextDomain.setVisibility(View.VISIBLE);
            mEditTextEmail.setVisibility(View.VISIBLE);
            mTextViewHeaderTitle.setText("Human Resource Management");
        } else {
            mTextViewYourEmail.setText((sPreferencesUser.getUser_info().getEmail()));
            mLinearLayoutBottomContainerForgot.setVisibility(View.VISIBLE);
            mTextViewYourEmail.setVisibility(View.VISIBLE);
            sTextViewSingleForgotPassword.setVisibility(View.GONE);
            mEditTextDomain.setVisibility(View.GONE);
            mEditTextEmail.setVisibility(View.GONE);
            mTextViewHeaderTitle.setText("Enter your password to myHR");
            mSpace.setVisibility(View.VISIBLE);
        }
    }

    public boolean isFirstLogin() {
        return (sPreferencesUser == null);
    }
    public boolean validateMail() {
        if (!isFirstLogin()) return true;
        String str = mEditTextEmail.getText().toString().trim();
        Pattern p = Pattern.compile(EMAIL_REGEX);
        Matcher m = p.matcher(str);
        if (!m.matches()) {
            Toast.makeText(FirstLoginActivity.this, "Email is not valid", Toast.LENGTH_SHORT).show();
        }
        return m.matches();
    }

    public static void loginCallback(String responseBody) {
        if (sInstance == null) return;
        sAuthorizedUser = sGson.fromJson(responseBody, AuthorizedUser.class);
        sAuthorizedUser.setDomain(mEditTextDomain.getText().toString());


        new JSONObject(new HashMap<String, String>());

        if (!sAuthorizedUser.isStatus()) {
            Utils.showToast(sInstance, mStringFalseLogin);
            sButtonSigIn.setEnabled(true);
            return;
        }
        MyHRPreferences.getInstance(sInstance).storeUser(sAuthorizedUser);
        sInstance.startActivity(new Intent(sInstance, MainContainerActivity.class));
        sInstance.finish();
    }

    public static void renewPasswordCallback(String responseBody) {
        if (sInstance == null) return;
        sTextViewSingleForgotPassword.setEnabled(true);
        sRenewPasswordResponce = sGson.fromJson(responseBody, RenewPasswordResponce.class);
        if (sRenewPasswordResponce.isStatus()) Utils.showToast(sInstance, mStringTrueRenewPassword);
        else Utils.showToast(sInstance, "Error");
    }

    public void notYourEmail() {
        MyHRPreferences.getInstance(sInstance).storeUser(null);
        //MyHRPreferences.getInstance(sInstance).storeArrayMeetingDay(null);
        sPreferencesUser = null;
        L.i("notYourEmail:sPreferencesUser = " + sPreferencesUser);
        mLinearLayoutBottomContainerForgot.setVisibility(View.GONE);
        mTextViewYourEmail.setVisibility(View.GONE);
        sTextViewSingleForgotPassword.setVisibility(View.VISIBLE);
        mEditTextDomain.setVisibility(View.VISIBLE);
        mEditTextEmail.setVisibility(View.VISIBLE);
        mTextViewHeaderTitle.setText("Human Resource Management");
        mSpace.setVisibility(View.GONE);
    }

    @Override
    public void finish() {
        super.finish();
        sInstance = null;
    }

    @Override
    protected void onDestroy() {
        sInstance = null;
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.single_forgot_password:
                if (!validateMail()) return;
                Call<ResponseBody> loginCall = Utils.getMyPosService().renewPassword(mEditTextEmail.getText().toString());
                CallResponseBody callResponseBody = new CallResponseBody();
                callResponseBody.setClazz(LoginResponse.class);
                loginCall.enqueue(callResponseBody);
                sTextViewSingleForgotPassword.setEnabled(false);
                break;
            case R.id.textView_login_container_forgot_password:
                Call<ResponseBody> loginCall2 = Utils.getMyPosService().renewPassword(sPreferencesUser.getUser_info().getEmail());
                CallResponseBody callResponseBody2 = new CallResponseBody();
                callResponseBody2.setClazz(LoginResponse.class);
                loginCall2.enqueue(callResponseBody2);
                break;
            case R.id.textView_login_container_notYourEmail:
                notYourEmail();
                break;

            default:
                break;
        }
    }

}
