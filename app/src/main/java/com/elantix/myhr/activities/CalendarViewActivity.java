package com.elantix.myhr.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.CalendarView;
import android.widget.TextView;

import com.elantix.myhr.R;

public class CalendarViewActivity extends AppCompatActivity {
    private TextView mTextViewDateDisplay;
    private CalendarView mCalendarView;
    public static final String DATE_EXTRA = "date_extra";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar_view);
        mTextViewDateDisplay = (TextView) findViewById(R.id.date_display);
        mCalendarView = (CalendarView) findViewById(R.id.calendarView);
        mCalendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView calendarView, int i, int i1, int i2) {
                mTextViewDateDisplay.setText(i2 + "/" + i1 + "/" + i);


            }
        });
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_OK, new Intent().putExtra(DATE_EXTRA, mTextViewDateDisplay.getText().toString()));
        finish();
        super.onBackPressed();
    }
}
