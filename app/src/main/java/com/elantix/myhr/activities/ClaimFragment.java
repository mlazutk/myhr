package com.elantix.myhr.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.elantix.myhr.CallResponseBody;
import com.elantix.myhr.L;
import com.elantix.myhr.R;
import com.elantix.myhr.adapters.ClaimeHistoryAdapter;
import com.elantix.myhr.beenResponses.ClaimeHistoryResponce;
import com.elantix.myhr.classes.MyHRPreferences;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * Created by misha on 13.02.2016.
 */
public class ClaimFragment extends Fragment implements View.OnClickListener {
    private static String mStringUserId;
    private static ClaimeHistoryAdapter sClaimeHistoryAdapter;
    private static RecyclerView.LayoutManager mLayoutManager;

    private static FloatingActionMenu mFloatingActionMenu;
    private static FloatingActionButton mFloatingActionCreateClaim;

    public static Context sContext;
    private static RecyclerView sRecyclerView;
    private static final int VERTICAL_ITEM_SPACE = 6;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.claim_fragment_layout, null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mStringUserId = MyHRPreferences.getInstance(getActivity()).getUser().getData().getUserId();
        sContext = getActivity();
        sRecyclerView = (RecyclerView) view.findViewById(R.id.claime_history_recycler_view);
        mFloatingActionMenu = (FloatingActionMenu) view.findViewById(R.id.menu);
        mFloatingActionCreateClaim = (FloatingActionButton) view.findViewById(R.id.menu_item_new_eclaim);

        mLayoutManager = new LinearLayoutManager(getActivity());
        sRecyclerView.setLayoutManager(mLayoutManager);
        //sRecyclerView.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
        mFloatingActionCreateClaim.setOnClickListener(this);

        Call<ResponseBody> claimeHistory = Utils.getMyPosService().getClaimeHistory(mStringUserId);
        CallResponseBody callResponseBody = new CallResponseBody();
        callResponseBody.setClazz(ClaimeHistoryResponce.class);
        claimeHistory.enqueue(callResponseBody);
    }


    public static void ClaimeHistoryCallback(String responceBody) {
        final ClaimeHistoryResponce chr =
                adjustClaimeHistoryList(new Gson().fromJson(responceBody, ClaimeHistoryResponce.class));
        if (chr == null) {
            return;
        }
        sClaimeHistoryAdapter = new ClaimeHistoryAdapter(chr, sContext);
        sRecyclerView.setAdapter(sClaimeHistoryAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        L.i("onDestroy");
    }

    public static ClaimeHistoryResponce adjustClaimeHistoryList(ClaimeHistoryResponce claimeHistoryResponce) {
        final ClaimeHistoryResponce chr = claimeHistoryResponce;
        if (chr == null) {
            return null;
        }
        String sdate = "";
        final List<ClaimeHistoryResponce.Data> dataList = chr.getData();
        final List<ClaimeHistoryResponce.Data> adjustList = new ArrayList<>();
        for (ClaimeHistoryResponce.Data data : dataList) {
            ClaimeHistoryResponce.Data next_data = data;
            String splitSpaceDate[] = next_data.getCreated_at().split("\\s");
            String splitedStrainDate[] = splitSpaceDate[0].split("-");
            if (sdate.equalsIgnoreCase(splitedStrainDate[0])) {
                next_data.setVisible_year(false);
            } else {
                next_data.setVisible_year(true);
            }
            sdate = splitedStrainDate[0];
            adjustList.add(next_data);
        }
        chr.setData(adjustList);
        //=========================================================
        sClaimeHistoryAdapter = new ClaimeHistoryAdapter(chr, sContext);
        sRecyclerView.setAdapter(sClaimeHistoryAdapter);


        return chr;
    }

    public static void NewClaimeCallback(String responce) {
        Call<ResponseBody> claimeHistory = Utils.getMyPosService().getClaimeHistory(mStringUserId);
        CallResponseBody callResponseBody = new CallResponseBody();
        callResponseBody.setClazz(ClaimeHistoryResponce.class);
        claimeHistory.enqueue(callResponseBody);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.menu_item_new_eclaim:
                startActivity(new Intent(getActivity(), NewClaimActivity.class));
                mFloatingActionMenu.close(false);
                break;
            default:
                break;
        }
    }

}