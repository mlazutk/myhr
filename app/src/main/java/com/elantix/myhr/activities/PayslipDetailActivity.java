package com.elantix.myhr.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.elantix.myhr.CallResponseBody;
import com.elantix.myhr.R;
import com.elantix.myhr.beenResponses.PayslipDetailResponce;
import com.elantix.myhr.beenResponses.PayslipMonthlyResponce;
import com.elantix.myhr.beenResponses.PayslipResponce;
import com.elantix.myhr.classes.MyHRPreferences;
import com.google.gson.Gson;

import okhttp3.ResponseBody;
import retrofit2.Call;


/**
 * Created by misha on 04.03.2016.
 */
public class PayslipDetailActivity extends AppCompatActivity {
    public static PayslipDetailActivity sInstance;
    public static final String PAYSLIP_ID_CONTAINER_EXTRA = "payslip_id_extra";
    public static final String PAYSLIP_TOTAL_AMOUNT_EXTRA = "total_amount_extra";
    private String id_container;
    private String total_amount;
    private static Gson sGson = new Gson();
    private static String sUserId;

    private static TextView mTextViewPayslipTotalAmount;
    private static TextView mTextViewPayslipDetailFrom;
    private static TextView mTextViewPayslipDetailTo;
    private static TextView mTextViewEmployer;
    private static TextView mTextViewEmployee;
    private static TextView mTextViewReference;
    private static TextView mTextViewPayslipDetailHourlyBasic;
    private static TextView mTextViewPayslipDetailWorkedHours;
    private static TextView mTextViewTotalAllowances;
    private static TextView mTextmTextViewTotalDeductions;
    private static TextView mTextViewOvertimePaymentFrom;
    private static TextView mTextViewOvertimePaymentTo;
    private static TextView mTextViewOvertimeHoursWorked;
    private static TextView mTextViewTotalOvertimePay;
    private static TextView mTextViewOtherAdditionalPayments;
    private static TextView mTextViewNetPay;
    private static TextView mTextViewDate;
    private static ImageView sImageViewClose;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payslip_monthly);
        sInstance = this;
        sUserId = MyHRPreferences.getInstance(sInstance).getUser().getUser_info().getEmployee_id();
        id_container = getIntent().getStringExtra(PAYSLIP_ID_CONTAINER_EXTRA);
        total_amount = getIntent().getStringExtra(PAYSLIP_TOTAL_AMOUNT_EXTRA);

        sImageViewClose = (ImageView) findViewById(R.id.iv_payslip_detail_close);
        mTextViewPayslipTotalAmount = (TextView) findViewById(R.id.tv_payslip_detail_total_amount);
        mTextViewPayslipDetailFrom = (TextView) findViewById(R.id.tv_payslip_detail_period_from);
        mTextViewPayslipDetailTo = (TextView) findViewById(R.id.tv_payslip_detail_period_to);
        mTextViewEmployer = (TextView) findViewById(R.id.tv_payslip_detail_employer);
        mTextViewEmployee = (TextView) findViewById(R.id.tv_payslip_detail_employee);
        mTextViewReference = (TextView) findViewById(R.id.tv_payslip_detail_reference);
        mTextViewPayslipDetailHourlyBasic = (TextView) findViewById(R.id.tv_payslip_detail_hourly_basic);
        mTextViewPayslipDetailWorkedHours = (TextView) findViewById(R.id.tv_payslip_detail_worked_hours);
        mTextViewTotalAllowances = (TextView) findViewById(R.id.tv_payslip_detail_total_allowances);
        mTextmTextViewTotalDeductions = (TextView) findViewById(R.id.tv_payslip_detail_total_deductions);
        mTextViewOvertimePaymentFrom = (TextView) findViewById(R.id.tv_payslip_detail_overtime_pay_period_from);
        mTextViewOvertimePaymentTo = (TextView) findViewById(R.id.tv_payslip_detail_overtime_pay_period_to);
        mTextViewOvertimeHoursWorked = (TextView) findViewById(R.id.tv_payslip_detail_overtime_hours);
        mTextViewTotalOvertimePay = (TextView) findViewById(R.id.tv_payslip_detail_totla_overtime_pay);
        mTextViewOtherAdditionalPayments = (TextView) findViewById(R.id.tv_payslip_detail_other_add_payment);
        mTextViewNetPay = (TextView) findViewById(R.id.tv_payslip_detail_net_pay);
        mTextViewDate = (TextView) findViewById(R.id.tv_payslip_detail_date);

        sImageViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mTextViewPayslipTotalAmount.setText("$" + total_amount);
        Call<ResponseBody> payslip = Utils.getMyPosService().getPayslipById(id_container);
        CallResponseBody callResponseBody = new CallResponseBody();
        callResponseBody.setClazz(PayslipDetailResponce.class);
        payslip.enqueue(callResponseBody);
    }

    public static void PayslipDetailCallback(String responceBody) {
        PayslipDetailResponce pds = new Gson().fromJson(responceBody, PayslipDetailResponce.class);
        PayslipDetailResponce.Data data = pds.getData();


        mTextViewDate.setText("Payslip " + data.getPayment_date());
        mTextViewPayslipDetailFrom.setText("From " + data.getSalary_start_date());
        mTextViewPayslipDetailTo.setText("To " + data.getSalary_end_date());
        mTextViewEmployer.setText(data.getEmployer_name());
        mTextViewEmployee.setText(data.getEmployee_name());
        mTextViewReference.setText(data.getReference());

        mTextViewPayslipDetailHourlyBasic.setText("$" + data.getHourly_basic() + " per hour");
        mTextViewPayslipDetailWorkedHours.setText(data.getWorked_hours() + " hours worked");

        mTextViewTotalAllowances.setText("$" + data.getAllowance_total());
        mTextmTextViewTotalDeductions.setText("$" + data.getDeduction_total());

        mTextViewOvertimePaymentFrom.setText("From " + data.getSalary_start_date());
        mTextViewOvertimePaymentTo.setText("To " + data.getSalary_end_date());

        mTextViewOvertimeHoursWorked.setText(data.getOt_hours());
        mTextViewTotalOvertimePay.setText("$" + data.getHourly_basic() + "x" + data.getOt_factor() + "x" + data.getOt_hours() + ":$" + data.getOt_amount());

        mTextViewOtherAdditionalPayments.setText("$" + data.getOther_payment_total());
        mTextViewNetPay.setText("$" + data.getNet_total());
    }

}
