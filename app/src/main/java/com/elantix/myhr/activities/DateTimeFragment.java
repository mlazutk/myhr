package com.elantix.myhr.activities;

/**
 * Created by misha on 21.02.2016.
 */

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import com.elantix.myhr.L;
import com.elantix.myhr.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class DateTimeFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {
    private int year;
    private int month;
    private int day;
    private int mInt;


    public static DateTimeFragment newInstance(int n) {
        DateTimeFragment f = new DateTimeFragment();
        Bundle args = new Bundle();
        args.putInt("index", n);
        f.setArguments(args);
        return f;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    public void onDateSet(DatePicker view, int _year, int _month, int _day) {
        // Do something with the date chosen by the user
        year = _year;
        month = _month;
        day = _day;
        int resource = getArguments().getInt("index");
        TextView tvdate = null;

        StringBuilder sb = new StringBuilder()
                // Month is 0 based, just add 1
                .append(day).append("/")
                .append(month).append("/")
                .append(year).append("");
        // set current date into textview
        L.i("resource = " + resource);
        switch (resource) {
            case 1:
                tvdate = (TextView) getActivity().findViewById(R.id.textView_create_leave_start_date);
                if (tvdate == null) return;
                ((CreateLeaveActivity) getActivity()).sStringStartDateToServer = sb.toString();
                String date[] = sb.toString().split("/");
                SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, d MMM. yyyy", Locale.ENGLISH);
                Calendar c = Calendar.getInstance();
                c.set(Integer.parseInt(date[2]), Integer.parseInt(date[1]), Integer.parseInt(date[0]));
                tvdate.setText(dateFormat.format(c.getTime()));
                break;
            case 2:
                tvdate = (TextView) getActivity().findViewById(R.id.textView_create_leave_end_date);
                if (tvdate == null) return;
                ((CreateLeaveActivity) getActivity()).sStringEndDateToServer = sb.toString();
                String date2[] = sb.toString().split("/");
                SimpleDateFormat dateFormat2 = new SimpleDateFormat("EEE, d MMM. yyyy", Locale.ENGLISH);
                Calendar c2 = Calendar.getInstance();
                c2.set(Integer.parseInt(date2[2]), Integer.parseInt(date2[1]), Integer.parseInt(date2[0]));
                tvdate.setText(dateFormat2.format(c2.getTime()));
                break;
            default:
                break;
        }


    }
}


