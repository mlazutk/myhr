package com.elantix.myhr.activities;

import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.elantix.myhr.L;
import com.elantix.myhr.R;
import com.elantix.myhr.TabFragment;
import com.elantix.myhr.classes.LoadImage;
import com.elantix.myhr.classes.LockTimer;
import com.elantix.myhr.classes.MyHRPreferences;
import com.squareup.picasso.Picasso;

import android.support.v4.app.DialogFragment;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainContainerActivity extends AppCompatActivity implements View.OnClickListener {
    DrawerLayout mDrawerLayout;
    public static NavigationView mNavigationView;
    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;
    private TextView mTextViewItemTimesheet;
    private TextView mTextViewItemMeeting;
    private TextView mTextViewItemLeave;
    private TextView mTextViewItemPayslip;
    private TextView mTextViewItemClaime;
    private TextView mTextViewItemSettings;
    private TextView mTextViewNameUser;
    private TabLayout mTabLayout;
    private TabFragment mTabFragment;
    private int mDelayTabSwitch = 10;
    private String mUserName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main_container);
        mTextViewItemTimesheet = (TextView) findViewById(R.id.item_timesheet);
        mTextViewItemMeeting = (TextView) findViewById(R.id.item_meeting);
        mTextViewItemLeave = (TextView) findViewById(R.id.item_leave);
        mTextViewItemPayslip = (TextView) findViewById(R.id.item_paislip);
        mTextViewItemClaime = (TextView) findViewById(R.id.item_claime);
        mTextViewItemSettings = (TextView) findViewById(R.id.item_settings);
        mTextViewNameUser = (TextView) findViewById(R.id.textView_menu_name_user);

        mTextViewItemTimesheet.setOnClickListener(this);
        mTextViewItemMeeting.setOnClickListener(this);
        mTextViewItemLeave.setOnClickListener(this);
        mTextViewItemPayslip.setOnClickListener(this);
        mTextViewItemClaime.setOnClickListener(this);
        mTextViewItemSettings.setOnClickListener(this);

        mTabLayout = (TabLayout) findViewById(R.id.tabs);
        mUserName = MyHRPreferences.getInstance(MainContainerActivity.this).getUser().getUser_info().getName();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mNavigationView = (NavigationView) findViewById(R.id.shitstuff);
        mDrawerLayout.closeDrawers();
        mNavigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.item_timesheet:
                                mDrawerLayout.closeDrawers();
                                new Handler().postDelayed(
                                        new Runnable() {
                                            @Override
                                            public void run() {
                                                mTabFragment.tabLayout.getTabAt(0).select();
                                            }
                                        }, mDelayTabSwitch);
                                break;
                            case R.id.item_meeting:
                                mDrawerLayout.closeDrawers();
                                new Handler().postDelayed(
                                        new Runnable() {
                                            @Override
                                            public void run() {
                                                mTabFragment.tabLayout.getTabAt(1).select();
                                            }
                                        }, mDelayTabSwitch);
                                break;
                            case R.id.item_leave:
                                mDrawerLayout.closeDrawers();
                                new Handler().postDelayed(
                                        new Runnable() {
                                            @Override
                                            public void run() {
                                                mTabFragment.tabLayout.getTabAt(2).select();
                                            }
                                        }, mDelayTabSwitch);
                                break;
                            case R.id.item_paislip:
                                mDrawerLayout.closeDrawers();
                                new Handler().postDelayed(
                                        new Runnable() {
                                            @Override
                                            public void run() {
                                                mTabFragment.tabLayout.getTabAt(3).select();
                                            }
                                        }, mDelayTabSwitch);
                                break;
                            case R.id.item_claime:
                                mDrawerLayout.closeDrawers();
                                new Handler().postDelayed(
                                        new Runnable() {
                                            @Override
                                            public void run() {
                                                mTabFragment.tabLayout.getTabAt(4).select();
                                            }
                                        }, mDelayTabSwitch);
                                break;
                            case R.id.item_settings:
                                mDrawerLayout.closeDrawers();
                                new Handler().postDelayed(
                                        new Runnable() {
                                            @Override
                                            public void run() {
                                                mTabFragment.tabLayout.getTabAt(5).select();
                                            }
                                        }, mDelayTabSwitch);
                                break;
                            default:
                                break;
                        }
                        menuItem.setChecked(true);
                        mDrawerLayout.closeDrawers();
                        return true;
                    }
                });

        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mTabFragment = new TabFragment();
        mFragmentTransaction.replace(R.id.containerView, mTabFragment).commit();
        final LinearLayout headerView = (LinearLayout) mNavigationView.getHeaderView(0);
        final TextView textView = (TextView) headerView.findViewById(R.id.textView_menu_name_user);
        final CircleImageView circleImageView = (CircleImageView) headerView.findViewById(R.id.profile_image);
        final String photo_path = MyHRPreferences.getInstance(MainContainerActivity.this).getUser().getUser_info().getPhoto_path();
        //final String photo_path = "http://dev.mypos.com.sg/assets/uploads/claims/d6da00953500a0f44187420e394ee948.png";
        if (!Utils.isEmpty(photo_path)) {
            Glide.with(this).load(photo_path).into(circleImageView);
        }
        textView.setText(mUserName);

        /**
         * Setup Drawer Toggle of the Toolbar
         */

        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.app_name,
                R.string.app_name);
        toolbar.setTitle(mUserName);

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                mTextViewNameUser.setText(mUserName);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });

        mDrawerToggle.syncState();

    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else
            super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.item_timesheet:
                mDrawerLayout.closeDrawers();
                new Handler().postDelayed(
                        new Runnable() {
                            @Override
                            public void run() {
                                mTabFragment.tabLayout.getTabAt(0).select();
                            }
                        }, mDelayTabSwitch);
                break;
            case R.id.item_meeting:
                mDrawerLayout.closeDrawers();
                new Handler().postDelayed(
                        new Runnable() {
                            @Override
                            public void run() {
                                mTabFragment.tabLayout.getTabAt(1).select();
                            }
                        }, mDelayTabSwitch);
                break;
            case R.id.item_leave:
                mDrawerLayout.closeDrawers();
                new Handler().postDelayed(
                        new Runnable() {
                            @Override
                            public void run() {
                                mTabFragment.tabLayout.getTabAt(2).select();
                            }
                        }, mDelayTabSwitch);
                break;
            case R.id.item_paislip:
                mDrawerLayout.closeDrawers();
                new Handler().postDelayed(
                        new Runnable() {
                            @Override
                            public void run() {
                                mTabFragment.tabLayout.getTabAt(3).select();
                            }
                        }, mDelayTabSwitch);
                break;
            case R.id.item_claime:
                mDrawerLayout.closeDrawers();
                new Handler().postDelayed(
                        new Runnable() {
                            @Override
                            public void run() {
                                mTabFragment.tabLayout.getTabAt(4).select();
                            }
                        }, mDelayTabSwitch);
                break;
            case R.id.item_settings:
                mDrawerLayout.closeDrawers();
                new Handler().postDelayed(
                        new Runnable() {
                            @Override
                            public void run() {
                                mTabFragment.tabLayout.getTabAt(5).select();
                            }
                        }, mDelayTabSwitch);
                break;
            default:
                break;
        }
    }

}
