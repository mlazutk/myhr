package com.elantix.myhr.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.elantix.myhr.CallResponseBody;
import com.elantix.myhr.Database.DBhelper;
import com.elantix.myhr.L;
import com.elantix.myhr.R;
import com.elantix.myhr.beenResponses.BreekTimeResponce;
import com.elantix.myhr.beenResponses.MeetingResponce;
import com.elantix.myhr.beenResponses.StartMeetingResponce;
import com.elantix.myhr.beenResponses.TickResponce;
import com.elantix.myhr.classes.MyHRPreferences;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import okhttp3.ResponseBody;
import retrofit2.Call;

public class StartEventMeetActivity extends AppCompatActivity implements View.OnClickListener,GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    public static final int REQUEST_OPEN_MAP = 1;
    public static final int REQUEST_START_DAY = 2;
    public static final int REQUEST_END_DAY = 3;
    public static final int REQUEST_START_MEETING = 4;
    public static final int REQUEST_END_MEETING = 5;
    public static final int REQUEST_START_BREEK = 6;
    public static final int REQUEST_END_BREEK = 7;

    public static int REQUEST_CODE = 0;
    public static final String EVENT_EXTRA = "event_extra";
    public static StartEventMeetActivity sInstance;
    private ImageView mImageViewBack;
    private TextView mTextViewName;
    private static TextView mTextViewDate;
    private static TextView mTextViewLocation;
    private TextView mTextViewStart;
    private TextView mTextViewTitleBack;
    private static EditText mEditTextMeetingDescr;
    private static String sUserId;
    private static String sLat;
    private static String sLng;
    private static String sStringLocation;
    private static String sStringDescr;
    private static String sStringDate;
    private static String tempDate;

    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    TextView tvLatlong;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!servicesAvailable()) {
            finish();
        }
        setContentView(R.layout.activity_start_meeting);
        sInstance = this;
        REQUEST_CODE = getIntent().getIntExtra(EVENT_EXTRA, 0);
        buildGoogleApiClient();

        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        } else
            Toast.makeText(this, "Not connected...", Toast.LENGTH_SHORT).show();


        sUserId = MyHRPreferences.getInstance(this).getUser().getUser_info().getEmployee_id();
        mImageViewBack = (ImageView) findViewById(R.id.start_meeting_back);
        mTextViewName = (TextView) findViewById(R.id.start_meeting_tv_name_user);
        mTextViewDate = (TextView) findViewById(R.id.start_meeting_tv_date);
        mTextViewLocation = (TextView) findViewById(R.id.start_meeting_tv_location);
        mTextViewStart = (TextView) findViewById(R.id.meeting_tv_start_event);
        mEditTextMeetingDescr = (EditText) findViewById(R.id.editText_meeting_description);
        mImageViewBack.setOnClickListener(this);
        mTextViewStart.setOnClickListener(this);
        mTextViewTitleBack = (TextView) findViewById(R.id.title_arrow_back);
        mTextViewName.setText(MyHRPreferences.getInstance(this).getUser().getUser_info().getName());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);

//-----------------------------------------------------------------------------------------------------------set time--------------------------------
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, d MMM, yyyy", Locale.ENGLISH);
        String splitedTime[] = Utils.getAMCurrentTime();
        String time = splitedTime[0] + " " + splitedTime[1].toLowerCase();
        String date = time + " " + new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH).format(new Date());


        //SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, d MMM, yyyy", Locale.ENGLISH);
        tempDate = simpleDateFormat.format(new Date());
        mTextViewDate.setText(date);

        if (REQUEST_CODE == MeetingFragment.REQUEST_START_DAY){
            mTextViewStart.setText("Start");
            mTextViewTitleBack.setText("Work Day");
        }else if (REQUEST_CODE == MeetingFragment.REQUEST_END_DAY){
            mTextViewStart.setText("End");
            mTextViewTitleBack.setText("Work Day");
        }else if (REQUEST_CODE == MeetingFragment.REQUEST_START_BREEK){
            mTextViewStart.setText("Start");
            mTextViewTitleBack.setText("Break");
        }else if (REQUEST_CODE == MeetingFragment.REQUEST_END_BREEK){
            mTextViewStart.setText("End");
            mTextViewTitleBack.setText("Break");
        }else if (REQUEST_CODE == MeetingFragment.REQUEST_START_MEETING){
            mTextViewStart.setText("Start");
            mTextViewTitleBack.setText("Meeting");
        }else if (REQUEST_CODE == MeetingFragment.REQUEST_END_MEETING){
            mTextViewStart.setText("End");
            mTextViewTitleBack.setText("Meeting");
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void finish() {
        sInstance = null;
        super.finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.start_meeting_back:
                finish();
                break;
            case R.id.meeting_tv_start_event:
                if (mTextViewLocation.getText().toString().length() == 0) return;
                callService(REQUEST_CODE, sLat, sLng, mTextViewLocation.getText().toString(),
                        tempDate, mEditTextMeetingDescr.getText().toString());
                mTextViewStart.setEnabled(false);
                break;
            default:
                break;
        }
    }

    public static void callService(int request_code, String lat, String lng, String location, String date, String descr) {
//        L.i("callService = ");
//        L.i("request_code = " + request_code);
//        L.i("lat = " + lat);
//        L.i("lng = " + lng);
//        L.i("sStringDescr = " + descr);
//        L.i("location = " + location);
//        L.i("date = " + date);
        sStringLocation = location;
        sStringDescr = descr;
        sLat = lat;
        sLng = lng;
        sStringDate = date;
        CallResponseBody callResponseBody = new CallResponseBody();
        Call<ResponseBody> eventCall = null;


        if (request_code == REQUEST_START_DAY) {
            eventCall = Utils.getMyPosService().tick(sUserId, sStringDate, sLat, sLng, sStringLocation, sStringDescr);
            callResponseBody.setClazz(TickResponce.class);
        } else if (request_code == REQUEST_END_DAY) {
            eventCall = Utils.getMyPosService().tick(sUserId, sStringDate, sLat, sLng, sStringLocation, sStringDescr);
            callResponseBody.setClazz(TickResponce.class);
        } else if (request_code == REQUEST_START_MEETING) {
            eventCall = Utils.getMyPosService().startMeeting(sUserId, sStringDate, sLat, sLng, Utils.EVENT_PROGRESS,
                    sStringLocation, sStringDescr);
            callResponseBody.setClazz(MeetingResponce.class);
        } else if (request_code == REQUEST_START_BREEK) {
            L.i("sStringDescr = "+sStringDescr);
            eventCall = Utils.getMyPosService().breaktimeStart(sUserId, sStringDate, sLat, sLng, Utils.EVENT_PROGRESS, sStringLocation, sStringDescr);
            callResponseBody.setClazz(BreekTimeResponce.class);
        } else if (request_code == REQUEST_END_BREEK) {
            L.i("sStringDescr = "+sStringDescr);
            eventCall = Utils.getMyPosService().breaktimeFinish(sUserId, sStringDate);
            callResponseBody.setClazz(BreekTimeResponce.class);
        } else {
        }
        if (eventCall != null) eventCall.enqueue(callResponseBody);
    }

    @Override
    protected void onDestroy() {
        sInstance = null;
        super.onDestroy();
    }
//
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (resultCode == RESULT_OK) {
//            mStringLocation = data.getStringExtra(MeetingMapActivity.LOCATION_EXTRA);
//            mTextViewLocation.setText(mStringLocation);
//            sLat = data.getStringExtra(MeetingMapActivity.LAT_EXTRA);
//            sLng = data.getStringExtra(MeetingMapActivity.LNG_EXTRA);
//        }
//    }

    public static void startMeetingCallback(String responseBody) {
        StartMeetingResponce startMeetingResponce = new Gson().fromJson(responseBody, StartMeetingResponce.class);
        if (!startMeetingResponce.isStatus()) {
            Toast.makeText(sInstance, "status:false", Toast.LENGTH_SHORT).show();
        }//else Toast.makeText(sInstance, "success", Toast.LENGTH_SHORT).show();
        sInstance.finish();
    }
//    public static void tickCallback(String responseBody) {
//        setResult(,RESULT_OK);
//        StartMeetingResponce startMeetingResponce = new Gson().fromJson(responseBody, StartMeetingResponce.class);
//        if (!startMeetingResponce.isStatus()) {
//            Toast.makeText(sInstance, "status:false", Toast.LENGTH_SHORT).show();
//        }//else Toast.makeText(sInstance, "success", Toast.LENGTH_SHORT).show();
//        sInstance.finish();
//    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
//
//        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
//            mGoogleApiClient.disconnect();
//        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult arg0) {
        Toast.makeText(this, "Failed to connect...", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onConnected(Bundle arg0) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);

        if (mLastLocation != null) {
            sLat = String.valueOf(mLastLocation.getLatitude());
            sLng = String.valueOf(mLastLocation.getLongitude());
            mTextViewLocation.setText(Utils.getCompleteAddressString(this, mLastLocation.getLatitude(), mLastLocation.getLongitude()));

        }
        else L.i("mLastLocation = null");

    }

    @Override
    public void onConnectionSuspended(int arg0) {
        Toast.makeText(this, "Connection suspended...", Toast.LENGTH_SHORT).show();

    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    private boolean servicesAvailable() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

        if (ConnectionResult.SUCCESS == resultCode) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(resultCode, this, 0).show();
            return false;
        }
    }
}
