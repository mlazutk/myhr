package com.elantix.myhr.activities;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.elantix.myhr.CallResponseBody;
import com.elantix.myhr.L;
import com.elantix.myhr.R;
import com.elantix.myhr.beenResponses.ClaimeResponce;
import com.elantix.myhr.beenResponses.PayslipMonthlyResponce;
import com.elantix.myhr.classes.BitmapUtils;
import com.elantix.myhr.classes.ImageLoaderTask2;
import com.google.gson.Gson;
//import com.squareup.picasso.Picasso;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;

public class ClaimeInfoActivity extends AppCompatActivity {
    public static final String CLAIME_ID_EXTRA = "claime_pos";
    private String claime_id;
    private static TextView mTextViewMainTitle;
    private static TextView mTextViewSubTitle;
    private static TextView mTextViewD;
    private static TextView mTextViewDay;
    private static TextView mTextViewMonth;
    private static TextView mTextViewAmount;
    private static TextView mTextViewDescription;
    private static ImageView mImageViewClose;
    private static List<ImageView> sImageViewList;
    private static LinearLayout sLinearLayoutImages;
    private static ClaimeInfoActivity sInstance;
    static  int heihtInPixels;
    static  int widthInPixels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_claime_info);
        claime_id = getIntent().getStringExtra(CLAIME_ID_EXTRA);
        sInstance = this;
        heihtInPixels = (int) getResources().getDimension(R.dimen.height_img_preview_claim);
        widthInPixels = (int) getResources().getDimension(R.dimen.width_img_preview_claim);
        sImageViewList=new ArrayList<>();
        for(int i=0;i<3;i++){
            sImageViewList.add(new ImageView(this));
        }
        mTextViewMainTitle = (TextView) findViewById(R.id.textView_claime_title_main);
        mTextViewSubTitle = (TextView) findViewById(R.id.textView_claime_sub_title);
        mTextViewD = (TextView) findViewById(R.id.textView_claime_d);
        mTextViewDay = (TextView) findViewById(R.id.textView_claime_day_week);
        mTextViewMonth = (TextView) findViewById(R.id.textView_claime_month);
        mTextViewAmount = (TextView) findViewById(R.id.textView_claime_amount);
        mTextViewDescription = (TextView) findViewById(R.id.textView_claime_description);
        sLinearLayoutImages = (LinearLayout) findViewById(R.id.ll_claime_images);
        mImageViewClose = (ImageView) findViewById(R.id.imageView_claime_close);
        mImageViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sInstance.finish();
            }
        });

        Call<ResponseBody> claime = Utils.getMyPosService().getClaime(claime_id);
        CallResponseBody callResponseBody = new CallResponseBody();
        callResponseBody.setClazz(ClaimeResponce.class);
        claime.enqueue(callResponseBody);
    }

    @Override
    protected void onResume() {
        super.onResume();
        L.i("onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        L.i("onPause");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        L.i("onDestroy");
    }

    public static void ClaimeCallback(String responce) {
        ClaimeResponce claimeResponce = new Gson().fromJson(responce, ClaimeResponce.class);
        final ClaimeResponce.Data data = claimeResponce.getData();
        mTextViewMainTitle.setText(data.getName());
        mTextViewSubTitle.setText(data.getStatus());

        mTextViewAmount.setText("$" + data.getAmount());
        mTextViewDescription.setText(data.getDescription());
        LayoutParams layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
//--------------------------------------------- extract photos----
        setCurrentTimeHeader(data);
        if (data.getPhotos().length() == 0) return;
        String fotos[] = data.getPhotos().split("\\,");
        ImageView imageView;
      //  L.i("fotos next"+fotos[0]);
//        L.i("fotos next"+fotos[1]);
        for (int i = 0; i < fotos.length; i++) {
            String next_url_photo = fotos[i];
            imageView = sImageViewList.get(i);
            sLinearLayoutImages.setLayoutParams(layoutParams);
            imageView.setVisibility(View.VISIBLE);
            sLinearLayoutImages.addView(imageView);

            LinearLayout.LayoutParams layoutPfarams =
                    new LinearLayout.LayoutParams(widthInPixels, heihtInPixels);
            imageView.setLayoutParams(layoutPfarams);

           // imageView.setImageURI(Uri.fromFile(new File(next_url_photo)));

            //Picasso.with(sInstance).load(next_url_photo).into(imageView);
//            new LoadImage(imageView).execute(next_url_photo);
//            new ImageLoaderTask2(imageView, next_url_photo,
            Glide.with(sInstance)
                    .load(next_url_photo)
                    .into(imageView);
//                    null, 0, null, 200, 400).executeOnExecutor(
//                    AsyncTask.THREAD_POOL_EXECUTOR, (Integer[]) null);

            //---------------------------- add space---------------------------------
            View space = new View(sInstance);
            space.setBackgroundColor(ContextCompat.getColor(sInstance, R.color.grey));
            LinearLayout.LayoutParams layoutParams1 = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            layoutParams1.bottomMargin = 10;
            layoutParams1.topMargin = 10;
            layoutParams1.height = 2;
            sLinearLayoutImages.addView(space, layoutParams1);
        }
        sImageViewList.clear();
        sImageViewList=new ArrayList<>();
        for(int i=0;i<3;i++){
            sImageViewList.add(new ImageView(sInstance));
        }
    }

    public static void setCurrentTimeHeader(ClaimeResponce.Data data) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String sdate = data.getCreated_at();

        String splited[] = sdate.split("\\s");
        Date date = null;
        try {
            date = formatter.parse(splited[0]);
        } catch (ParseException e) {
            e.printStackTrace();
            return;
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        String shortMonth = cal.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.ENGLISH);
        String dayWeek = cal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.ENGLISH);
        String dayMonth = new SimpleDateFormat("d").format(cal.getTime());

        mTextViewD.setText(dayMonth);
        mTextViewDay.setText(dayWeek);
        mTextViewMonth.setText(shortMonth);
    }
    private static class LoadImage extends AsyncTask<String, String, Bitmap> {
        public ImageView mImageView;
        Bitmap bitmap;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        public LoadImage(ImageView imageView) {
            mImageView=imageView;
        }

        protected Bitmap doInBackground(String... args) {
            try {
                bitmap = BitmapFactory.decodeStream((InputStream)new URL(args[0]).getContent());
//                bitmap = BitmapUtils.loadBitmap(args[0]);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap image) {

            if(image != null){
                mImageView.setImageBitmap(image);

            }else{

            }
        }
    }
}
