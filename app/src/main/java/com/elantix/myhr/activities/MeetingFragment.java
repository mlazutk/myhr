package com.elantix.myhr.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.elantix.myhr.CallResponseBody;
import com.elantix.myhr.L;
import com.elantix.myhr.R;
import com.elantix.myhr.adapters.HistoryAdapter;
import com.elantix.myhr.adapters.HistoryMeetingAdapter;
import com.elantix.myhr.beenResponses.MeetingResponce;
import com.elantix.myhr.beenResponses.MeetingTodayResponce;
import com.elantix.myhr.beenResponses.TimeSheetTableResponce;
import com.elantix.myhr.classes.Data;
import com.elantix.myhr.classes.Event;
import com.elantix.myhr.classes.MyHRPreferences;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.gson.Gson;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by misha on 13.02.2016.
 */
public class MeetingFragment extends Fragment implements View.OnClickListener {
    private static TextView mTextViewPM;
    private static TextView mTextViewClockTime;
    public static final int REQUEST_START_DAY = 2;
    public static final int REQUEST_END_DAY = 3;
    public static final int REQUEST_START_MEETING = 4;
    public static final int REQUEST_END_MEETING = 5;
    public static final int REQUEST_START_BREEK = 6;
    public static final int REQUEST_END_BREEK = 7;

    private static String sUserId;

    public static String sLat;
    public static String sLng;
    public static String sStringLocation;
    public static String sStringDescr;
    public static String sStringDate;

    private static TextView mTextViewTimeStartWorkDay;
    private static TextView mTextViewLocationStartWorkDay;
    private static TextView mTextViewDescrStartWorkDay;
    private static LinearLayout mLinearLayoutStartDayContainer;


    private static TextView mTextViewTimeEndWorkDay;
    private static TextView mTextViewDateHeader;
    private static TextView mTextViewLocationEndWorkDay;
    private static TextView mTextViewDescrEndWorkDay;
    private static LinearLayout mLinearLayoutEndDayContainer;

    public static RecyclerView mRecyclerViewMeeting;

    //public static ListView lvMain;

    public static Activity sInstance;
    public static final int REQUEST_CODE_START_EVENT = 1;
    public static HistoryAdapter sHistoryAdapter;
    public static int numbTick = 0;

    private LinearLayoutManager mLayoutManagerMeeting;
    private static FloatingActionMenu mFloatingActionMenu;
    private static FloatingActionButton mFloatingActionStartMeeting;
    private static FloatingActionButton mFloatingActionEndMeeting;

    //--------------------------------------

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.meeting_fragment_layout, null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        sInstance = getActivity();
        sUserId = MyHRPreferences.getInstance(sInstance).getUser().getUser_info().getEmployee_id();
        sHistoryAdapter = new HistoryAdapter(new LinkedList<Event>(), getActivity());
        mFloatingActionMenu = (FloatingActionMenu) view.findViewById(R.id.menu);
        mFloatingActionStartMeeting = (FloatingActionButton) view.findViewById(R.id.menu_item_start_meeting);
        mFloatingActionEndMeeting = (FloatingActionButton) view.findViewById(R.id.menu_item_end_meeting);
        mTextViewPM = (TextView) view.findViewById(R.id.clock_pm_meeting);
        mTextViewClockTime = (TextView) view.findViewById(R.id.clock_time_meeting);

        mRecyclerViewMeeting = (RecyclerView) view.findViewById(R.id.meetings_recycler_meeting);
        mLayoutManagerMeeting = new LinearLayoutManager(getActivity());
        mRecyclerViewMeeting.setLayoutManager(mLayoutManagerMeeting);

        mRecyclerViewMeeting.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                        getActivity().startActivity(new Intent(getActivity(), MeetingMapActivity.class)
                                        .putExtra(MeetingMapActivity.POSITION_ADAPTER_EXTRA, position)
                                        .putExtra(MeetingMapActivity.WHAT_FRAGMENT, 2)

                        );
                    }
                })
        );

        mFloatingActionStartMeeting.setOnClickListener(this);
        mFloatingActionEndMeeting.setOnClickListener(this);
        mTextViewDateHeader = (TextView) view.findViewById(R.id.meeting_tv_title_data);
        //---------------------------------------------------------
        Typeface robotoMediumTypeface = Typeface
                .createFromAsset(getActivity().getAssets(), "fonts/Roboto-Medium.ttf");
        Typeface robotoRegularTypeface = Typeface
                .createFromAsset(getActivity().getAssets(), "fonts/Roboto-Regular.ttf");
        mTextViewClockTime.setTypeface(robotoMediumTypeface);
        mTextViewPM.setTypeface(robotoRegularTypeface);
        setCurrentTimeHeader();
        //-------------------------------------------- Show table events today--------------
        // getTableTodayEvents();
        //------------------ Show menu item visibility and getTableTodayEvents()------------
        mFloatingActionMenu.close(false);
        setMenuEnabled(false);
        getTableTodayEvents();
    }

    @Override
    public void onPause() {
        super.onPause();
       // mFloatingActionMenu.close(false);
    }

    public static void meetingCallback(String responseBody) {
        MeetingResponce meetingResponce = new Gson().fromJson(responseBody, MeetingResponce.class);
        getTableTodayEvents();
       // L.i("meetingResponce status():" + meetingResponce.isStatus());
       // if (!meetingResponce.isStatus()) return;
    }

    public static void timeSheetTableTodayCallback(String responseBody) {
        TimeSheetTableResponce timeSheetTableResponce = new Gson().fromJson(responseBody, TimeSheetTableResponce.class);
        L.i("timeSheetTableResponce: " +timeSheetTableResponce);
        mFloatingActionMenu.close(false);
        if (timeSheetTableResponce == null) {
            setMenuEnabled(false);
            return;
        }
        if (!timeSheetTableResponce.isStatus()) {
            setMenuEnabled(false);
            return;
        }
        L.i("timeSheetTableResponce isStatus: " + timeSheetTableResponce.isStatus());
        List<Data> dataList = timeSheetTableResponce.getData();
        final List<Event> eventList = new ArrayList<>();
        Data adjustedData = null;
        for (int i = 0; i < timeSheetTableResponce.getData().size(); i++) {
            Event event = new Event();
            Data data = dataList.get(i);
            adjustedData = adjustData(data);
            event.setData(adjustedData);
            eventList.add(event);
        }
        numbTick = 0;
        setMenuVisibility(adjustedData);
        mFloatingActionMenu.close(false);
        sHistoryAdapter.setmEventList(eventList);
        L.i("setAdapter : " + sHistoryAdapter.getItemCount());
        mRecyclerViewMeeting.setAdapter(sHistoryAdapter);
        mRecyclerViewMeeting.scrollToPosition(eventList.size() - 1);

        if (StartEventMeetActivity.sInstance != null) {
            StartEventMeetActivity.sInstance.finish();
        }
    }
    public static Data adjustData(Data data) {
        Data adjustData = data;
        String started_at = data.getStarted_at();
        String finished_at = data.getFinished_at();
        if (Utils.IDENTIFICATOR_TICK.equalsIgnoreCase(data.getIdentificator())) {
            numbTick = numbTick + 1;
            if ((numbTick & 1) == 1) {
                adjustData.setColor(Utils.EVENT_COLOR_GREEN);
                adjustData.setTitle(Utils.TITLE_START_DAY);
                adjustData.setStatus(Utils.EVENT_PROGRESS);
            } else {
                adjustData.setColor(Utils.EVENT_COLOR_RED);
                adjustData.setTitle(Utils.TITLE_END_DAY);
                adjustData.setStatus(Utils.EVENT_FINISHED);
            }

        } else if (Utils.IDENTIFICATOR_BREAK_TIME.equalsIgnoreCase(data.getIdentificator())) {
            if (!Utils.isEmpty(started_at)) {
                if (Utils.isEmpty(finished_at)) {
                    adjustData.setColor(Utils.EVENT_COLOR_ORANGE);
                    adjustData.setTitle(Utils.TITLE_BREEK);
                    adjustData.setStatus(Utils.EVENT_PROGRESS_BREAK);
                } else {
                    adjustData.setColor(Utils.EVENT_COLOR_ORANGE);
                    adjustData.setTitle(Utils.TITLE_BREEK);
                    adjustData.setStatus(Utils.EVENT_DONE_BREAK);
                }
            }
        } else if (Utils.IDENTIFICATOR_MEETING.equalsIgnoreCase(data.getIdentificator())) {
            if (!Utils.isEmpty(started_at)) {
                if (Utils.isEmpty(finished_at)) {
                    adjustData.setColor(Utils.EVENT_COLOR_PURPLE);
                    adjustData.setTitle(Utils.TITLE_MEETING);
                    adjustData.setStatus(Utils.EVENT_PROGRESS);
                } else {
                    adjustData.setColor(Utils.EVENT_COLOR_PURPLE);
                    adjustData.setTitle(Utils.TITLE_MEETING);
                    adjustData.setStatus(Utils.EVENT_FINISHED);
                }

            }
        }
        return adjustData;
    }

    public static void getTableTodayEvents() {
        Call<ResponseBody> timeSheetTableCall = Utils.getMyPosService().timeSheetTable(sUserId);
        timeSheetTableCall.enqueue(new retrofit2.Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if(response==null) return;
                    try {
                       // L.i("timeSheetTableResponce: " + response.body().string());
                        timeSheetTableTodayCallback(response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    setMenuEnabled(false);
                }
            });
    }

    public static void setMenuVisibility(Data data) {
        if (data == null) {
            return;
        }
        if (Utils.EVENT_COLOR_GREEN.equalsIgnoreCase(data.getColor())) {
            mFloatingActionStartMeeting.setVisibility(View.VISIBLE);
            mFloatingActionEndMeeting.setVisibility(View.GONE);
        } else if (Utils.EVENT_COLOR_RED.equalsIgnoreCase(data.getColor())) {
            mFloatingActionStartMeeting.setVisibility(View.GONE);
            mFloatingActionEndMeeting.setVisibility(View.GONE);
        } else if (Utils.EVENT_COLOR_ORANGE.equalsIgnoreCase(data.getColor())) {
            if (Utils.EVENT_PROGRESS_BREAK.equalsIgnoreCase(data.getStatus())) {
                mFloatingActionStartMeeting.setVisibility(View.GONE);
                mFloatingActionEndMeeting.setVisibility(View.GONE);
            } else {
                mFloatingActionStartMeeting.setVisibility(View.VISIBLE);
                mFloatingActionEndMeeting.setVisibility(View.GONE);
            }
        } else if (Utils.EVENT_COLOR_PURPLE.equalsIgnoreCase(data.getColor())) {
            if (Utils.EVENT_PROGRESS.equalsIgnoreCase(data.getStatus())) {
                mFloatingActionStartMeeting.setVisibility(View.GONE);
                mFloatingActionEndMeeting.setVisibility(View.VISIBLE);
            } else {
                mFloatingActionStartMeeting.setVisibility(View.VISIBLE);
                mFloatingActionEndMeeting.setVisibility(View.GONE);
            }
        }
    }

    public static void meetingTodayCallback(String responseBody) {
        MeetingTodayResponce meetingsTodayResponce = new Gson().fromJson(responseBody, MeetingTodayResponce.class);

        if ((meetingsTodayResponce != null) && (meetingsTodayResponce.isStatus())) {
            Data arrayData[] = meetingsTodayResponce.getData();
            Data lastData = arrayData[arrayData.length - 1];
            if (lastData == null) {
                mFloatingActionStartMeeting.setVisibility(View.VISIBLE);
                mFloatingActionEndMeeting.setVisibility(View.GONE);
                return;
            }
            if (Utils.EVENT_PROGRESS.equalsIgnoreCase(lastData.getStatus())) {
                mFloatingActionStartMeeting.setVisibility(View.GONE);
                mFloatingActionEndMeeting.setVisibility(View.VISIBLE);
            } else {
                mFloatingActionStartMeeting.setVisibility(View.VISIBLE);
                mFloatingActionEndMeeting.setVisibility(View.GONE);
            }
        } else {
            mFloatingActionStartMeeting.setVisibility(View.VISIBLE);
            mFloatingActionEndMeeting.setVisibility(View.GONE);
        }

    }

    //=============================================================================================================
    public static void setCurrentTimeHeader() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, d MMM, yyyy", Locale.ENGLISH);
        String dateFormated = dateFormat.format(new Date());
        String formSplit[] = dateFormated.split(",");
        mTextViewDateHeader.setText(formSplit[0] + "," + formSplit[1]);
        String splitedTime[] = Utils.getAMCurrentTime();
        mTextViewClockTime.setText(splitedTime[0]);
        mTextViewPM.setText(splitedTime[1]);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(getActivity(), StartEventMeetActivity.class);
        switch (v.getId()) {
            case R.id.menu_item_start_meeting:
                intent.putExtra(
                        StartEventMeetActivity.EVENT_EXTRA, REQUEST_START_MEETING);
                startActivity(intent);
                break;
            case R.id.menu_item_end_meeting:
                CallResponseBody callResponseBody = new CallResponseBody();
                Call<ResponseBody> eventCall = Utils.getMyPosService().finishMeeting(sUserId);
                callResponseBody.setClazz(MeetingResponce.class);
                eventCall.enqueue(callResponseBody);
                break;

            default:
                break;
        }

    }
    //  @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (resultCode != Activity.RESULT_OK) return;
//        if (requestCode == REQUEST_CODE_START_EVENT) {
//            sLat = data.getStringExtra(MeetingMapActivity.LAT_EXTRA);
//            sLng = data.getStringExtra(MeetingMapActivity.LNG_EXTRA);
//            sStringDescr = data.getStringExtra(MeetingMapActivity.DESCRIPTION_EXTRA);
//            sStringDate = data.getStringExtra(MeetingMapActivity.DATE_EXTRA);
//        }
//    }

//    public static void meetingLastCallback(String responseBody) {
//        MeetingLastResponce meetingLastResponce = new Gson().fromJson(responseBody, MeetingLastResponce.class);
//        if (meetingLastResponce == null || !meetingLastResponce.isStatus()) return;
//        L.i("meetingLastCallback : " + meetingLastResponce.getData().getStatus());
//        if ("done".equalsIgnoreCase(meetingLastResponce.getData().getStatus())) {
//            mFloatingActionStartMeeting.setVisibility(View.VISIBLE);
//            mFloatingActionEndMeeting.setVisibility(View.GONE);
//        } else {
//            //L.i("meetingLastCallback : progress");
//            mFloatingActionStartMeeting.setVisibility(View.GONE);
//            mFloatingActionEndMeeting.setVisibility(View.VISIBLE);
//        }
//        getTableTodayEvents();
//    }
public static void setMenuEnabled(boolean enable) {
    if(enable) {

    }else {
        mFloatingActionStartMeeting.setVisibility(View.GONE);
        mFloatingActionEndMeeting.setVisibility(View.GONE);
    }
}
}
