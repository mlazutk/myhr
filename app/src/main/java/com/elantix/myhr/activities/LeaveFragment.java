package com.elantix.myhr.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.elantix.myhr.L;
import com.google.gson.Gson;

import okhttp3.ResponseBody;
import retrofit2.Call;

import com.elantix.myhr.CallResponseBody;
import com.elantix.myhr.R;
import com.elantix.myhr.adapters.LeaveHistoryAdapter;
import com.elantix.myhr.adapters.LeaveTypesAdapter;
import com.elantix.myhr.beenResponses.LeaveHistoryResponce;
import com.elantix.myhr.beenResponses.LeaveTypesResponse;
import com.elantix.myhr.classes.MyHRPreferences;

import java.util.ArrayList;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class LeaveFragment extends Fragment implements View.OnClickListener {
    private static RecyclerView mRecyclerViewLeave;
    private static RecyclerView.Adapter mAdapterLeave;
    private static RecyclerView.LayoutManager mLayoutManagerLeave;
    private static LeaveTypesResponse mLeaveTypesResponse;
    private static Gson mGson = new Gson();

    private static RecyclerView mRecyclerViewLeaveHistory;
    private static RecyclerView.Adapter mAdapterLeaveHistory;
    private static RecyclerView.LayoutManager mLayoutManagerLeaveHistory;
    private static LeaveHistoryResponce mLeaveHistoryResponse;
    private TextView mTextViewLeaveLink;
    private TextView mTextViewLeaveHistoryLink;
    private String mStringUserId;

    private static final int VERTICAL_ITEM_SPACE = 2;
    public static Context sContext;


    public static LeaveFragment newInstance(long id) {
        Bundle arguments = new Bundle();
        //arguments.putLong(EXTRA_NEWS_ID, id);
        LeaveFragment fragment = new LeaveFragment();
        fragment.setArguments(arguments);
        return fragment;
    }

    public LeaveFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_leave_page, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        sContext = getActivity();
        mStringUserId = MyHRPreferences.getInstance(getActivity()).getUser().getData().getUserId();
        mTextViewLeaveLink = (TextView) view.findViewById(R.id.textView_LEAVE_link);
        mTextViewLeaveHistoryLink = (TextView) view.findViewById(R.id.textView_LEAVEHISTORY_link);
        mTextViewLeaveLink.setOnClickListener(this);
        mTextViewLeaveHistoryLink.setOnClickListener(this);
        mRecyclerViewLeave = (RecyclerView) view.findViewById(R.id.leave_types_recycler_view);
        mRecyclerViewLeaveHistory = (RecyclerView) view.findViewById(R.id.linearLayout_leave_history_recycler_view);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerViewLeave.setHasFixedSize(true);
        mRecyclerViewLeaveHistory.setHasFixedSize(true);

        //mRecyclerView.addItemDecoration(new MarginDecoration(this));
        //mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        // use a linear layout manager
        mLayoutManagerLeave = new LinearLayoutManager(getActivity());
        mRecyclerViewLeave.setLayoutManager(mLayoutManagerLeave);

        mLayoutManagerLeaveHistory = new LinearLayoutManager(getActivity());
        mRecyclerViewLeaveHistory.setLayoutManager(mLayoutManagerLeaveHistory);
//        mRecyclerView.addItemDecoration(
//                new DividerItemDecoration(getActivity(), R.drawable.divider_leave));
        mRecyclerViewLeave.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
        // mRecyclerViewLeaveHistory.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));

        Call<ResponseBody> leavesTypes = Utils.getMyPosService().getLeaveTypes(mStringUserId);
        CallResponseBody callResponseBody = new CallResponseBody();
        callResponseBody.setClazz(LeaveTypesResponse.class);
        leavesTypes.enqueue(callResponseBody);

        Call<ResponseBody> leavesHistory = Utils.getMyPosService().getLeavesHistory(mStringUserId);
        CallResponseBody callResponseBodyHistory = new CallResponseBody();
        callResponseBodyHistory.setClazz(LeaveHistoryResponce.class);
        leavesHistory.enqueue(callResponseBodyHistory);
    }

    public static void fillLeaveTypesList(final String responseBody) {
        mLeaveTypesResponse = mGson.fromJson(responseBody, LeaveTypesResponse.class);
        mAdapterLeave = new LeaveTypesAdapter(mLeaveTypesResponse, sContext);
        mRecyclerViewLeave.setAdapter(mAdapterLeave);

        mRecyclerViewLeave.addOnItemTouchListener(
                new RecyclerItemClickListener(sContext, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        sContext.startActivity(new Intent(sContext, CreateLeaveActivity.class)
                                        .putExtra(CreateLeaveActivity.PlaceholderFragment.LEAVE_JSON_EXTRA, responseBody)
                                        .putExtra(CreateLeaveActivity.PlaceholderFragment.LEAVE_POSITION_ITEM_EXTRA, position)
                        );
                    }
                })
        );
        //mAdapter.set();
    }

    public static void fillLeaveHistoryListCallback(final String responseBody) {
        mLeaveHistoryResponse = mGson.fromJson(responseBody, LeaveHistoryResponce.class);
        if(mLeaveHistoryResponse==null) {
            L.i("fillLeaveHistoryListCallback responseBody =" + responseBody);
            L.i("fillLeaveHistoryListCallback mLeaveTypesResponse =" + mLeaveHistoryResponse);
            return;
        }
        L.i("fillLeaveHistoryListCallback responseBody =" + responseBody);
        L.i("fillLeaveHistoryListCallback mLeaveTypesResponse =" + mLeaveHistoryResponse);
        final List<LeaveHistoryResponce.Data> data = mLeaveHistoryResponse.getDataList();


        String prev_date = "";
        List<LeaveHistoryResponce.Data> adjust_list = new ArrayList<>();
        for (LeaveHistoryResponce.Data leaveHistoryData : data) {
            LeaveHistoryResponce.Data mData = leaveHistoryData;

            String splitedDate[] = mData.getStart_date().split("-");

            if (prev_date.equalsIgnoreCase(splitedDate[0])) {
                mData.setYearVisibility(8);
            } else {
                mData.setYearVisibility(0);
            }

            prev_date = splitedDate[0];
            adjust_list.add(mData);
            mLeaveHistoryResponse.setDataList(adjust_list);
            //L.i("adjusted list: " + adjust_list.toString());


            mAdapterLeaveHistory = new LeaveHistoryAdapter(mLeaveHistoryResponse, sContext);
            mRecyclerViewLeaveHistory.setAdapter(mAdapterLeaveHistory);
        }
//        for (LeaveHistoryResponce.Data datal : adjust_list)
//        {
//            L.i(datal.toString());
//            L.i("---------------------");
//        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.textView_LEAVE_link:
                mTextViewLeaveLink.setTextColor(ContextCompat.getColor(getActivity(), R.color.green));
                mTextViewLeaveHistoryLink.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));
                mRecyclerViewLeave.setVisibility(View.VISIBLE);
                mRecyclerViewLeaveHistory.setVisibility(View.GONE);

                break;
            case R.id.textView_LEAVEHISTORY_link:
                mTextViewLeaveHistoryLink.setTextColor(ContextCompat.getColor(getActivity(), R.color.green));
                mTextViewLeaveLink.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));
                mRecyclerViewLeave.setVisibility(View.GONE);
                mRecyclerViewLeaveHistory.setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }
    }


//    @Override
//    public void onLeaveItemClick(View view, int position, long id) {
//        L.i("clicked "+ position);
//        startActivity(new Intent(sContext,CreateLeaveActivity.class));
//               // putExtra());
//
//    }
}
