package com.elantix.myhr.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.elantix.myhr.CallResponseBody;
import com.elantix.myhr.L;
import com.elantix.myhr.R;
import com.elantix.myhr.beenResponses.AuthorizedUser;
import com.elantix.myhr.beenResponses.NewClaimResponce;
import com.elantix.myhr.classes.BitmapCache;
import com.elantix.myhr.classes.ImageChooser;
import com.elantix.myhr.classes.ImageLoaderTask;
import com.elantix.myhr.classes.ImageLoaderTask2;
import com.elantix.myhr.classes.MyHRPreferences;
import com.elantix.myhr.classes.SyncCounter;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class NewClaimActivity extends AppCompatActivity implements View.OnClickListener, Animation.AnimationListener {
    private String mStringUserId;
    private AuthorizedUser.User_info mAuthorizedUserInfo;
    public static final int PICK_UP_CAMERA = 1;
    public static final int PICK_UP_GALARY = 2;
    private ImageView mImageViewBack;
    private ImageView mImageViewAttach;
    private ImageView mImageViewGo;

    public TextView mTextViewImageSize1;
    public TextView mTextViewImageSize2;
    public TextView mTextViewImageSize3;

    public TextView mTextViewImageNameActivity1;
    public TextView mTextViewImageNameActivity2;
    public TextView mTextViewImageNameActivity3;

    public ImageView mImageViewPreview1;
    public ImageView mImageViewPreview2;
    public ImageView mImageViewPreview3;
    public ImageView mImageViewClose1;
    public ImageView mImageViewClose2;
    public ImageView mImageViewClose3;
    //public ImageView mImageViewCamera;
    private int currImageNumber;

    private EditText mEditTextClaimeName;
    private EditText mEditTextAmount;
    private EditText mEditTextDescription;
    private static TextView mTextViewSubmit;


    //private Button mButtonSelectGallary;
    //private Uri mImageCaptureUri;
//    private String mImageCurrSelectedPathGallery;
    //private Uri mImageCurrSelectedUriCamera;
    private Uri mTempUri;
    private List<Uri> mUriList;
    private String mStringSelectedImagePath;
    private LinearLayout mLinearLayoutMenu;
    private LinearLayout mLinearLayoutInstrument1;
    private LinearLayout mLinearLayoutInstrument2;
    private LinearLayout mLinearLayoutInstrument3;

    private LinkedList<Uri> mListUri;
    private LinkedList<String> mPathAttachedStr;
    private Animation fade_in;
    private Animation fade_out;
    private static NewClaimActivity sInstance;
    public GalleryAdapter m_adapter;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;

    //private ArrayList<String> _images;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_eclaime);
        //ButterKnife.inject(this);
        sInstance = this;
        mAuthorizedUserInfo = MyHRPreferences.getInstance(this).getUser().getUser_info();
        mStringUserId = MyHRPreferences.getInstance(this).getUser().getData().getUserId();

        fade_in = AnimationUtils.loadAnimation(this, R.anim.falling_menu_in);
        fade_out = AnimationUtils.loadAnimation(this, R.anim.falling_menu_out);
        mImageViewBack = (ImageView) findViewById(R.id.claime_iv_back);
        mImageViewAttach = (ImageView) findViewById(R.id.eclaim_iv_attach);
        mImageViewPreview1 = (ImageView) findViewById(R.id.eclaime_preview1);
        mImageViewPreview2 = (ImageView) findViewById(R.id.eclaime_preview2);
        mImageViewPreview3 = (ImageView) findViewById(R.id.eclaime_preview3);

        mImageViewClose1 = (ImageView) findViewById(R.id.claim_iv_close1);
        mImageViewClose2 = (ImageView) findViewById(R.id.claim_iv_close2);
        mImageViewClose3 = (ImageView) findViewById(R.id.claim_iv_close3);

        //mImageViewCamera = (ImageView) findViewById(R.id.iv_item_camera);

        mEditTextClaimeName = (EditText) findViewById(R.id.claime_name);
        mEditTextAmount = (EditText) findViewById(R.id.claime_amount);
        mTextViewSubmit = (TextView) findViewById(R.id.eclaim_tv_submit);
        //   mTextViewImageName = (TextView) findViewById(R.id.claim_tv_name_img);
//        mTextViewImageSize = (TextView) findViewById(R.id.claim_tv_size);
        mEditTextDescription = (EditText) findViewById(R.id.editText_claime_description);
        mTextViewImageSize1 = (TextView) findViewById(R.id.claim_tv_size1);
        mTextViewImageSize2 = (TextView) findViewById(R.id.claim_tv_size2);
        mTextViewImageSize3 = (TextView) findViewById(R.id.claim_tv_size3);

        mTextViewImageNameActivity1 = (TextView) findViewById(R.id.claim_tv_name_img1);
        mTextViewImageNameActivity2 = (TextView) findViewById(R.id.claim_tv_name_img2);
        mTextViewImageNameActivity3 = (TextView) findViewById(R.id.claim_tv_name_img3);

        mLinearLayoutMenu = (LinearLayout) findViewById(R.id.ll_menu_method);

        mLinearLayoutInstrument1 = (LinearLayout) findViewById(R.id.eclaime_instrument_block1);
        mLinearLayoutInstrument2 = (LinearLayout) findViewById(R.id.eclaime_instrument_block2);
        mLinearLayoutInstrument3 = (LinearLayout) findViewById(R.id.eclaime_instrument_block3);


        mImageViewBack.setOnClickListener(this);
        mImageViewAttach.setOnClickListener(this);
        mTextViewSubmit.setOnClickListener(this);
        mImageViewClose1.setOnClickListener(this);
        mImageViewClose2.setOnClickListener(this);
        mImageViewClose3.setOnClickListener(this);
        // mImageViewCamera.setOnClickListener(this);


        fade_in.setAnimationListener(this);
        fade_out.setAnimationListener(this);
        fade_in.setDuration(300);
        fade_out.setDuration(400);
        //-----------------------------------------------
//        String[] projection = {MediaStore.Images.Thumbnails._ID};
//        Cursor cursor=getContentResolver().query(MediaStore.Images.Thumbnails.INTERNAL_CONTENT_URI,
//                projection,null,null,MediaStore.Images.Thumbnails.IMAGE_ID);
        // _images = new ArrayList<String>();
        mPathAttachedStr=new LinkedList<>();
        mListUri = getImagesUri(this);
        L.i("mListUri = " + mListUri);
        // initialize bitmap cache
        BitmapCache.InitBitmapCache();

        m_adapter = new GalleryAdapter(this, mListUri);
        mRecyclerView = (RecyclerView) this.findViewById(R.id.lstGallery);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(m_adapter);
        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        setPreviewFromGallery(position);
                    }
                })
        );
    }

    public void setPreviewFromGallery(int position) {
        hideMenuSelectMethod();
        if (position == 0) {
            openCamera();
            return;
        }
        final GalleryAdapter adapter = (GalleryAdapter) mRecyclerView.getAdapter();
        final LinkedList<Uri> listUri = adapter.getListUri();
        final Uri uri = listUri.get(position);
        L.i("clicked uri = " + uri);
        final String path = uri.getPath();
        final int heihtInPixels = (int) getResources().getDimension(R.dimen.height_img_preview_claim);
        final int widthInPixels = (int) getResources().getDimension(R.dimen.width_img_preview_claim);
        LinearLayout.LayoutParams layoutParams =
                new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, heihtInPixels);
        mStringSelectedImagePath = path;
        mPathAttachedStr.add(mStringSelectedImagePath);
        String name_img = "";
        if (path != null || !path.toString().isEmpty()) {
            name_img = path.substring(path.lastIndexOf('/') + 1, path.length());
        }
        if(mPathAttachedStr.size()==1){
            mImageViewPreview1.setLayoutParams(layoutParams);
            mLinearLayoutInstrument1.setVisibility(View.VISIBLE);
            setImgSize(path, mTextViewImageSize1);
            mTextViewImageNameActivity1.setText(name_img);
            mImageViewPreview1.setScaleType(ImageView.ScaleType.CENTER_CROP);
            new ImageLoaderTask2(mImageViewPreview1, path,
                    m_adapter, 0, null, 200, 400).executeOnExecutor(
                    AsyncTask.THREAD_POOL_EXECUTOR, (Integer[]) null);
        }else if(mPathAttachedStr.size()==2){
            mImageViewPreview2.setLayoutParams(layoutParams);
            mLinearLayoutInstrument2.setVisibility(View.VISIBLE);
            setImgSize(path, mTextViewImageSize2);
            mTextViewImageNameActivity2.setText(name_img);
            mImageViewPreview2.setScaleType(ImageView.ScaleType.CENTER_CROP);
            new ImageLoaderTask2(mImageViewPreview2, path,
                    m_adapter, 0, null, 200, 400).executeOnExecutor(
                    AsyncTask.THREAD_POOL_EXECUTOR, (Integer[]) null);
        }else if(mPathAttachedStr.size()==3){
            mImageViewPreview3.setLayoutParams(layoutParams);
            mLinearLayoutInstrument3.setVisibility(View.VISIBLE);
            setImgSize(path, mTextViewImageSize3);
            mTextViewImageNameActivity3.setText(name_img);
            mImageViewPreview3.setScaleType(ImageView.ScaleType.CENTER_CROP);
            new ImageLoaderTask2(mImageViewPreview3, path,
                    m_adapter, 0, null, 200, 400).executeOnExecutor(
                    AsyncTask.THREAD_POOL_EXECUTOR, (Integer[]) null);
        }{

        }

        // L.i("clicked path = " + path);




        //Picasso.with(this).load(path).fit().centerCrop().into(mImageViewPreview);





    }

    public LinkedList<Uri> getImagesUri(Activity activity) {
//        Uri uri;
//        ArrayList<String> listOfAllImages = new ArrayList<String>();
//        String[] projection = {MediaStore.Images.Media.DATA};
//        Cursor cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
//                projection, // Which columns to return
//                null,       // Return all rows
//                null,
//                MediaStore.Images.Media._ID);
//
//        if (cursor != null)
//            do {
//                int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
//                // Get image filename
//                String imagePath = cursor.getString(columnIndex);
//                listOfAllImages.add(imagePath);
//            } while (cursor.moveToNext());
//        return listOfAllImages;
        final Cursor cursor = this.getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null, null, null,
                null);
        LinkedList listUri = new LinkedList<Uri>();

        try {
            cursor.moveToFirst();
            L.i("image count= " + cursor.getCount());


            // loop to put all image uris into our arraylist
            for (int i = 0; i < cursor.getCount(); i++) {
                cursor.moveToPosition(i);
                listUri.add(Uri.parse(cursor.getString(1)));
                //  L.i("m_uris[" + i + "]=" + listUri.get(i));
            }

        } catch (Exception e) {
            L.i("onCreate error: " + e.toString());
            return listUri;
        }
        return listUri;
    }


    public void clearImage1() {
        mStringSelectedImagePath = "";
        mTextViewImageSize1.setText("");
        mTextViewImageNameActivity1.setText("");
        LinearLayout.LayoutParams layoutParams =
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mImageViewPreview1.setLayoutParams(layoutParams);
        mImageViewPreview1.setImageResource(R.drawable.bg_preview_empty);
        mLinearLayoutInstrument1.setVisibility(View.GONE);
        mPathAttachedStr.removeFirst();
    }
    public void clearImage2() {
        mStringSelectedImagePath = "";
        mTextViewImageSize2.setText("");
        mTextViewImageNameActivity2.setText("");
        LinearLayout.LayoutParams layoutParams =
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mImageViewPreview2.setLayoutParams(layoutParams);
        mImageViewPreview2.setImageResource(R.drawable.bg_preview_empty);
        mLinearLayoutInstrument2.setVisibility(View.GONE);
        mPathAttachedStr.remove(1);
    }
    public void clearImage3() {
        mStringSelectedImagePath = "";
        mTextViewImageSize3.setText("");
        mTextViewImageNameActivity3.setText("");
        LinearLayout.LayoutParams layoutParams =
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mImageViewPreview3.setLayoutParams(layoutParams);
        mImageViewPreview3.setImageResource(R.drawable.bg_preview_empty);
        mLinearLayoutInstrument3.setVisibility(View.GONE);
        mPathAttachedStr.removeLast();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.claime_iv_back:
                finish();
                break;
            case R.id.claim_iv_close1:
                clearImage1();
                break;
            case R.id.claim_iv_close2:
                clearImage2();
                break;
            case R.id.claim_iv_close3:
                clearImage3();
                break;
            case R.id.eclaim_iv_attach:
                if (mPathAttachedStr.size() < 3) {
                    showMenuSelectMethod();
                }
                break;
            case R.id.eclaim_tv_submit:
                submit();
                break;

            default:
                break;
        }
    }


    public void openCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        mTempUri = generateFileUri();
        try {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mTempUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, PICK_UP_CAMERA);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        L.d("resultCode = " + resultCode);
        if (data == null) {
            L.d("data == null");
            // return;
        }
        hideMenuSelectMethod();
        if(resultCode==RESULT_OK) {
            if (requestCode == PICK_UP_CAMERA) {
                if (mTempUri == null) {
                    Toast.makeText(this, "SD card not available", Toast.LENGTH_SHORT).show();
                    return;
                }
                L.d("mTempUri =  " + mTempUri);
                String imagePath = ImageChooser.getPath(sInstance, mTempUri);
               // String name_img = "";
                //------------------------set image name---------------------------
//                if (mTempUri != null || !mTempUri.toString().isEmpty()) {
//                    File myFile = new File(mTempUri.toString());
//                    String img_path = myFile.getAbsolutePath();
//                    name_img = img_path.substring(img_path.lastIndexOf('/') + 1, img_path.length());
//                }
                mStringSelectedImagePath = imagePath;
                mPathAttachedStr.add(mStringSelectedImagePath);
                final int heihtInPixels = (int) getResources().getDimension(R.dimen.height_img_preview_claim);
                final int widthInPixels = (int) getResources().getDimension(R.dimen.width_img_preview_claim);
                LinearLayout.LayoutParams layoutParams =
                        new LinearLayout.LayoutParams(widthInPixels, heihtInPixels);
                String name_img = "";
                if (imagePath != null || !imagePath.isEmpty()) {
                    name_img = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.length());
                }

                if(mPathAttachedStr.size()==1){
                    mImageViewPreview1.setLayoutParams(layoutParams);
                    mLinearLayoutInstrument1.setVisibility(View.VISIBLE);
                    setImgSize(imagePath, mTextViewImageSize1);
                    mTextViewImageNameActivity1.setText(name_img);
                    mLinearLayoutInstrument1.setVisibility(View.VISIBLE);
                    new ImageLoaderTask2(mImageViewPreview1, imagePath,
                            m_adapter, 0, null, 200, 400).executeOnExecutor(
                            AsyncTask.THREAD_POOL_EXECUTOR, (Integer[]) null);

                }else  if(mPathAttachedStr.size()==2){
                    mImageViewPreview2.setLayoutParams(layoutParams);
                    mLinearLayoutInstrument2.setVisibility(View.VISIBLE);
                    setImgSize(imagePath, mTextViewImageSize2);
                    mTextViewImageNameActivity2.setText(name_img);
                    mLinearLayoutInstrument2.setVisibility(View.VISIBLE);
                    new ImageLoaderTask2(mImageViewPreview2, imagePath,
                            m_adapter, 0, null, 200, 400).executeOnExecutor(
                            AsyncTask.THREAD_POOL_EXECUTOR, (Integer[]) null);

                }else if(mPathAttachedStr.size()==3){
                    mImageViewPreview3.setLayoutParams(layoutParams);
                    mLinearLayoutInstrument3.setVisibility(View.VISIBLE);
                    setImgSize(imagePath, mTextViewImageSize3);
                    mTextViewImageNameActivity3.setText(name_img);
                    mLinearLayoutInstrument3.setVisibility(View.VISIBLE);
                    new ImageLoaderTask2(mImageViewPreview3, imagePath,
                            m_adapter, 0, null, 200, 400).executeOnExecutor(
                            AsyncTask.THREAD_POOL_EXECUTOR, (Integer[]) null);


                }
                //---------------------------------------------------------
                //setImagePreviewFromCamera(imagePath);
            }
        }
    }

    public void setImagePreviewFromCamera(String capturePath) {


        //String path = ImageChooser.getPath(this, mImageCaptureUri);
        L.d("path camera " + capturePath);
        //Picasso.with(this).load(new File(capturePath)).fit().centerCrop().into(mImageViewPreview);




       // setImgSize(capturePath);
        //--------------------------

    }

    public void setImgSize(String imagePath,TextView textView) {
        File pick_img = new File(imagePath);
        double megabytes = (pick_img.length() / 1024.0) / 1024.0;
        final double d = Math.rint(100.0 * megabytes) / 100.0;
        textView.setText(String.valueOf(d) + " mb");
    }
//    public String getRealPathFromUri(Uri uri) {
//        String prj[] = {MediaStore.Images.Media.DATA};
//        L.d("uri ==  " + uri);
//        //Cursor cursor = managedQuery(uri, prj, null, null, null);
//        Cursor cursor = getContentResolver().query(uri, prj, null, null, null);
//        if (cursor == null) return null;
//        int col_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
//        cursor.moveToFirst();
//        String path = cursor.getString(col_index);
//        L.d("path ==  " + path);
//        cursor.close();
//        return path;
//    }

    private Uri generateFileUri() {
        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            L.i("! Environment.MEDIA_MOUNTED ");
            return null;
        }
        File file = new File(Environment.getExternalStorageDirectory(), "CamScanDoc");
        L.i("Environment.getExternalStorageDirectory() =  " + Environment.getExternalStorageDirectory());
        // L.i("path.getPath() =  " + file.getPath());
        if (!file.exists()) {
            L.i("!path.exists()");
            if (!file.mkdirs()) {
                L.i("!path.mkdirs()");
                return null;
            }
        }
        String timeStamp = String.valueOf(System.currentTimeMillis());
        File newFile = new File(file.getPath() + File.separator + timeStamp + ".jpg");
        return Uri.fromFile(newFile);
    }

    public void submit() {
        //  mTextViewSubmit.setEnabled(false);
        if (mEditTextClaimeName.getText().toString().length() == 0) {
            Utils.showNotificationDialog(sInstance, getResources().getString(R.string.title_error_empty_eclaime_name), 1);
            //  mTextViewSubmit.setEnabled(true);
            return;
        } else if (mEditTextAmount.getText().toString().length() == 0) {
            Utils.showNotificationDialog(sInstance, getResources().getString(R.string.title_error_empty_eclaime_amount), 2);
            // mTextViewSubmit.setEnabled(true);
            return;
        }
        if (mEditTextDescription.getText().toString().length() == 0) {
            Utils.showNotificationDialog(sInstance, getResources().getString(R.string.title_error_empty_eclaime_description), 3);
            // mTextViewSubmit.setEnabled(true);
            return;
        }
        Map<String, RequestBody> map = new HashMap<>();
        final Iterator<String> iterator = mPathAttachedStr.iterator();
        L.i("mPathAttachedStr.size"+mPathAttachedStr.size());
        int i = 1;
        RequestBody fbody;
        while (iterator.hasNext()) {
            final String next_path = iterator.next();
            // String path_img = getRealPathFromUri(next);
            File file = new File(next_path);
            fbody = RequestBody.create(MediaType.parse("image/*"), file);
            L.i("put next photo "+i+": "+file.getAbsolutePath());
            L.i("file.getName() " + file.getName());
            map.put("photo" + i + "\";  filename=\"" + file.getName() + "\"", fbody);
            i = i + 1;
        }
        L.i("map.size "+map.size());
        //  String path_img = ImageChooser.getPath(sInstance, mImageCurrSelectedPathGallery);
//        File file = new File(path);
//        RequestBody fbody = RequestBody.create(MediaType.parse("image/*"), file);
//        map.put("photo1" + "\"; filename=\"pp.png\"", fbody);

        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), mEditTextClaimeName.getText().toString());
        RequestBody amount = RequestBody.create(MediaType.parse("text/plain"), mEditTextAmount.getText().toString());
        RequestBody descr = RequestBody.create(MediaType.parse("text/plain"), mEditTextDescription.getText().toString());


        map.put("name", name);
        map.put("amount", amount);
        map.put("description", descr);
        Call<ResponseBody> requestClaimeMap = Utils.getMyPosService().newClaimeMap(mStringUserId, map);
        CallResponseBody callResponseBody = new CallResponseBody();
        callResponseBody.setClazz(NewClaimResponce.class);
        callResponseBody.setContext(sInstance);
        requestClaimeMap.enqueue(callResponseBody);
        // mTextViewSubmit.setEnabled(true);
        sInstance.finish();
//==================================================================
    }

//    public static void NewClaimeCallback(String responce) {
//        ClaimFragment.NewClaimeCallback(responce);
//        mTextViewSubmit.setEnabled(true);
//        sInstance.finish();
//    }


    public void showMenuSelectMethod() {
        mLinearLayoutMenu.startAnimation(fade_in);
    }

    public void hideMenuSelectMethod() {
        mLinearLayoutMenu.startAnimation(fade_out);
    }


    @Override
    public void onAnimationStart(Animation animation) {
        if (animation.getDuration() == 300) {
            mLinearLayoutMenu.setVisibility(View.VISIBLE);
        } else {

        }
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        if (animation.getDuration() == 300) {
        } else {
            mLinearLayoutMenu.setVisibility(View.GONE);
        }
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
//
//    // ViewHolder class
//    public static class ViewHolder {
//        public ImageView picture;
//        public int position;
//    }

    public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ViewHolder> {
        private final Context context;
        private LinkedList<Uri> imagesUris;
        private final int MAX_TASKS = 50;

        public LinkedList<Uri> getListUri() {
            return imagesUris;
        }

        public void setListUri(LinkedList<Uri> listUri) {
            imagesUris = listUri;
        }

        public GalleryAdapter(Context context, LinkedList<Uri> listUri) {
            this.context = context;
            this.imagesUris = listUri;
            L.i("GalleryAdapter:imagesUris.size() = " + imagesUris.size());
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public ImageView mImageView;
            public int position;

            public ViewHolder(View v) {
                super(v);
                mImageView = (ImageView) v.findViewById(R.id.iv_item_image);
            }
        }

        @Override
        public GalleryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            // create a new view
            LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.gallery_item, parent, false);
            // set the view's size, margins, paddings and layout parameters
            ViewHolder vh = new ViewHolder(linearLayout);
            return vh;
        }

        // Replace the contents of a view (invoked by the layout manager)
        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            final Uri uri = imagesUris.get(position);
            holder.position = position;
            L.i("position[" + position + "] = "
                    + imagesUris.get(position).getPath());
            L.i("GalleryAdapter:onBindViewHolder called ");

            if (position == 0) {
                L.i("GalleryAdapter:position == 0 ");

                holder.mImageView.setImageResource(R.drawable.eclaime_camera);
            } else {
                L.i("GalleryAdapter:loadBitmap(holder.mImageView, position-1, holder);");
                loadBitmap(holder.mImageView, position, holder);
            }
        }


        // load Bitmap either from our cache or asynchronously
        public void loadBitmap(ImageView photo, Integer position,
                               ViewHolder holder) {
            Bitmap bitmap = null;
            bitmap = BitmapCache.getBitmapFromMemCache(position);

            if (bitmap != null) {
                photo.setImageBitmap(bitmap);
                L.i("setting imageview from CACHE");
            } else {
                L.i("SyncCounter.current = "
                        + SyncCounter.current());

                // only start MAX_TASKS asynctasks at a time to prevent
                // out-of-memory errors and to stay under android limit
                L.i("setting imageview from CACHE");

                if (SyncCounter.current() < MAX_TASKS) {
                    new ImageLoaderTask(photo, imagesUris.get(position).getPath(),
                            m_adapter, position, holder, 200, 200).executeOnExecutor(
                            AsyncTask.THREAD_POOL_EXECUTOR, (Integer[]) null);
                }
            }
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public int getItemCount() {
            return imagesUris.size();
        }
    }

}