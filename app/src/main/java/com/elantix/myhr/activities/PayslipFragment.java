package com.elantix.myhr.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.elantix.myhr.CallResponseBody;
import com.elantix.myhr.L;
import com.elantix.myhr.R;
import com.elantix.myhr.adapters.PayslipMonthAdapter;
import com.elantix.myhr.beenResponses.PayslipMonthlyResponce;
import com.elantix.myhr.classes.MyHRPreferences;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.elantix.myhr.beenResponses.PayslipMonthlyResponce.Data;
import com.elantix.myhr.beenResponses.PayslipMonthlyResponce.Data.Month;

import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * Created by misha on 13.02.2016.
 */
public class PayslipFragment extends Fragment implements View.OnClickListener {
    private static RecyclerView mRecyclerViewPayslip;
    private static RecyclerView.Adapter mAdapterPayslip;
    private static RecyclerView.LayoutManager mLayoutManagerPayslip;
    private static PayslipMonthlyResponce sPayslipMonthlyResponce;
    private static Gson mGson = new Gson();

    private TextView mTextViewMonthTitle;
    private TextView mTextViewTotalTitle;
    private String mStringUserId;

    private static final int VERTICAL_ITEM_SPACE = 2;
    public static Context sContext;


    public static PayslipFragment newInstance(long id) {
        Bundle arguments = new Bundle();
        //arguments.putLong(EXTRA_NEWS_ID, id);
        PayslipFragment fragment = new PayslipFragment();
        fragment.setArguments(arguments);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.payslip_fragment_layout,null);
    }

    @Override
    public void onResume() {
        super.onResume();

//        PayslipMonthlyCallback("{\n" +
//                "    \"status\": true,\n" +
//                "    \"data\": {\n" +
//                "        \"November_2015\": {\n" +
//                "            \"totalAmount\": \"2000\",        \n" +
//                "            \"year\": \"2015\",\n" +
//                "            \"month\": \"11\",\n" +
//                "            \"monthName\": \"November\",\n" +
//                "            \"container\": [\n" +
//                "                {\n" +
//                "                    \"id\": \"2\",\n" +
//                "                    \"payment_date\": \"16/11/2015\",\n" +
//                "                    \"employee_name\": \"Angelina Phelps\",\n" +
//                "                    \"salary_period\": \"01/11/2015 to 15/11/2015\",\n" +
//                "                    \"net_total\": \"10.00\",\n" +
//                "                    \"payment_mode\": \"Cash\",\n" +
//                "                    \"reference\": \"Prepaid\"\n" +
//                "                },\n" +
//                "                {\n" +
//                "                    \"id\": \"1\",\n" +
//                "                    \"payment_date\": \"01/11/2015\",\n" +
//                "                    \"employee_name\": \"Angelina Phelps\",\n" +
//                "                    \"salary_period\": \"01/10/2015 to 31/10/2015\",\n" +
//                "                    \"net_total\": \"1.00\",\n" +
//                "                    \"payment_mode\": \"Cash\",\n" +
//                "                    \"reference\": \"\"\n" +
//                "                }\n" +
//                "            ]\n" +
//                "        },\n" +
//                "        \"October_2015\": {\n" +
//                "            \"totalAmount\": \"$1500\",        \n" +
//                "            \"year\": \"2015\",\n" +
//                "            \"month\": \"10\",\n" +
//                "            \"monthName\": \"October\",\n" +
//                "            \"container\": [\n" +
//                "                {\n" +
//                "                    \"id\": \"3\",\n" +
//                "                    \"payment_date\": \"15/10/2015\",\n" +
//                "                    \"employee_name\": \"Angelina Phelps\",\n" +
//                "                    \"salary_period\": \"01/10/2015 to 14/10/2015\",\n" +
//                "                    \"net_total\": \"11.00\",\n" +
//                "                    \"payment_mode\": \"Cash\",\n" +
//                "                    \"reference\": \"\"\n" +
//                "                }\n" +
//                "            ]\n" +
//                "        }\n" +
//                "    }\n" +
//                "}");
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        sContext = getActivity();
        mStringUserId = MyHRPreferences.getInstance(getActivity()).getUser().getData().getUserId();

        mRecyclerViewPayslip = (RecyclerView) view.findViewById(R.id.payslip_type_recycler);
        mRecyclerViewPayslip.setHasFixedSize(true);
        mLayoutManagerPayslip = new LinearLayoutManager(getActivity());
        mRecyclerViewPayslip.setLayoutManager(mLayoutManagerPayslip);

        Call<ResponseBody> payslipMonthly = Utils.getMyPosService().getPayslipMonthly(mStringUserId);
        CallResponseBody callResponseBody = new CallResponseBody();
        callResponseBody.setClazz(PayslipMonthlyResponce.class);
        payslipMonthly.enqueue(callResponseBody);

        // mRecyclerViewPayslip.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
    }


    public static void PayslipMonthlyCallback(final String jsonString) {
        JSONObject rootObj = null;
        JSONObject dataObj = null;
        //----------------------------
        String totalAmount = null;
        String year = null;
        String month = null;
        String monthName = null;
        JSONArray container = null;


        //---------------------------
        boolean isStatus = false;

        try {
            rootObj = new JSONObject(jsonString);
            isStatus = rootObj.getBoolean("status");
            dataObj = rootObj.getJSONObject("data");
            if (!isStatus) return;
            final Iterator<String> keys = dataObj.keys();

            final PayslipMonthlyResponce pmr = new PayslipMonthlyResponce();
            Data data = pmr.new Data();
            Month nextMonth = null;
            List<Month> monthList = new ArrayList<>();
            Month.ContainerObj containerObj = null;
            //List<ContainerObj> containerObjList = null;
            String prev_date = "";
            while (keys.hasNext()) {

                JSONObject keyObj = (JSONObject) dataObj.get(keys.next());
                totalAmount = keyObj.getString("totalAmount");
                year = keyObj.getString("year");
                month = keyObj.getString("month");
                monthName = keyObj.getString("monthName");


//------------------------------------------------------------------------fill container------------
                //containerObjList = new ArrayList<>();
                container = keyObj.getJSONArray("container");
                int numberOfItems = container.length();
                for (int i = 0; i < numberOfItems; i++) {
                    JSONObject nextObj = container.optJSONObject(i);
                    if (nextObj == null) return;
                    String id = nextObj.getString("id");
                    String payment_date = nextObj.getString("payment_date");
                    String employee_name = nextObj.getString("employee_name");
                    String salary_period = nextObj.getString("salary_period");
                    String net_total = nextObj.getString("net_total");
                    String payment_mode = nextObj.getString("payment_mode");
                    String reference = nextObj.getString("reference");
//------------------------------------------------------------------------fill next month------------

                    nextMonth = data.new Month();
                    nextMonth.setTotalAmount(totalAmount);
                    nextMonth.setYear(year);
                    nextMonth.setMonth(month);
                    nextMonth.setMonthName(monthName);

                    if (prev_date.equalsIgnoreCase(monthName + " " + year)) {
                        nextMonth.setIsVisibleTitle(false);
                    } else {
                        nextMonth.setIsVisibleTitle(true);
                    }
                    prev_date = monthName + " " + year;

                    containerObj = nextMonth.new ContainerObj();
                    containerObj.setId(id);
                    containerObj.setPayment_date(payment_date);
                    containerObj.setEmployee_name(employee_name);
                    containerObj.setSalary_period(salary_period);
                    containerObj.setNet_total(net_total);
                    containerObj.setPayment_mode(payment_mode);
                    containerObj.setReference(reference);
                    //containerObjList.add(containerObj);
                    nextMonth.setContainer(containerObj);
                    monthList.add(nextMonth);
                }
                //nextMonth.setContainer(containerObjList);

                data.setMonthList(monthList);
                pmr.setData(data);
            }
            mAdapterPayslip = new PayslipMonthAdapter(pmr, sContext);
            mRecyclerViewPayslip.setAdapter(mAdapterPayslip);
            mRecyclerViewPayslip.addOnItemTouchListener(
                    new RecyclerItemClickListener(sContext, new RecyclerItemClickListener.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            final Month clickedMonth = pmr.getData().getMonthList().get(position);
                            sContext.startActivity(new Intent(sContext, PayslipDetailActivity.class)
                                            .putExtra(PayslipDetailActivity.PAYSLIP_ID_CONTAINER_EXTRA, clickedMonth.getContainer().getId())
                                            .putExtra(PayslipDetailActivity.PAYSLIP_TOTAL_AMOUNT_EXTRA, clickedMonth.getTotalAmount())
                            );
                        }
                    })
            );
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.textView_LEAVE_link:

                break;
            case R.id.textView_LEAVEHISTORY_link:

                break;
            default:
                break;
        }
    }


//    @Override
//    public void onLeaveItemClick(View view, int position, long id) {
//        L.i("clicked "+ position);
//        startActivity(new Intent(sContext,CreateLeaveActivity.class));
//               // putExtra());
//
//    }
}


