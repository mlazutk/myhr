package com.elantix.myhr.activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.elantix.myhr.Database.DBhelper;
import com.elantix.myhr.L;
import com.elantix.myhr.R;
import com.elantix.myhr.adapters.HistoryAdapter;
import com.elantix.myhr.adapters.HistoryMeetingAdapter;
import com.elantix.myhr.classes.Event;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MeetingMapActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String LOCATION_EXTRA = "location_extra";
    public static final String LAT_EXTRA = "lat_extra";
    public static final String LNG_EXTRA = "lng_extra";
    public static final String DESCRIPTION_EXTRA = "descr_extra";
    public static final String DATE_EXTRA = "started_extra";
    public static final String POSITION_ADAPTER_EXTRA = "pos_adapter";
    public static final String WHAT_FRAGMENT = "what_fragment";
    private int position_adapter;
    private int what_fragment;
    private String lat;
    private String lng;
    private TextView mTextViewTitle;
    private TextView mTextViewPoint;
    private TextView mTextViewPeriod;
    private TextView mTextViewTitleDayMonth;
    private TextView mTextViewTitleDayWeek;
    private TextView mTextViewTitleMonth;
    private SupportMapFragment mMapFragment;
    private GoogleMap mGoogleMap;
    private ImageView mImageViewBack;
    private Marker mMarker;
    private String mStringLocation;
    private Event mEvent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meeting_map);
        mTextViewTitle = (TextView) findViewById(R.id.meeting_map_tv_title);
        mTextViewPoint = (TextView) findViewById(R.id.meeting_map_tv_point);
        mTextViewPeriod = (TextView) findViewById(R.id.meeting_map_tv_period);
        mImageViewBack = (ImageView) findViewById(R.id.meeting_map_iv_back);
        mTextViewTitleDayMonth = (TextView) findViewById(R.id.map_title_time);
        mTextViewTitleDayWeek = (TextView) findViewById(R.id.map_title_day_week);
        mTextViewTitleMonth = (TextView) findViewById(R.id.map_title_month);
        Typeface robotoRegularTypeface = Typeface
                .createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
        mTextViewTitle.setTypeface(robotoRegularTypeface);
        mTextViewPoint.setTypeface(robotoRegularTypeface);
        mTextViewPeriod.setTypeface(robotoRegularTypeface);
        mImageViewBack.setOnClickListener(this);
        position_adapter = getIntent().getIntExtra(POSITION_ADAPTER_EXTRA, -1);
        what_fragment = getIntent().getIntExtra(WHAT_FRAGMENT, -1);
        if (what_fragment == 1) {
            HistoryAdapter historyAdapter = (HistoryAdapter) TimesheetFragment.mRecyclerView.getAdapter();
            mEvent = historyAdapter.getEvent(position_adapter);
        } else if (what_fragment == 2) {
            HistoryAdapter historyAdapter = (HistoryAdapter) MeetingFragment.mRecyclerViewMeeting.getAdapter();
            mEvent = historyAdapter.getEvent(position_adapter);
        }
        setTitle(mEvent);
        setCurrentTimeHeader();
        mMapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mGoogleMap = mMapFragment.getMap();
        if (mGoogleMap == null) {
            finish();
            return;
        }
        init();

    }

    private void init() {
//        CameraPosition cameraPosition = new CameraPosition.Builder()
//                .target(new LatLng(Double.valueOf(mEvent.getData().getLat()), Double.valueOf(mEvent.getData().getLng())))
//                .zoom(15f)//
//                .build();
//        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
//        mGoogleMap.animateCamera(cameraUpdate);

        CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(Double.valueOf(mEvent.getData().getLat()), Double.valueOf(mEvent.getData().getLng())));
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);
        mGoogleMap.moveCamera(center);
        mGoogleMap.animateCamera(zoom);
        mTextViewPoint.setText(mEvent.getData().getAddress());
        String started_at = mEvent.getData().getStarted_at();
        String finished_at = mEvent.getData().getFinished_at();

        String date = "";
        String splited[] = {};
        StringBuilder sb = new StringBuilder();
        if ((started_at != null) && (started_at.length() > 0)) {
            date = Utils.convertAMPM(started_at);
            splited = date.split("\\s");
            sb.append(splited[0]).append(" ").append(splited[1]).append(" - ");
        }
        if ((finished_at != null) && (finished_at.length() > 0)) {
            date = Utils.convertAMPM(finished_at);
            splited = date.split("\\s");
            sb.append(splited[0]).append(" ").append(splited[1]);
        }
        mTextViewPeriod.setText(sb.toString());

//        mGoogleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
//            @Override
//            public void onMapClick(LatLng latLng) {
//                if (mMarker != null) {
//                    mMarker.remove();
//                }
//                mMarker = mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(latLng.latitude, latLng.longitude)).icon(
//                        BitmapDescriptorFactory.fromResource(R.drawable.marker_map_meeting)).draggable(true).visible(true));
//                lat = String.valueOf(latLng.latitude);
//                lng = String.valueOf(latLng.longitude);
//                mStringLocation = getAdressMarker(latLng.latitude, latLng.longitude);
//            }
//        });

        mGoogleMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {

            @Override
            public void onMapLongClick(LatLng latLng) {
                // Log.d(TAG, "onMapLongClick: " + latLng.latitude + "," + latLng.longitude);
            }
        });

        mGoogleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {

            @Override
            public void onCameraChange(CameraPosition camera) {
                // Log.d(TAG, "onCameraChange: " + camera.target.latitude + "," + camera.target.longitude);

            }
        });
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mGoogleMap.setMyLocationEnabled(true);
//        mGoogleMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
//
//            @Override
//            public void onMyLocationChange(Location arg0) {
//                // TODO Auto-generated method stub
//                mStringLocation = getAdressMarker(arg0.getLatitude(), arg0.getLongitude());
//                mMarker = mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(arg0.getLatitude(), arg0.getLongitude())).icon(
//                        BitmapDescriptorFactory.fromResource(R.drawable.marker_map_meeting)));
//                lat = String.valueOf(arg0.getLatitude());
//                lng = String.valueOf(arg0.getLongitude());
//                CameraPosition cameraPosition = new CameraPosition.Builder()
//                        .target(new LatLng(arg0.getLatitude(), arg0.getLongitude()))
//                        .zoom(16f)
////                        .bearing(45)
////                        .tilt(20)
//                        .build();
//                CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
//                mGoogleMap.animateCamera(cameraUpdate);
//               // mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16f));
//
//            }
//        });

    }

    public String getAdressMarker(double lat, double lng) {
        String adress = "";
        try {
            Geocoder geo = new Geocoder(MeetingMapActivity.this.getApplicationContext(), Locale.getDefault());
            List<Address> addresses = geo.getFromLocation(lat, lng, 1);
            if (addresses.isEmpty()) {
                mTextViewPoint.setText("Waiting for Location");
            } else {
                if (addresses.size() > 0) {
                    adress = addresses.get(0).getFeatureName() + ", " + addresses.get(0).getLocality() + ", " + addresses.get(0).getAdminArea() + ", " + addresses.get(0).getCountryName();
                    mTextViewPoint.setText(adress);
                    //Toast.makeText(getApplicationContext(), "Address:- " + addresses.get(0).getFeatureName() + addresses.get(0).getAdminArea() + addresses.get(0).getLocality(), Toast.LENGTH_LONG).show();
                }
            }
        } catch (Exception e) {
            e.printStackTrace(); // getFromLocation() may sometimes fail
            return adress;
        }
        return adress;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.meeting_map_iv_back:
                finish();

                break;
            case R.id.textView_LEAVEHISTORY_link:

                break;
            default:
                break;
        }
    }

    public void setCurrentTimeHeader() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String sdate = mEvent.getData().getStarted_at();
        if (Utils.isEmpty(sdate)) {
            sdate = mEvent.getData().getFinished_at();
        }
        if (Utils.isEmpty(sdate)) return;
        String splited[] = sdate.split("\\s");
        L.i("date: " + sdate);
        L.i("Title = " + mEvent.getData().getTitle());
        Date date = null;
        try {
            date = formatter.parse(splited[0]);
        } catch (ParseException e) {
            e.printStackTrace();
            return;
        }
        L.i("date: " + date);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        String shortMonth = cal.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.ENGLISH);
        String dayWeek = cal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.ENGLISH);
        String dayMonth = new SimpleDateFormat("d").format(cal.getTime());

        L.i("shortMonth: " + shortMonth);
        L.i("dayWeek: " + dayWeek);
        L.i("dayMonth: " + dayMonth);

        mTextViewTitleMonth.setText(shortMonth);
        mTextViewTitleDayMonth.setText(dayMonth);
        mTextViewTitleDayWeek.setText(dayWeek);
    }

    public void setTitle(Event event) {
        if (Utils.IDENTIFICATOR_TICK.equalsIgnoreCase(event.getData().getIdentificator())) {
            mTextViewTitle.setText("Work Day");
        } else if (Utils.IDENTIFICATOR_BREAK_TIME.equalsIgnoreCase(event.getData().getIdentificator())) {
            mTextViewTitle.setText("Break");
        } else if (Utils.IDENTIFICATOR_MEETING.equalsIgnoreCase(event.getData().getIdentificator())) {
            mTextViewTitle.setText("Meeting");
        }

    }
}