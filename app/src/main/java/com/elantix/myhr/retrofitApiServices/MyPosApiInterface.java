package com.elantix.myhr.retrofitApiServices;


import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;

import com.elantix.myhr.beenResponses.CreateLeaveResponse;

import java.util.Map;

/**
 * Created by misha on 24.01.2016.
 */
public interface MyPosApiInterface {
    //    @Headers({
//            "Authorization: Basic c3ZvbGhvdnNraXk6VTlHSGVLSnN5M2Y"
//    })
//@Headers({
//        "Accept: application/json"
//})
    @GET("/api/users/cabinet")
    CreateLeaveResponse getAccount(@Header("token") String token
    );

    //http://dev.mypos.com.sg/api/employee/{id}/leave          http://dev.mypos.com.sg/api/employee
    @GET("employee")
    retrofit2.Call<ResponseBody> getEmployeeList();


    //    @Headers("Content-Type: application/json")
//    @FormUrlEncoded
//    @POST("employee/{id}/leave")
//    Call<ResponseBody> createLeave(@Path("id") String id,@Field("leave_type_id") String leave_type_id,
//                                             @Field("start_date") String start_date,
//                                             @Field("end_date") String end_date,
//
//    @Field("reason") String reason);// @Body CreateLeaveBeen createLeaveBeen
//
    // create leave application
    @FormUrlEncoded
    @POST("employee/{id}/leave")
    retrofit2.Call<ResponseBody> createLeave(@Path("id") String id,
                                             @Field("leave_type_id") String leave_type_id,
                                             @Field("start_date") String start_date,
                                             @Field("end_date") String end_date,
                                             @Field("reason") String reason);

    @GET("employee/{id}")
    retrofit2.Call<ResponseBody> getEmployee(@Path("id") String id);

    @FormUrlEncoded
    @POST("auth/login")
    retrofit2.Call<ResponseBody> login(@Field("identity") String email, @Field("password") String password);

    //show leaves by employeeID(history leave)
    @GET("employee/{id}/leaves")
    retrofit2.Call<ResponseBody> getLeavesHistory(@Path("id") String id);

    //show leave types by employeeID
    @GET("employee/{id}/leavetypes")
    retrofit2.Call<ResponseBody> getLeaveTypes(@Path("id") String id);
    //renew user password

    @FormUrlEncoded
    @POST("auth/forgot_password")
    Call<ResponseBody> renewPassword(@Field("identity") String email);


    @FormUrlEncoded
    @POST("employee/{id}/meeting/start")
    Call<ResponseBody> startMeeting(@Path("id") String id,
                                    @Field("datetime") String datetime,
                                    @Field("lat") String lat,
                                    @Field("lng") String lng,
                                    @Field("status") String status,
                                    @Field("address") String address,
                                    @Field("note") String note);


    @POST("employee/{id}/meeting/finish")
    Call<ResponseBody> finishMeeting(@Path("id") String id);


    @FormUrlEncoded
    @POST("employee/{id}/tick")
    Call<ResponseBody> tick(@Path("id") String id,
                            @Field("datetime") String datetime,
                            @Field("lat") String lat,
                            @Field("lng") String lng,
                            @Field("address") String address,
                            @Field("note") String note);

    @FormUrlEncoded
    @POST("employee/{id}/tick")
    Call<ResponseBody> tickEnd(@Path("id") String id,
                               @Field("lat") String lat,
                               @Field("lng") String lng,
                               @Field("address") String address,
                               @Field("note") String note);


    @FormUrlEncoded
    @POST("employee/{id}/breaktime/start")
    Call<ResponseBody> breaktimeStart(@Path("id") String id,
                                      @Field("started") String started,
                                      @Field("lat") String lat,
                                      @Field("lng") String lng,
                                      @Field("status") String status,
                                      @Field("address") String address,
                                      @Field("note") String note);

    @FormUrlEncoded
    @POST("employee/{id}/breaktime/finish")
    Call<ResponseBody> breaktimeFinish(@Path("id") String id,
                                       @Field("finished") String finished);

    @GET("employee/{id}/meetings/today")
    Call<ResponseBody> meetingsToday(@Path("id") String id);

    @GET("employee/{id}/tick/status")
    Call<ResponseBody> tickStatus(@Path("id") String id);

    @GET("employee/{id}/timesheet/today")
    Call<ResponseBody> timeSheetTable(@Path("id") String id);

    @GET("employee/{id}/meeting/last")
    Call<ResponseBody> meetingLast(@Path("id") String id);


    // Get payslip by employee ID
    @GET("employee/{id}/payslip")
    Call<ResponseBody> getPayslipTypes(@Path("id") String id);


    // Get payslip by employee ID with monthly view
    @GET("employee/{id}/payslip/monthly")
    Call<ResponseBody> getPayslipMonthly(@Path("id") String id);

    // Get payslip by  ID
    @GET("payslip/{id}")
    Call<ResponseBody> getPayslipById(@Path("id") String id);

    //Get Claims list by Employee ID
    @GET("employee/{id}/claim")
    Call<ResponseBody> getClaimeHistory(@Path("id") String id);


    //Get Claim by ID
    @GET("claim/{id}")
    Call<ResponseBody> getClaime(@Path("id") String id);


    @Multipart
    @FormUrlEncoded
    @POST("employee/{id}/claim")
    Call<ResponseBody> newClaime(
            @Part("id") RequestBody userId,
            @Part("name") RequestBody name,
            @Part("amount") RequestBody amount,
            @Part("description") RequestBody description,
            @Part("photo1") RequestBody photo1,
            @Part("photo2") RequestBody photo2,
            @Part("photo3") RequestBody photo3);


    @Multipart
    @POST("employee/{id}/claim")
    Call<ResponseBody> newClaimeMap(@Path("id") String id,
                                    @PartMap Map<String, RequestBody> params);
}