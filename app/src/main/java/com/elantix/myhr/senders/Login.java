package com.elantix.myhr.senders;

/**
 * Created by misha on 27.01.2016.
 */
public class Login {
   // private String domain;
    public String identity;
    public String password;

//    public Login(String domain, String email,String password) {
//        this.domain=domain;
//        this.identity = email;
//        this.password = password;
//    }

    public Login(String email,String password) {
        this.identity = email;
        this.password = password;
    }

    public String getEmail() {
        return identity;
    }

    public void setEmail(String email) {
        this.identity = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
