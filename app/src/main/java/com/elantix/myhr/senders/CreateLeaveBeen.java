package com.elantix.myhr.senders;


/**
 * Created by misha on 24.01.2016.
 */
public class CreateLeaveBeen {

    public String leave_type_id;

    public String start_date;

    public String end_date;

    public String reason;

    public String getLeave_type_id() {
        return leave_type_id;
    }

    public void setLeave_type_id(String leave_type_id) {
        this.leave_type_id = leave_type_id;
    }

    public String getStart_date() {
        return start_date;
    }

    public CreateLeaveBeen() {

    }
    public CreateLeaveBeen(String start_date, String end_date, String reason) {
        this.start_date = start_date;
        this.end_date = end_date;
        this.reason = reason;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
