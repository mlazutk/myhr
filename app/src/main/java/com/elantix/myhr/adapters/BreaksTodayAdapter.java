package com.elantix.myhr.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.elantix.myhr.R;
import com.elantix.myhr.activities.Utils;
import com.elantix.myhr.beenResponses.BreektimeFinishResponce;
import com.elantix.myhr.beenResponses.BreektimeStartResponce;
import com.elantix.myhr.beenResponses.MeetingTodayResponce;

/**
 * Created by misha on 24.02.2016.
 */
public class BreaksTodayAdapter extends RecyclerView.Adapter<BreaksTodayAdapter.ViewHolder> {

    private BreektimeStartResponce mBreektimeStartResponce;
    private Context mContext;

    public class ViewHolder extends RecyclerView.ViewHolder {


        public LinearLayout mLinearLayoutRoot;
        public LinearLayout mLinearLayoutTimeBlock;
        public LinearLayout mLinearLayoutDescr;

        private TextView mTextViewBreakStart;
        private TextView mTextViewBreakEnd;
        private TextView mTextViewDescription;

        public ViewHolder(View v) {
            super(v);
            mLinearLayoutRoot = (LinearLayout) v;
            mLinearLayoutTimeBlock = (LinearLayout) mLinearLayoutRoot.findViewById(R.id.breaks_ll_times_block);
            mTextViewBreakStart = (TextView) mLinearLayoutTimeBlock.findViewById(R.id.breeks_tv_start_break);
            mTextViewBreakEnd = (TextView) mLinearLayoutTimeBlock.findViewById(R.id.breeks_tv_end_break);

            mLinearLayoutDescr = (LinearLayout) mLinearLayoutRoot.findViewById(R.id.breeks_ll_descr);
            mTextViewDescription = (TextView) mLinearLayoutDescr.findViewById(R.id.breeks_tv_descr_break);

        }
    }

    public void add(int position, String item) {
        // mHistoryLeaveResponse.add(position, item);
        // notifyItemInserted(position);
    }

    public void remove(String item) {
//        int position = mDataset.indexOf(item);
//        mDataset.remove(position);
//        notifyItemRemoved(position);
    }

    public BreaksTodayAdapter(BreektimeStartResponce myDataset, Context context) {
        mBreektimeStartResponce = myDataset;
        mContext = context;
    }

    @Override
    public BreaksTodayAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LinearLayout relativeLayoutRoot = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_recycler_view_breeks, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(relativeLayoutRoot);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        BreektimeStartResponce.Data[] data = mBreektimeStartResponce.getData();
        holder.mTextViewBreakStart.setText(data[position].getStarted_at());
        holder.mTextViewBreakEnd.setText(data[position].getFinished_at());
        holder.mTextViewDescription.setText(data[position].getNote());
    }

    @Override
    public int getItemCount() {
        return mBreektimeStartResponce.getData().length;
    }
}

