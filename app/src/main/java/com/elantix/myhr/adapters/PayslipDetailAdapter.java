package com.elantix.myhr.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.elantix.myhr.R;
import com.elantix.myhr.beenResponses.PayslipDetailResponce;
import com.elantix.myhr.beenResponses.PayslipMonthlyResponce;
import com.elantix.myhr.beenResponses.PayslipTypesResponce;
import com.elantix.myhr.classes.MyHRPreferences;

import java.util.List;

/**
 * Created by misha on 29.01.16.
 */
public class PayslipDetailAdapter extends RecyclerView.Adapter<PayslipDetailAdapter.ViewHolder> {
    private PayslipDetailResponce mPayslipDetailResponce;
    private Context mContext;


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case


        public TextView mTextViewDate;
        public TextView mTextViewName;
        public TextView mTextViewTotal;

        public ViewHolder(View v) {
            super(v);

            mTextViewDate = (TextView) v.findViewById(R.id.payslip_types_date);
            mTextViewName = (TextView) v.findViewById(R.id.payslip_types_name);
            mTextViewTotal = (TextView) v.findViewById(R.id.payslip_types_net_pay);

        }
    }


    public PayslipDetailAdapter(PayslipDetailResponce myDataset, Context context) {
        mPayslipDetailResponce = myDataset;
        mContext = context;
    }

    @Override
    public PayslipDetailAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LinearLayout linearLayoutRoot = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_payslip_list_recycler, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(linearLayoutRoot);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
//        List<PayslipTypesResponce.Data> data = mPayslipTypesResponce.getDataList();
//        holder.mTextViewDate.setText(data.get(position).getPayment_date());
//
//
//        //String name_sername=data.get(position).getEmployee_name();
//        String name = "";
//        String sername = "";
//
//        name = MyHRPreferences.getInstance(mContext).getUser().getUser_info().getFirst_name();
//        sername = MyHRPreferences.getInstance(mContext).getUser().getUser_info().getLast_name();
//
//        holder.mTextViewName.setText(name + "\n" + sername);
//
//        String payNetTotal = "$" + data.get(position).getNet_total();
//        if (payNetTotal.length() > 6) payNetTotal = payNetTotal.substring(0, 5) + "...";
//        holder.mTextViewTotal.setText(payNetTotal);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return 0;
    }
}
