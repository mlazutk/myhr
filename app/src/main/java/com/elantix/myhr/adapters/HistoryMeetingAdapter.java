package com.elantix.myhr.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.elantix.myhr.L;
import com.elantix.myhr.R;
import com.elantix.myhr.activities.Utils;
import com.elantix.myhr.classes.Data;
import com.elantix.myhr.classes.Event;

import java.util.List;

/**
 * Created by misha on 23.02.2016.
 */

public class HistoryMeetingAdapter extends RecyclerView.Adapter<HistoryMeetingAdapter.ViewHolder> {
    private List<Event> mEventList;
    private Context mContext;

    public List<Event> getmEventList() {
        return mEventList;
    }

    //public updateAdapter()
    public void add(Event item) {
        mEventList.add(item);
    }

    public void removeLast() {
        int position = mEventList.size() - 1;
        L.i("mEventList.size() = " + mEventList.size());
        L.i("position = " + position);
        mEventList.remove(position);
    }

    public Event getEvent(int pos) {
        return mEventList.get(pos);
    }

    public void setmEventList(List<Event> eventList) {
        mEventList = eventList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public LinearLayout mLinearLayoutRoot;
        private ScrollView mScrollViewMeeting;
        // private LinearLayout mLinearLayoutWrappedScrollView;
        //private LinearLayout mLinearLayoutMeetingStartDayContainer;

        public LinearLayout mLinearLayoutTimeCont;
        public LinearLayout mLinearLayoutLocDescr;

        private TextView mTextViewTimeStart;
        private TextView mTextViewAmPmStart;
        private TextView mTextViewAmPmEnd;
        private TextView mTextViewTimeEnd;
        private TextView mTextViewLocation;
        private TextView mTextViewDescription;
        private TextView mTextViewTitleItem;
        private View mViewVerticalColor;

        public ViewHolder(View v) {
            super(v);
            mLinearLayoutRoot = (LinearLayout) v;
            mLinearLayoutTimeCont = (LinearLayout) mLinearLayoutRoot.findViewById(R.id.item_meeting_time_container);
            mTextViewTimeStart = (TextView) mLinearLayoutTimeCont.findViewById(R.id.meeting_tv_time_start);
            mTextViewAmPmStart = (TextView) mLinearLayoutTimeCont.findViewById(R.id.meeting_tv_pm_am);
            mTextViewAmPmEnd = (TextView) mLinearLayoutTimeCont.findViewById(R.id.meeting_tv_pm_am_finish);
            mTextViewTimeEnd = (TextView) mLinearLayoutTimeCont.findViewById(R.id.meeting_tv_time_end);

            mViewVerticalColor = mLinearLayoutRoot.findViewById(R.id.vertical_color_view);
            mLinearLayoutLocDescr = (LinearLayout) mLinearLayoutRoot.findViewById(R.id.item_meeting_loc_descr_container);
            mTextViewLocation = (TextView) mLinearLayoutLocDescr.findViewById(R.id.meeting_tv_location);
            mTextViewDescription = (TextView) mLinearLayoutLocDescr.findViewById(R.id.meeting_tv_description);
            mTextViewTitleItem = (TextView) mLinearLayoutLocDescr.findViewById(R.id.item_timesheet_title);
        }
    }


    public HistoryMeetingAdapter(List<Event> myDataset, Context context) {
        mEventList = myDataset;
        mContext = context;
    }

    @Override
    public HistoryMeetingAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_timesheet_meeting, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(linearLayout);
        return vh;
    }
    // Replace the contents of a view (invoked by the layout manager)

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Data data = mEventList.get(position).getData();

        //data.toString();
        final String started_at = data.getStarted_at();
        final String finished_at = data.getFinished_at();
        if (data.getTitle().equalsIgnoreCase(Utils.TITLE_START_DAY)) {
            if (!Utils.isEmpty(started_at)) {
                String date = Utils.convertAMPM(started_at);
                String splited[] = date.split("\\s");
                holder.mTextViewTimeStart.setText(splited[0]);
                holder.mTextViewAmPmStart.setText(splited[1]);
            }
            holder.mViewVerticalColor.setBackgroundColor(ContextCompat.getColor(mContext, R.color.green));

        } else if ((data.getTitle().equalsIgnoreCase(Utils.TITLE_END_DAY))) {
            if (!Utils.isEmpty(started_at)) {
                String date = Utils.convertAMPM(started_at);
                String splited[] = date.split("\\s");
                holder.mTextViewTimeStart.setText(splited[0]);
                holder.mTextViewAmPmStart.setText(splited[1]);
            }
            holder.mViewVerticalColor.setBackgroundColor(ContextCompat.getColor(mContext, R.color.red));

        } else {
            String splited[] = {};
            if (!Utils.isEmpty(started_at)) {
                String date = Utils.convertAMPM(started_at);
                splited = date.split("\\s");
                holder.mTextViewTimeStart.setText(splited[0]);
                holder.mTextViewAmPmStart.setText(splited[1]);
            }
            if (!Utils.isEmpty(finished_at)) {
                String date = Utils.convertAMPM(finished_at);
                splited = date.split("\\s");
                holder.mTextViewTimeEnd.setText(splited[0]);
                holder.mTextViewAmPmEnd.setText(splited[1]);
            }
            String color = data.getColor();
            if (color.equalsIgnoreCase(Utils.EVENT_COLOR_ORANGE)) {
                holder.mViewVerticalColor.setBackgroundColor(ContextCompat.getColor(mContext, R.color.orange));
            } else
                holder.mViewVerticalColor.setBackgroundColor(ContextCompat.getColor(mContext, R.color.purple));

        }

        holder.mTextViewTitleItem.setText(data.getTitle());
        holder.mTextViewLocation.setText(data.getAddress());
        holder.mTextViewDescription.setText(data.getNote());
    }

    @Override
    public int getItemCount() {
        return mEventList.size();
    }


}
