package com.elantix.myhr.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.elantix.myhr.L;
import com.elantix.myhr.R;
import com.elantix.myhr.activities.Utils;
import com.elantix.myhr.beenResponses.MeetingTodayResponce;
import com.elantix.myhr.classes.Data;
import com.elantix.myhr.classes.Event;
import com.elantix.myhr.classes.HistoryData;

import java.util.List;

/**
 * Created by misha on 23.02.2016.
 */

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {
    private List<Event> mEventList;
    private Context mContext;

    public List<Event> getmEventList() {
        return mEventList;
    }
    //public updateAdapter()


    public Event getEvent(int pos) {
        return mEventList.get(pos);
    }

    public void setmEventList(List<Event> eventList) {
        mEventList = eventList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ScrollView mScrollViewMeeting;
        public LinearLayout mLinearLayoutLocDescr;

        private TextView mTextViewTimeStart;
        private TextView mTextViewAmPmStart;
        private TextView mTextViewAmPmEnd;
        private TextView mTextViewTimeEnd;
        private TextView mTextViewLocation;
        private TextView mTextViewDescription;
        private TextView mTextViewTitleItem;
        private View mViewVerticalColor;

        public ViewHolder(View v) {
            super(v);
            mTextViewTimeStart = (TextView) v.findViewById(R.id.meeting_tv_time_start);
            mTextViewAmPmStart = (TextView) v.findViewById(R.id.meeting_tv_pm_am);
            mTextViewAmPmEnd = (TextView) v.findViewById(R.id.meeting_tv_pm_am_finish);
            mTextViewTimeEnd = (TextView) v.findViewById(R.id.meeting_tv_time_end);

            mViewVerticalColor = v.findViewById(R.id.vertical_color_view);
            mLinearLayoutLocDescr = (LinearLayout) v.findViewById(R.id.item_meeting_loc_descr_container);
            mTextViewLocation = (TextView) v.findViewById(R.id.meeting_tv_location);
            mTextViewDescription = (TextView) v.findViewById(R.id.meeting_tv_description);
            mTextViewTitleItem = (TextView) v.findViewById(R.id.item_timesheet_title);
        }
    }

    public void add(Event item) {
        mEventList.add(item);
    }


    public HistoryAdapter(List<Event> myDataset, Context context) {
        mEventList = myDataset;
        mContext = context;
    }

    @Override
    public HistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_timesheet_meeting, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(linearLayout);
        return vh;
    }
    // Replace the contents of a view (invoked by the layout manager)

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Data data = mEventList.get(position).getData();
        //L.i(data.toString());
        final String started_at = data.getStarted_at();
        final String finished_at = data.getFinished_at();
        if (data.getTitle().equalsIgnoreCase(Utils.TITLE_START_DAY) || (data.getTitle().equalsIgnoreCase(Utils.TITLE_END_DAY))) {
//            holder.mTextViewTimeEnd.setVisibility(View.INVISIBLE);
//            holder.mTextViewAmPmEnd.setVisibility(View.INVISIBLE);

            String date = Utils.convertAMPM(started_at);
            String splited[] = date.split("\\s");
            holder.mTextViewTimeStart.setText(splited[0]);
            holder.mTextViewAmPmStart.setText(splited[1]);
            holder.mTextViewTimeEnd.setText("");
            holder.mTextViewAmPmEnd.setText("");
            //  }
            if (data.getTitle().equalsIgnoreCase(Utils.TITLE_END_DAY)) {
                holder.mViewVerticalColor.setBackgroundColor(ContextCompat.getColor(mContext, R.color.red));
            }
            if (data.getTitle().equalsIgnoreCase(Utils.TITLE_START_DAY)) {
                holder.mViewVerticalColor.setBackgroundColor(ContextCompat.getColor(mContext, R.color.green));
            }
        } else {
//            holder.mTextViewTimeEnd.setVisibility(View.VISIBLE);
//            holder.mTextViewAmPmEnd.setVisibility(View.VISIBLE);
//            holder.mTextViewTimeEnd.setText("");
//            holder.mTextViewAmPmEnd.setText("");
            String splited[] = {};
            if (!Utils.isEmpty(started_at)) {
                String date = Utils.convertAMPM(started_at);
                splited = date.split("\\s");
                holder.mTextViewTimeStart.setText(splited[0]);
                holder.mTextViewAmPmStart.setText(splited[1]);
            }
            if (!Utils.isEmpty(finished_at)) {
                String date = Utils.convertAMPM(finished_at);
                splited = date.split("\\s");
                holder.mTextViewTimeEnd.setText(splited[0]);
                holder.mTextViewAmPmEnd.setText(splited[1]);
            } else {
                holder.mTextViewTimeEnd.setText("");
                holder.mTextViewAmPmEnd.setText("");
            }
            String color = data.getColor();
            if (color.equalsIgnoreCase(Utils.EVENT_COLOR_ORANGE)) {
                holder.mViewVerticalColor.setBackgroundColor(ContextCompat.getColor(mContext, R.color.orange));
            } else
                holder.mViewVerticalColor.setBackgroundColor(ContextCompat.getColor(mContext, R.color.purple));

        }

        holder.mTextViewTitleItem.setText(data.getTitle());
        holder.mTextViewLocation.setText(data.getAddress());
        L.i("data getNote() = " + data.getNote());
        holder.mTextViewDescription.setText(data.getNote());
    }

    @Override
    public int getItemCount() {
        return mEventList.size();
    }


}
