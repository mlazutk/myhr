package com.elantix.myhr.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.elantix.myhr.L;
import com.elantix.myhr.R;
import com.elantix.myhr.activities.ClaimeInfoActivity;
import com.elantix.myhr.activities.RecyclerItemClickListener;
import com.elantix.myhr.activities.Utils;
import com.elantix.myhr.beenResponses.ClaimeHistoryResponce;
import com.elantix.myhr.beenResponses.LeaveHistoryResponce;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by misha on 17.02.2016.
 */
public class ClaimeHistoryAdapter extends RecyclerView.Adapter<ClaimeHistoryAdapter.ViewHolder> {
    private ClaimeHistoryResponce mClaimeHistoryResponce;
    private Context mContext;
    private String oldYear;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mTextViewYear;
        private TextView mTextViewTitle;
        private TextView mTextViewMonth;
        private TextView mTextViewReason;
        private TextView mTextViewStatusLabel;
        private TextView mTextViewPrice;
        private TextView mTextViewDetails;
        private View mViewVerticalColor;

        public ImageView mImageViewStatusLabel;

        public ViewHolder(View v) {
            super(v);
            mTextViewYear = (TextView) v.findViewById(R.id.textView_date);
            mTextViewMonth = (TextView) v.findViewById(R.id.tv_month_claim_history);
            mTextViewTitle = (TextView) v.findViewById(R.id.tv_title_claim_history);
            mTextViewReason = (TextView) v.findViewById(R.id.tv_descr_claim_history);
            mViewVerticalColor = (View) v.findViewById(R.id.v_vertical_colorized);

            mTextViewStatusLabel = (TextView) v.findViewById(R.id.tv_status_claim_history);
            mImageViewStatusLabel = (ImageView) v.findViewById(R.id.imageView_status_label);
            mTextViewPrice = (TextView) v.findViewById(R.id.tv_price_claim_history);
            mTextViewDetails = (TextView) v.findViewById(R.id.tv_details_claim_history);
        }
    }

    public void add(int position, String item) {
        // mHistoryLeaveResponse.add(position, item);
        // notifyItemInserted(position);
    }

    public void remove(String item) {
//        int position = mDataset.indexOf(item);
//        mDataset.remove(position);
//        notifyItemRemoved(position);
    }

    public ClaimeHistoryAdapter(ClaimeHistoryResponce myDataset, Context context) {
        mClaimeHistoryResponce = myDataset;
        mContext = context;
    }

    @Override
    public ClaimeHistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LinearLayout relativeLayoutRoot = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_claim_history_recycler, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(relativeLayoutRoot);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final List<ClaimeHistoryResponce.Data> dataList = mClaimeHistoryResponce.getData();
        final ClaimeHistoryResponce.Data data = dataList.get(position);
        holder.mTextViewTitle.setText(data.getName());
        String splitedSpaceDate[] = data.getCreated_at().split("\\s");
        String splitedStrainDate[] = splitedSpaceDate[0].split("-");
        holder.mTextViewYear.setText(splitedStrainDate[0] + " Year");
        holder.mTextViewReason.setText(data.getDescription());
        holder.mTextViewPrice.setText("$" + data.getAmount());
        //SimpleDateFormat dateFormat = new SimpleDateFormat("MMM d", Locale.ENGLISH);
        Calendar c = Calendar.getInstance();
        c.set(Integer.parseInt(splitedStrainDate[0]), Integer.parseInt(splitedStrainDate[1]),
                Integer.parseInt(splitedStrainDate[2]));

        //holder.mTextViewMonth.setText(dateFormat.format(c.getTime()));

        holder.mTextViewMonth.setText(Utils.theMonth(Integer.parseInt(splitedStrainDate[1])) +" "+ splitedStrainDate[2]);
        holder.mTextViewYear.setVisibility(data.isVisible_year() ? View.VISIBLE : View.GONE);

        String statusLabel = data.getStatus();
        if ("Approved".equalsIgnoreCase(statusLabel)) {
            holder.mViewVerticalColor.setBackgroundColor(ContextCompat.getColor(mContext, R.color.green));
            holder.mImageViewStatusLabel.setImageResource(R.drawable.leave_history_approved);
        } else if ("Rejected".equalsIgnoreCase(statusLabel)) {
            holder.mViewVerticalColor.setBackgroundColor(ContextCompat.getColor(mContext, R.color.red));
            holder.mImageViewStatusLabel.setImageResource(R.drawable.leave_history_rejected);
        } else if ("Pending".equalsIgnoreCase(statusLabel)) {
            holder.mViewVerticalColor.setBackgroundColor(ContextCompat.getColor(mContext, R.color.pending));
            holder.mImageViewStatusLabel.setImageResource(R.drawable.leave_history_pending);
        }
        holder.mTextViewStatusLabel.setText(data.getStatus());

//        new RecyclerItemClickListener(sContext, new RecyclerItemClickListener.OnItemClickListener() {
//            @Override
//            public void onItemClick(View view, int position) {
//                L.i("claim clicked : " + position);
//                final ClaimeHistoryResponce.Data data = chr.getData().get(position);
//                sContext.
//                );
//            }
//        })
//        );

        holder.mTextViewDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                L.i("claim clicked : " + position);
                final ClaimeHistoryResponce.Data data1 = mClaimeHistoryResponce.getData().get(position);
                mContext.startActivity(new Intent(mContext, ClaimeInfoActivity.class)
                        .putExtra(ClaimeInfoActivity.CLAIME_ID_EXTRA, data1.getId()));
            }
        });

    }


    @Override
    public int getItemCount() {
        return mClaimeHistoryResponce.getData().size();
    }
}
