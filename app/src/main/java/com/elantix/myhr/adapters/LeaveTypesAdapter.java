package com.elantix.myhr.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.elantix.myhr.R;
import com.elantix.myhr.activities.Utils;
import com.elantix.myhr.beenResponses.LeaveTypesResponse;

/**
 * Created by misha on 29.01.16.
 */
public class LeaveTypesAdapter extends RecyclerView.Adapter<LeaveTypesAdapter.ViewHolder> {
    private LeaveTypesResponse mLeaveTypesResponse;
    private Context mContext;


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case

        public LinearLayout mLinearLayoutRoot1;
        public LinearLayout mLinearLayoutLeaveName;
        public TextView mTextViewTitle;
        public TextView mTextViewPaidUnpaid;
        public TextView mTextViewQuoterImg;
        public TextView mTextViewQuoterText;
        public ImageView mImageView;

        public ViewHolder(View v) {
            super(v);
            mLinearLayoutRoot1 = (LinearLayout) v;
            mLinearLayoutLeaveName = (LinearLayout) mLinearLayoutRoot1.findViewById(R.id.linearLayout_name_leave);
            mTextViewTitle = (TextView) mLinearLayoutRoot1.findViewById(R.id.textView_leave_history_title);
            mTextViewPaidUnpaid = (TextView) mLinearLayoutRoot1.findViewById(R.id.textView_leave_history_paidUnpaid);
            mImageView = (ImageView) mLinearLayoutRoot1.findViewById(R.id.imageView_leave_period);
            mTextViewQuoterImg = (TextView) mLinearLayoutRoot1.findViewById(R.id.textView_leave_types_quote_ballance_img);
            mTextViewQuoterText = (TextView) mLinearLayoutRoot1.findViewById(R.id.textView_leave_types_quote_ballance_txt);
        }
    }

    public void add(int position, String item) {
        // mHistoryLeaveResponse.add(position, item);
        // notifyItemInserted(position);
    }

    public void remove(String item) {
//        int position = mDataset.indexOf(item);
//        mDataset.remove(position);
//        notifyItemRemoved(position);
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public LeaveTypesAdapter(LeaveTypesResponse myDataset, Context context) {
        mLeaveTypesResponse = myDataset;
        mContext = context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public LeaveTypesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LinearLayout linearLayoutRoot = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_leave_types_recycler, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(linearLayoutRoot);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        LeaveTypesResponse.Data[] data = mLeaveTypesResponse.getData();
        holder.mTextViewTitle.setText(data[position].getLeave_name());
        float quota_ballance = data[position].getQuota_ballance();

        holder.mTextViewQuoterImg.setText(new Utils().cutQuotaBallance(mContext, quota_ballance));
        holder.mTextViewQuoterText.setText(new Utils().cutQuotaBallance(mContext, quota_ballance) + " Days");

        String paidUnpaid = data[position].getPaid();
        if ("-1".equals(paidUnpaid))
            holder.mTextViewPaidUnpaid.setText("Unpaid");
        else {
            holder.mTextViewPaidUnpaid.setText("Paid");
            holder.mImageView.setImageResource(R.drawable.elive_circle_orange);
            if (quota_ballance > 0) {
                holder.mTextViewQuoterImg.setTextColor(ContextCompat.getColor(mContext, R.color.white));
            } else {
                holder.mTextViewQuoterImg.setTextColor(ContextCompat.getColor(mContext, R.color.grey));
                holder.mImageView.setImageResource(R.drawable.elive_circle_white);
                holder.mTextViewQuoterText.setTextColor(ContextCompat.getColor(mContext, R.color.grey));
            }

        }


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mLeaveTypesResponse.getData().length;
    }
}
