package com.elantix.myhr.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import com.elantix.myhr.L;
import com.elantix.myhr.R;
import com.elantix.myhr.beenResponses.LeaveHistoryResponce;

/**
 * Created by misha on 17.02.2016.
 */
public class LeaveHistoryAdapter extends RecyclerView.Adapter<LeaveHistoryAdapter.ViewHolder> {
    private LeaveHistoryResponce mLeaveHistoryResponse;
    private Context mContext;
    private String oldYear;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {

        public RelativeLayout mRelativeLayoutRoot;
        public RelativeLayout mRelativeLayoutMainBody;
        public LinearLayout mLinearLayoutBodyHeader;
        public LinearLayout mLinearLayoutBelowHeader;
        public TextView mTextViewYear;
        public TextView mTextViewTitle;
        public TextView mTextViewPeriod;
        public TextView mTextViewPaidUnpaid;
        public TextView mTextViewReason;
        public TextView mTextViewStatusLabel;

        public ImageView mImageViewPeriod;
        public ImageView mImageViewStatusLabel;

        public ViewHolder(View v) {
            super(v);
            mRelativeLayoutRoot = (RelativeLayout) v;
            mRelativeLayoutMainBody = (RelativeLayout) mRelativeLayoutRoot.findViewById(R.id.rl_main_body);
            mLinearLayoutBodyHeader = (LinearLayout) mRelativeLayoutRoot.findViewById(R.id.ll_images_header);
            mLinearLayoutBelowHeader = (LinearLayout) mRelativeLayoutMainBody.findViewById(R.id.layout_text_body);
            //mLinearLayoutRoot1 = (LinearLayout) v;
            mTextViewYear = (TextView) mRelativeLayoutRoot.findViewById(R.id.textView_date);

            mTextViewPeriod = (TextView) mLinearLayoutBodyHeader.findViewById(R.id.textView_period_leave_history);
            mTextViewStatusLabel = (TextView) mLinearLayoutBodyHeader.findViewById(R.id.textView_status_leave_history);

            mImageViewStatusLabel = (ImageView) mLinearLayoutBodyHeader.findViewById(R.id.imageView_status_label);

            mTextViewTitle = (TextView) mLinearLayoutBelowHeader.findViewById(R.id.textView_title_leave_history);
            mTextViewPaidUnpaid = (TextView) mLinearLayoutBelowHeader.findViewById(R.id.textView_paidUnpaid_leave_history);
            mTextViewReason = (TextView) mLinearLayoutBelowHeader.findViewById(R.id.textView_reason_leave_history);


        }
    }

    public void add(int position, String item) {
        // mHistoryLeaveResponse.add(position, item);
        // notifyItemInserted(position);
    }

    public void remove(String item) {
//        int position = mDataset.indexOf(item);
//        mDataset.remove(position);
//        notifyItemRemoved(position);
    }

    public LeaveHistoryAdapter(LeaveHistoryResponce myDataset, Context context) {
        mLeaveHistoryResponse = myDataset;
        mContext = context;
    }

    @Override
    public LeaveHistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        RelativeLayout relativeLayoutRoot = (RelativeLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_leave_history_recycler, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(relativeLayoutRoot);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final List<LeaveHistoryResponce.Data> dataList = mLeaveHistoryResponse.getDataList();
        holder.mTextViewTitle.setText(dataList.get(position).getLeave_name());
        String splitedStart[] = dataList.get(position).getStart_date().split("-");
        holder.mTextViewYear.setText(splitedStart[0] + " Year");

        holder.mTextViewYear.setVisibility(dataList.get(position).getYearVisibility() == 8 ? View.GONE : View.VISIBLE);


        String statusLabel = dataList.get(position).getStatusLabel();
        if ("Approved".equalsIgnoreCase(statusLabel))
            holder.mImageViewStatusLabel.setImageResource(R.drawable.leave_history_approved);
        else if ("Rejected".equalsIgnoreCase(statusLabel))
            holder.mImageViewStatusLabel.setImageResource(R.drawable.leave_history_rejected);
        else if ("Pending".equalsIgnoreCase(statusLabel))
            holder.mImageViewStatusLabel.setImageResource(R.drawable.leave_history_pending);
        holder.mTextViewReason.setText(dataList.get(position).getReason());
        holder.mTextViewStatusLabel.setText(dataList.get(position).getStatusLabel());


        String startDateFromServer = dataList.get(position).getStart_date();
        String endDateFromServer = dataList.get(position).getEnd_date();
        String startArray[] = startDateFromServer.split("-");
        String endArray[] = endDateFromServer.split("-");

        Calendar c_start = Calendar.getInstance();
        Calendar c_end = Calendar.getInstance();
        c_start.set(Integer.parseInt(startArray[0]), Integer.parseInt(startArray[1]), Integer.parseInt(startArray[2]));
        c_end.set(Integer.parseInt(endArray[0]), Integer.parseInt(endArray[1]), Integer.parseInt(endArray[2]));

        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy", Locale.ENGLISH);

        String startFormatedDate = dateFormat.format(c_start.getTime());
        String endFormatedDate = dateFormat.format(c_end.getTime());
        String startDay[] = startFormatedDate.split(",");
        String endDay[] = endFormatedDate.split(",");

        //count days
        SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date1 = null;
        Date date2 = null;
        try {
            date1 = myFormat.parse(startDateFromServer);
            date2 = myFormat.parse(endDateFromServer);
        } catch (ParseException e) {
            e.printStackTrace();
            return;
        }
        long diff = date2.getTime() - date1.getTime();
        String countDays = String.valueOf(TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
        String titleDays = "";

        if (Integer.parseInt(countDays) > 1) titleDays = "(" + countDays + " Days)";
        else titleDays = "(" + countDays + " Day)";
//=======================
        holder.mTextViewPeriod.setText(startDay[1] + " - " + endDay[1] + " " + titleDays);
        String paidUnpaid = dataList.get(position).getPaid();
        if ("-1".equals(paidUnpaid))
            holder.mTextViewPaidUnpaid.setText("Unpaid");
        else {
            holder.mTextViewPaidUnpaid.setText("Paid");
        }
    }


    private Calendar toCalendar(long timestamp) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar;
    }
    //  L.i("data[position].getLeave_name() = " + data[position].getLeave_name());

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mLeaveHistoryResponse.getDataList().size();
    }
}
