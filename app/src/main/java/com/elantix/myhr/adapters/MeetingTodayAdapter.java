package com.elantix.myhr.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.elantix.myhr.L;
import com.elantix.myhr.R;
import com.elantix.myhr.activities.Utils;
import com.elantix.myhr.beenResponses.LeaveHistoryResponce;
import com.elantix.myhr.beenResponses.MeetingTodayResponce;
import com.elantix.myhr.classes.Data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by misha on 23.02.2016.
 */

public class MeetingTodayAdapter extends RecyclerView.Adapter<MeetingTodayAdapter.ViewHolder> {
    private MeetingTodayResponce mMeetingTodayResponce;
    private Context mContext;

    public class ViewHolder extends RecyclerView.ViewHolder {

        public LinearLayout mLinearLayoutRoot;
        private ScrollView mScrollViewMeeting;
        // private LinearLayout mLinearLayoutWrappedScrollView;
        //private LinearLayout mLinearLayoutMeetingStartDayContainer;

        public LinearLayout mLinearLayoutTimeCont;
        public LinearLayout mLinearLayoutLocDescr;

        private TextView mTextViewTimeStart;
        private TextView mTextViewAmPmStart;
        private TextView mTextViewAmPmEnd;
        private TextView mTextViewTimeEnd;
        private TextView mTextViewLocation;
        private TextView mTextViewDescription;

        public ViewHolder(View v) {
            super(v);
            mLinearLayoutRoot = (LinearLayout) v;
            //mScrollViewMeeting = (ScrollView) mLinearLayoutRoot.findViewById(R.id.meeting_scroll_view);
            //mLinearLayoutWrappedScrollView = (LinearLayout) mScrollViewMeeting.findViewById(R.id.meeting_wrapped_ll_by_scrv);
            // mLinearLayoutMeetingStartDayContainer=(LinearLayout) mLinearLayoutWrappedScrollView.findViewById(R.id.meeting_startday_container);

            mLinearLayoutTimeCont = (LinearLayout) mLinearLayoutRoot.findViewById(R.id.item_meeting_time_container);
            mTextViewTimeStart = (TextView) mLinearLayoutTimeCont.findViewById(R.id.meeting_tv_time_start);
            mTextViewAmPmStart = (TextView) mLinearLayoutTimeCont.findViewById(R.id.meeting_tv_pm_am);
            mTextViewAmPmEnd = (TextView) mLinearLayoutTimeCont.findViewById(R.id.meeting_tv_pm_am_finish);
            mTextViewTimeEnd = (TextView) mLinearLayoutTimeCont.findViewById(R.id.meeting_tv_time_end);
            mTextViewTimeStart.setText("tttttttttt");
            mLinearLayoutLocDescr = (LinearLayout) mLinearLayoutRoot.findViewById(R.id.item_meeting_loc_descr_container);
            mTextViewLocation = (TextView) mLinearLayoutLocDescr.findViewById(R.id.meeting_tv_location);
            mTextViewDescription = (TextView) mLinearLayoutLocDescr.findViewById(R.id.meeting_tv_description);

        }
    }

    public void add(int position, String item) {
        // mHistoryLeaveResponse.add(position, item);
        // notifyItemInserted(position);
    }

    public void remove(String item) {
//        int position = mDataset.indexOf(item);
//        mDataset.remove(position);
//        notifyItemRemoved(position);
    }

    public MeetingTodayAdapter(MeetingTodayResponce myDataset, Context context) {
        mMeetingTodayResponce = myDataset;
        mContext = context;
    }

    @Override
    public MeetingTodayAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_recycler_view_meeting, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(linearLayout);
        return vh;
    }
    // Replace the contents of a view (invoked by the layout manager)

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Data[] data = mMeetingTodayResponce.getData();
        String adress = Utils.getCompleteAddressString(mContext,
                Double.valueOf(data[position].getLat()), Double.valueOf(data[position].getLng()));


        String started = Utils.convertAMPM(data[position].getStarted_at());
        String splitedStarted[] = started.split("\\s");
        holder.mTextViewTimeStart.setText(splitedStarted[0]);
        holder.mTextViewAmPmStart.setText(splitedStarted[1]);
        //--------------------------------------------------
        holder.mTextViewTimeEnd.setText(Utils.convertAMPM(data[position].getFinished_at()));
        String splitedEnded[] = started.split("\\s");
        holder.mTextViewTimeEnd.setText(splitedEnded[0]);
        holder.mTextViewAmPmEnd.setText(splitedEnded[1]);


        holder.mTextViewLocation.setText(adress);

        holder.mTextViewDescription.setText(data[position].getNote());
    }

    @Override
    public int getItemCount() {
        return mMeetingTodayResponce.getData().length;
    }


}
