package com.elantix.myhr.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.elantix.myhr.R;
import com.elantix.myhr.beenResponses.PayslipMonthlyResponce;

import com.elantix.myhr.beenResponses.PayslipMonthlyResponce.Data;
import com.elantix.myhr.classes.MyHRPreferences;

import java.util.List;

import com.elantix.myhr.beenResponses.PayslipMonthlyResponce.Data;
import com.elantix.myhr.beenResponses.PayslipMonthlyResponce.Data.Month;
import com.elantix.myhr.beenResponses.PayslipMonthlyResponce.Data.Month.ContainerObj;

/**
 * Created by misha on 08.03.2016.
 */
public class PayslipMonthAdapter extends RecyclerView.Adapter<PayslipMonthAdapter.ViewHolder> {
    private PayslipMonthlyResponce mPayslipMonthlyResponce;
    private Context mContext;

    public class ViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout mLinearLayout;
        private TextView mTextViewMonthTitle;
        private TextView mTextViewTotalTitle;
        private String mStringUserId;

        public TextView mTextViewDate;
        public TextView mTextViewName;
        public TextView mTextViewTotal;
        public ImageView mImageView;

        public ViewHolder(View v) {
            super(v);
            mStringUserId = MyHRPreferences.getInstance(mContext).getUser().getData().getUserId();
            mLinearLayout = (LinearLayout) v.findViewById(R.id.ll_payslip_list_header);
            mTextViewMonthTitle = (TextView) v.findViewById(R.id.payslip_list_month_title);
            mTextViewTotalTitle = (TextView) v.findViewById(R.id.payslip_list_total_title);

            mTextViewDate = (TextView) v.findViewById(R.id.payslip_types_date);
            mTextViewName = (TextView) v.findViewById(R.id.payslip_types_name);
            mTextViewTotal = (TextView) v.findViewById(R.id.payslip_types_net_pay);

        }
    }

    public void add(int position, String item) {
        // mHistoryLeaveResponse.add(position, item);
        // notifyItemInserted(position);
    }

    public void remove(String item) {
//        int position = mDataset.indexOf(item);
//        mDataset.remove(position);
//        notifyItemRemoved(position);
    }

    public PayslipMonthAdapter(PayslipMonthlyResponce myDataset, Context context) {
        mPayslipMonthlyResponce = myDataset;
        mContext = context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public PayslipMonthAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LinearLayout linearLayoutRoot = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_payslip_list_recycler, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(linearLayoutRoot);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Data data = mPayslipMonthlyResponce.getData();
        final List<Month> monthList = data.getMonthList();
        final Month month = monthList.get(position);
        holder.mTextViewMonthTitle.setText(month.getMonthName() + " " + month.getYear());
        holder.mTextViewTotalTitle.setText("$" + month.getTotalAmount());
        holder.mLinearLayout.setVisibility(month.isVisibleTitle() ? View.VISIBLE : View.GONE);

        String date[] = month.getContainer().getPayment_date().split("/");
        holder.mTextViewDate.setText(date[0]);
        holder.mTextViewName.setText(month.getContainer().getReference());
        holder.mTextViewTotal.setText(month.getContainer().getNet_total());
        //-------------------------------- container-----------------
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mPayslipMonthlyResponce.getData().getMonthList().size();
    }
}
