package com.elantix.myhr;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

import com.elantix.myhr.activities.ClaimFragment;
import com.elantix.myhr.activities.ClaimeInfoActivity;
import com.elantix.myhr.activities.MeetingFragment;
import com.elantix.myhr.activities.NewClaimActivity;
import com.elantix.myhr.activities.PayslipDetailActivity;
import com.elantix.myhr.activities.PayslipFragment;
import com.elantix.myhr.activities.StartEventMeetActivity;
import com.elantix.myhr.activities.TimesheetFragment;
import com.elantix.myhr.beenResponses.BreekTimeResponce;
import com.elantix.myhr.beenResponses.BreektimeFinishResponce;
import com.elantix.myhr.beenResponses.ClaimeHistoryResponce;
import com.elantix.myhr.beenResponses.ClaimeResponce;
import com.elantix.myhr.beenResponses.MeetingLastResponce;
import com.elantix.myhr.beenResponses.MeetingResponce;
import com.elantix.myhr.beenResponses.MeetingTodayResponce;
import com.elantix.myhr.beenResponses.NewClaimResponce;
import com.elantix.myhr.beenResponses.PayslipDetailResponce;
import com.elantix.myhr.beenResponses.PayslipMonthlyResponce;
import com.elantix.myhr.beenResponses.PayslipTypesResponce;
import com.elantix.myhr.beenResponses.StartMeetingResponce;
import com.elantix.myhr.beenResponses.TickResponce;
import com.elantix.myhr.beenResponses.TickStatusResponce;
import com.elantix.myhr.beenResponses.TimeSheetTableResponce;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.elantix.myhr.activities.CreateLeaveActivity;
import com.elantix.myhr.activities.LeaveFragment;
import com.elantix.myhr.activities.FirstLoginActivity;
import com.elantix.myhr.beenResponses.CreateLeaveResponse;
import com.elantix.myhr.beenResponses.EmployeeListResponse;
import com.elantix.myhr.beenResponses.EmployeeResponse;
import com.elantix.myhr.beenResponses.LeaveHistoryResponce;
import com.elantix.myhr.beenResponses.LeaveTypesResponse;
import com.elantix.myhr.beenResponses.LoginResponse;
import com.elantix.myhr.beenResponses.RenewPasswordResponce;

/**
 * Created by misha on 27.01.2016.
 */
public class CallResponseBody<CustomResponseBody extends ResponseBody> implements Callback<ResponseBody> {

    private Class mClazz;
    private Activity mContext;
    private LoginResponse mLoginResponse;
    private LeaveTypesResponse mHistoryLeaveResponse;
    private EmployeeResponse mEmployeeResponse;
    private EmployeeListResponse mEmployeeListResponse;
    private RenewPasswordResponce mRenewPasswordResponce;


    public Class getClazz() {
        return mClazz;
    }

    public void setClazz(Class clazz) {
        this.mClazz = clazz;
    }

    public Context getContext() {
        return mContext;
    }

    public void setContext(Activity context) {
        mContext = context;
    }

    @Override
    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
        String responseBody = null;
        try {
            final ResponseBody body = response.body();
            if (body != null) {
                responseBody = body.string();
            } else {
                L.i("Responce meassage: " + response.message());
                L.i("Responce code: " + response.code());
                L.i("Responce errorBody: " + response.errorBody().string());
                if (mContext != null) {
                    Toast.makeText(mContext, response.errorBody().string(), Toast.LENGTH_LONG).show();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        String stringClass[] = getClazz().toString().split("\\.");
        L.i("responseBody of " + stringClass[4] + " = " + responseBody);
        L.i("--------------------------------- next Responce-------------------");


        if (response.isSuccessful()) {
            Gson gson = new Gson();
            if (getClazz() == EmployeeResponse.class) {
                mEmployeeResponse = gson.fromJson(responseBody, EmployeeResponse.class);
            } else if (getClazz() == EmployeeListResponse.class) {
                mEmployeeListResponse = gson.fromJson(responseBody, EmployeeListResponse.class);
            } else if (getClazz() == CreateLeaveResponse.class) {
                CreateLeaveActivity.PlaceholderFragment.createLeaveCallback(responseBody);
            } else if (getClazz() == LoginResponse.class) {
                FirstLoginActivity.loginCallback(responseBody);
            } else if (getClazz() == LeaveTypesResponse.class) {
                LeaveFragment.fillLeaveTypesList(responseBody);
            } else if (getClazz() == LeaveHistoryResponce.class) {
                LeaveFragment.fillLeaveHistoryListCallback(responseBody);
            } else if (getClazz() == RenewPasswordResponce.class) {
                FirstLoginActivity.renewPasswordCallback(responseBody);
            } else if (getClazz() == StartMeetingResponce.class) {
                StartEventMeetActivity.startMeetingCallback(responseBody);
            } else if (getClazz() == BreekTimeResponce.class) {
                TimesheetFragment.breaktimeCallback(responseBody);
            } else if (getClazz() == BreektimeFinishResponce.class) {
                TimesheetFragment.breaktimeCallback(responseBody);
            } else if (getClazz() == MeetingResponce.class) {
                MeetingFragment.meetingCallback(responseBody);
            } else if (getClazz() == TickResponce.class) {
                TimesheetFragment.tickCallback(responseBody);
            } else if (getClazz() == MeetingTodayResponce.class) {
                MeetingFragment.meetingTodayCallback(responseBody);
            } else if (getClazz() == TickStatusResponce.class) {
                TimesheetFragment.tickStatusCallback(responseBody);
            } else if (getClazz() == TimeSheetTableResponce.class) {
               // TimesheetFragment.timeSheetTableTodayCallback(responseBody);
              //  MeetingFragment.timeSheetTableTodayCallback(responseBody);
            } else if (getClazz() == MeetingLastResponce.class) {
                // MeetingFragment.meetingLastCallback(responseBody);
            } else if (getClazz() == PayslipTypesResponce.class) {
                // PayslipFragment.payslipMonthCallback(responseBody);
            } else if (getClazz() == PayslipMonthlyResponce.class) {
                PayslipFragment.PayslipMonthlyCallback(responseBody);
            } else if (getClazz() == PayslipDetailResponce.class) {
                PayslipDetailActivity.PayslipDetailCallback(responseBody);
            } else if (getClazz() == ClaimeHistoryResponce.class) {
                ClaimFragment.ClaimeHistoryCallback(responseBody);
            } else if (getClazz() == ClaimeResponce.class) {
                ClaimeInfoActivity.ClaimeCallback(responseBody);
            } else if (getClazz() == NewClaimResponce.class) {
                //NewClaimActivity.NewClaimeCallback(responseBody);
                ClaimFragment.NewClaimeCallback(responseBody);
            }
        } else {
            // Toast.makeText(CallResponseBody.this, "", Toast.LENGTH_SHORT).show();
            L.i("response.isSuccess() = " + response.isSuccessful());
        }
    }
    @Override
    public void onFailure(Call<ResponseBody> call, Throwable t) {
        StringWriter errors = new StringWriter();
        t.printStackTrace(new PrintWriter(errors));
        L.i("onFailure " + errors.toString());
    }
}
