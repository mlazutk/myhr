package com.elantix.myhr;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.elantix.myhr.activities.ClaimFragment;
import com.elantix.myhr.activities.LeaveFragment;
import com.elantix.myhr.activities.MainContainerActivity;
import com.elantix.myhr.activities.MeetingFragment;
import com.elantix.myhr.activities.PayslipFragment;
import com.elantix.myhr.activities.SettingsFragment;
import com.elantix.myhr.activities.TimesheetFragment;

/**
 * Created by misha on 13.02.2016.
 */
public class TabFragment extends Fragment {
    private TextView mTextViewItemTimesheet;
    private TextView mTextViewItemMeeting;
    private TextView mTextViewItemLeave;
    private TextView mTextViewItemPayslip;
    private TextView mTextViewItemClaime;
    private TextView mTextViewItemSettings;
    public static TabLayout tabLayout;
    public static ViewPager viewPager;
    public static int int_items = 6;
    private NavigationView mNavigationView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        /**
         *Inflate tab_layout and setup Views.
         */
        mNavigationView= MainContainerActivity.mNavigationView;
        View x = inflater.inflate(R.layout.tab_layout, null);
//        View view_menu = inflater.inflate(R.layout.menu_layout, null);
//        mTextViewItemTimesheet = (TextView) view_menu.findViewById(R.id.item_timesheet);
//        mTextViewItemMeeting = (TextView) view_menu.findViewById(R.id.item_meeting);
//        mTextViewItemLeave = (TextView) view_menu.findViewById(R.id.item_leave);
//        mTextViewItemPayslip = (TextView) view_menu.findViewById(R.id.item_paislip);
//        mTextViewItemClaime = (TextView) view_menu.findViewById(R.id.item_claime);
//        mTextViewItemSettings = (TextView) view_menu.findViewById(R.id.item_settings);

        tabLayout = (TabLayout) x.findViewById(R.id.tabs);
        viewPager = (ViewPager) x.findViewById(R.id.viewpager);
        viewPager.setOffscreenPageLimit(0);

        /**
         *Set an Apater for the View Pager
         */
        final MyAdapter pagerAdapter = new MyAdapter(getChildFragmentManager());
        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                //L.i("onPageScrolled "+ position);
            }

            @Override
            public void onPageSelected(int position) {
                final Menu menu = mNavigationView.getMenu();
                final MenuItem item = menu.getItem(position);
                final MenuItem menuItem = item.setChecked(true);
               // L.i("MenuItem: " + menuItem.getTitle());
            //    colorizeItems(position);
                //pagerAdapter.notifyDataSetChanged();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        /**
         * Now , this is a workaround ,
         * The setupWithViewPager dose't works without the runnable .
         * Maybe a Support Library Bug .
         */

        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                tabLayout.setupWithViewPager(viewPager);
            }
        });

        return x;

    }
//
//    public void colorizeItems(int positions) {
//        mTextViewItemTimesheet.setTextColor(ContextCompat.getColor(getActivity(), (R.color.grey)));
//        mTextViewItemMeeting.setTextColor(ContextCompat.getColor(getActivity(), (R.color.grey)));
//        mTextViewItemLeave.setTextColor(ContextCompat.getColor(getActivity(), (R.color.grey)));
//        mTextViewItemPayslip.setTextColor(ContextCompat.getColor(getActivity(), (R.color.grey)));
//        mTextViewItemClaime.setTextColor(ContextCompat.getColor(getActivity(), (R.color.grey)));
//        switch (positions) {
//            case 0:
//                mTextViewItemTimesheet.setTextColor(ContextCompat.getColor(getActivity(), (R.color.green)));
//                break;
//            case 1:
//                mTextViewItemMeeting.setTextColor(ContextCompat.getColor(getActivity(), (R.color.green)));
//                break;
//            case 2:
//                mTextViewItemLeave.setTextColor(ContextCompat.getColor(getActivity(), (R.color.green)));
//                break;
//            case 3:
//                mTextViewItemPayslip.setTextColor(ContextCompat.getColor(getActivity(), (R.color.green)));
//                break;
//            case 4:
//                mTextViewItemClaime.setTextColor(ContextCompat.getColor(getActivity(), (R.color.green)));
//                break;
//            case 5:
//                mTextViewItemSettings.setTextColor(ContextCompat.getColor(getActivity(), (R.color.green)));
//                break;
//            default:
//                break;
//        }
//
//
//    }

    class MyAdapter extends FragmentPagerAdapter {

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        /**
         * Return fragment with respect to Position .
         */

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new TimesheetFragment();
                case 1:
                    return new MeetingFragment();
                case 2:
                    return new LeaveFragment();
                case 3:
                    return new PayslipFragment();
                case 4:
                    return new ClaimFragment();
                case 5:
                    return new SettingsFragment();
            }
            return null;
        }

        @Override
        public int getCount() {

            return int_items;

        }

        /**
         * This method returns the title of the tab according to the position.
         */

        @Override
        public CharSequence getPageTitle(int position) {

            switch (position) {
                case 0:
                    return "eTimesheet";
                case 1:
                    return "eMeeting";
                case 2:
                    return "eLeave";
                case 3:
                    return "Payslip";
                case 4:
                    return "eClaim";
                case 5:
                    return "Settings";

            }
            return null;
        }
    }

}
