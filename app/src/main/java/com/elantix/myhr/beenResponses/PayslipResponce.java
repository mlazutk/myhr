package com.elantix.myhr.beenResponses;

/**
 * Created by misha on 04.03.2016.
 */
public class PayslipResponce {
    private boolean status;

    public boolean isStatus() {
        return status;
    }


    public class Data {
        private String id;

        private String payment_date;

        private String salary_period;

        private String payment_mode;

        private String net_total;

        private String reference;

        private String employee_name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getPayment_date() {
            return payment_date;
        }

        public void setPayment_date(String payment_date) {
            this.payment_date = payment_date;
        }

        public String getSalary_period() {
            return salary_period;
        }

        public void setSalary_period(String salary_period) {
            this.salary_period = salary_period;
        }

        public String getPayment_mode() {
            return payment_mode;
        }

        public void setPayment_mode(String payment_mode) {
            this.payment_mode = payment_mode;
        }

        public String getNet_total() {
            return net_total;
        }

        public void setNet_total(String net_total) {
            this.net_total = net_total;
        }

        public String getReference() {
            return reference;
        }

        public void setReference(String reference) {
            this.reference = reference;
        }

        public String getEmployee_name() {
            return employee_name;
        }

        public void setEmployee_name(String employee_name) {
            this.employee_name = employee_name;
        }

        @Override
        public String toString() {
            return "ClassPojo [id = " + id + ", payment_date = " + payment_date + ", salary_period = " + salary_period + ", payment_mode = " + payment_mode + ", net_total = " + net_total + ", reference = " + reference + ", employee_name = " + employee_name + "]";
        }
    }

}
