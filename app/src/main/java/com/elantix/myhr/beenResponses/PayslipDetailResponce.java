package com.elantix.myhr.beenResponses;

/**
 * Created by misha on 04.03.2016.
 */
public class PayslipDetailResponce {
    private boolean status;

    public boolean isStatus() {
        return status;
    }

    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {
        private String ot_hours;

        private String salary_period;

        private String worked_hours;

        private String net_total;

        private String has_ot;

        private String employee_name;

        private String id;

        private String ot_amount;

        private String hourly_basic;

        private String salary_start_date;

        private String salary_end_date;

        private String employer_cpf;

        private String employee_id;

        private String other_payment_total;

        private String payment_date;

        private String has_cpf;

        private String status;

        private String employer_name;

        private String reference;

        private String deduction_total;

        private String payment_mode;

        private String ot_factor;

        private String aw_cpf_total;

        private String monthly_rate;

        private String ow_cpf_total;

        private String allowance_total;

        public String getOt_hours() {
            return ot_hours;
        }

        public void setOt_hours(String ot_hours) {
            this.ot_hours = ot_hours;
        }

        public String getSalary_period() {
            return salary_period;
        }

        public void setSalary_period(String salary_period) {
            this.salary_period = salary_period;
        }

        public String getWorked_hours() {
            return worked_hours;
        }

        public void setWorked_hours(String worked_hours) {
            this.worked_hours = worked_hours;
        }

        public String getNet_total() {
            return net_total;
        }

        public void setNet_total(String net_total) {
            this.net_total = net_total;
        }

        public String getHas_ot() {
            return has_ot;
        }

        public void setHas_ot(String has_ot) {
            this.has_ot = has_ot;
        }

        public String getEmployee_name() {
            return employee_name;
        }

        public void setEmployee_name(String employee_name) {
            this.employee_name = employee_name;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getOt_amount() {
            return ot_amount;
        }

        public void setOt_amount(String ot_amount) {
            this.ot_amount = ot_amount;
        }

        public String getHourly_basic() {
            return hourly_basic;
        }

        public void setHourly_basic(String hourly_basic) {
            this.hourly_basic = hourly_basic;
        }

        public String getSalary_start_date() {
            return salary_start_date;
        }

        public void setSalary_start_date(String salary_start_date) {
            this.salary_start_date = salary_start_date;
        }

        public String getSalary_end_date() {
            return salary_end_date;
        }

        public void setSalary_end_date(String salary_end_date) {
            this.salary_end_date = salary_end_date;
        }

        public String getEmployer_cpf() {
            return employer_cpf;
        }

        public void setEmployer_cpf(String employer_cpf) {
            this.employer_cpf = employer_cpf;
        }

        public String getEmployee_id() {
            return employee_id;
        }

        public void setEmployee_id(String employee_id) {
            this.employee_id = employee_id;
        }

        public String getOther_payment_total() {
            return other_payment_total;
        }

        public void setOther_payment_total(String other_payment_total) {
            this.other_payment_total = other_payment_total;
        }

        public String getPayment_date() {
            return payment_date;
        }

        public void setPayment_date(String payment_date) {
            this.payment_date = payment_date;
        }

        public String getHas_cpf() {
            return has_cpf;
        }

        public void setHas_cpf(String has_cpf) {
            this.has_cpf = has_cpf;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getEmployer_name() {
            return employer_name;
        }

        public void setEmployer_name(String employer_name) {
            this.employer_name = employer_name;
        }

        public String getReference() {
            return reference;
        }

        public void setReference(String reference) {
            this.reference = reference;
        }

        public String getDeduction_total() {
            return deduction_total;
        }

        public void setDeduction_total(String deduction_total) {
            this.deduction_total = deduction_total;
        }

        public String getPayment_mode() {
            return payment_mode;
        }

        public void setPayment_mode(String payment_mode) {
            this.payment_mode = payment_mode;
        }

        public String getOt_factor() {
            return ot_factor;
        }

        public void setOt_factor(String ot_factor) {
            this.ot_factor = ot_factor;
        }

        public String getAw_cpf_total() {
            return aw_cpf_total;
        }

        public void setAw_cpf_total(String aw_cpf_total) {
            this.aw_cpf_total = aw_cpf_total;
        }

        public String getMonthly_rate() {
            return monthly_rate;
        }

        public void setMonthly_rate(String monthly_rate) {
            this.monthly_rate = monthly_rate;
        }

        public String getOw_cpf_total() {
            return ow_cpf_total;
        }

        public void setOw_cpf_total(String ow_cpf_total) {
            this.ow_cpf_total = ow_cpf_total;
        }

        public String getAllowance_total() {
            return allowance_total;
        }

        public void setAllowance_total(String allowance_total) {
            this.allowance_total = allowance_total;
        }

        @Override
        public String toString() {
            return "ClassPojo [ot_hours = " + ot_hours + ", salary_period = " + salary_period + ", worked_hours = " + worked_hours + ", net_total = " + net_total + ", has_ot = " + has_ot + ", employee_name = " + employee_name + ", id = " + id + ", ot_amount = " + ot_amount + ", hourly_basic = " + hourly_basic + ", salary_start_date = " + salary_start_date + ", salary_end_date = " + salary_end_date + ", employer_cpf = " + employer_cpf + ", employee_id = " + employee_id + ", other_payment_total = " + other_payment_total + ", payment_date = " + payment_date + ", has_cpf = " + has_cpf + ", status = " + status + ", employer_name = " + employer_name + ", reference = " + reference + ", deduction_total = " + deduction_total + ", payment_mode = " + payment_mode + ", ot_factor = " + ot_factor + ", aw_cpf_total = " + aw_cpf_total + ", monthly_rate = " + monthly_rate + ", ow_cpf_total = " + ow_cpf_total + ", allowance_total = " + allowance_total + "]";
        }
    }

}
