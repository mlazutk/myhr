package com.elantix.myhr.beenResponses;

/**
 * Created by misha on 18.02.2016.
 */
public class AuthorizedUser {
    private User_info user_info;

    private boolean status;

    private String domain;

    private Data data;

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public User_info getUser_info() {
        return user_info;
    }

    public void setUser_info(User_info user_info) {
        this.user_info = user_info;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "AuthorizedUser [user_info = " + user_info + ", status = " + status + ", data = " + data + "]";
    }

    public class Data {
        private String userId;

        private String apiKey;

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getApiKey() {
            return apiKey;
        }

        public void setApiKey(String apiKey) {
            this.apiKey = apiKey;
        }

        @Override
        public String toString() {
            return "Data [userId = " + userId + ", apiKey = " + apiKey + "]";
        }
    }

    public class User_info {
        private String part_time;

        private String bank_name;

        private String probation_end_date;

        private String designation_id;

        private String address1;

        private String address2;

        private String employer_cpf_factor;

        private String employee_cpf_factor;

        private String has_contract;

        private String job_status;

        private String photo_path;

        private String has_ot;

        private String contract_duration;

        private String pass_type;

        private String city;

        private String basic_hourly_rate;

        private String first_name;

        private String probation_start_date;

        private String probation_duration;

        private String company_name;

        private String branch_code;

        private String gender;

        private String employee_id;

        private String salary_payment_dates;

        private String branch_name;

        private String responsibility;

        private String has_cpf;

        private String employement_start_date;

        private String date_of_birth;

        private String same_as_salary_period;

        private String medical_benefit;

        private String postcode;

        private String bank_code;

        private String bank_branch_name;

        private String account_name;

        private String country;

        private String schedual_name;

        private String email;

        private String last_name;

        private String ot_payment_dates;

        private String holiday_rate_factor;

        private String monthly_rate;

        private String has_probation;

        private String work_permit_no;

        private String salary_period;

        private String fin;

        private String account_no;

        private String id;

        private String ic;

        private String duty;

        private String name;

        private String ot_period_detail;

        private String schedual_id;

        private String user_id;

        private String branch_id;

        private String designation_name;

        private String ot_period;

        private String hourly_payment;

        private String salary_period_detail;

        private String permit_expire_date;

        private String holiday_payment_type;

        private String nationality;

        private String hand_phone;

        private String ot_factor;

        private String passport;

        private String medical_examination_fee_claim;

        private String telephone;

        private String holiday_hourly_rate;

        private String notice_period;

        public String getPart_time() {
            return part_time;
        }

        public void setPart_time(String part_time) {
            this.part_time = part_time;
        }

        public String getBank_name() {
            return bank_name;
        }

        public void setBank_name(String bank_name) {
            this.bank_name = bank_name;
        }

        public String getProbation_end_date() {
            return probation_end_date;
        }

        public void setProbation_end_date(String probation_end_date) {
            this.probation_end_date = probation_end_date;
        }

        public String getDesignation_id() {
            return designation_id;
        }

        public void setDesignation_id(String designation_id) {
            this.designation_id = designation_id;
        }

        public String getAddress1() {
            return address1;
        }

        public void setAddress1(String address1) {
            this.address1 = address1;
        }

        public String getAddress2() {
            return address2;
        }

        public void setAddress2(String address2) {
            this.address2 = address2;
        }

        public String getEmployer_cpf_factor() {
            return employer_cpf_factor;
        }

        public void setEmployer_cpf_factor(String employer_cpf_factor) {
            this.employer_cpf_factor = employer_cpf_factor;
        }

        public String getEmployee_cpf_factor() {
            return employee_cpf_factor;
        }

        public void setEmployee_cpf_factor(String employee_cpf_factor) {
            this.employee_cpf_factor = employee_cpf_factor;
        }

        public String getHas_contract() {
            return has_contract;
        }

        public void setHas_contract(String has_contract) {
            this.has_contract = has_contract;
        }

        public String getJob_status() {
            return job_status;
        }

        public void setJob_status(String job_status) {
            this.job_status = job_status;
        }

        public String getPhoto_path() {
            return photo_path;
        }

        public void setPhoto_path(String photo_path) {
            this.photo_path = photo_path;
        }

        public String getHas_ot() {
            return has_ot;
        }

        public void setHas_ot(String has_ot) {
            this.has_ot = has_ot;
        }

        public String getContract_duration() {
            return contract_duration;
        }

        public void setContract_duration(String contract_duration) {
            this.contract_duration = contract_duration;
        }

        public String getPass_type() {
            return pass_type;
        }

        public void setPass_type(String pass_type) {
            this.pass_type = pass_type;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getBasic_hourly_rate() {
            return basic_hourly_rate;
        }

        public void setBasic_hourly_rate(String basic_hourly_rate) {
            this.basic_hourly_rate = basic_hourly_rate;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getProbation_start_date() {
            return probation_start_date;
        }

        public void setProbation_start_date(String probation_start_date) {
            this.probation_start_date = probation_start_date;
        }

        public String getProbation_duration() {
            return probation_duration;
        }

        public void setProbation_duration(String probation_duration) {
            this.probation_duration = probation_duration;
        }

        public String getCompany_name() {
            return company_name;
        }

        public void setCompany_name(String company_name) {
            this.company_name = company_name;
        }

        public String getBranch_code() {
            return branch_code;
        }

        public void setBranch_code(String branch_code) {
            this.branch_code = branch_code;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getEmployee_id() {
            return employee_id;
        }

        public void setEmployee_id(String employee_id) {
            this.employee_id = employee_id;
        }

        public String getSalary_payment_dates() {
            return salary_payment_dates;
        }

        public void setSalary_payment_dates(String salary_payment_dates) {
            this.salary_payment_dates = salary_payment_dates;
        }

        public String getBranch_name() {
            return branch_name;
        }

        public void setBranch_name(String branch_name) {
            this.branch_name = branch_name;
        }

        public String getResponsibility() {
            return responsibility;
        }

        public void setResponsibility(String responsibility) {
            this.responsibility = responsibility;
        }

        public String getHas_cpf() {
            return has_cpf;
        }

        public void setHas_cpf(String has_cpf) {
            this.has_cpf = has_cpf;
        }

        public String getEmployement_start_date() {
            return employement_start_date;
        }

        public void setEmployement_start_date(String employement_start_date) {
            this.employement_start_date = employement_start_date;
        }

        public String getDate_of_birth() {
            return date_of_birth;
        }

        public void setDate_of_birth(String date_of_birth) {
            this.date_of_birth = date_of_birth;
        }

        public String getSame_as_salary_period() {
            return same_as_salary_period;
        }

        public void setSame_as_salary_period(String same_as_salary_period) {
            this.same_as_salary_period = same_as_salary_period;
        }

        public String getMedical_benefit() {
            return medical_benefit;
        }

        public void setMedical_benefit(String medical_benefit) {
            this.medical_benefit = medical_benefit;
        }

        public String getPostcode() {
            return postcode;
        }

        public void setPostcode(String postcode) {
            this.postcode = postcode;
        }

        public String getBank_code() {
            return bank_code;
        }

        public void setBank_code(String bank_code) {
            this.bank_code = bank_code;
        }

        public String getBank_branch_name() {
            return bank_branch_name;
        }

        public void setBank_branch_name(String bank_branch_name) {
            this.bank_branch_name = bank_branch_name;
        }

        public String getAccount_name() {
            return account_name;
        }

        public void setAccount_name(String account_name) {
            this.account_name = account_name;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getSchedual_name() {
            return schedual_name;
        }

        public void setSchedual_name(String schedual_name) {
            this.schedual_name = schedual_name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getOt_payment_dates() {
            return ot_payment_dates;
        }

        public void setOt_payment_dates(String ot_payment_dates) {
            this.ot_payment_dates = ot_payment_dates;
        }

        public String getHoliday_rate_factor() {
            return holiday_rate_factor;
        }

        public void setHoliday_rate_factor(String holiday_rate_factor) {
            this.holiday_rate_factor = holiday_rate_factor;
        }

        public String getMonthly_rate() {
            return monthly_rate;
        }

        public void setMonthly_rate(String monthly_rate) {
            this.monthly_rate = monthly_rate;
        }

        public String getHas_probation() {
            return has_probation;
        }

        public void setHas_probation(String has_probation) {
            this.has_probation = has_probation;
        }

        public String getWork_permit_no() {
            return work_permit_no;
        }

        public void setWork_permit_no(String work_permit_no) {
            this.work_permit_no = work_permit_no;
        }

        public String getSalary_period() {
            return salary_period;
        }

        public void setSalary_period(String salary_period) {
            this.salary_period = salary_period;
        }

        public String getFin() {
            return fin;
        }

        public void setFin(String fin) {
            this.fin = fin;
        }

        public String getAccount_no() {
            return account_no;
        }

        public void setAccount_no(String account_no) {
            this.account_no = account_no;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getIc() {
            return ic;
        }

        public void setIc(String ic) {
            this.ic = ic;
        }

        public String getDuty() {
            return duty;
        }

        public void setDuty(String duty) {
            this.duty = duty;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getOt_period_detail() {
            return ot_period_detail;
        }

        public void setOt_period_detail(String ot_period_detail) {
            this.ot_period_detail = ot_period_detail;
        }

        public String getSchedual_id() {
            return schedual_id;
        }

        public void setSchedual_id(String schedual_id) {
            this.schedual_id = schedual_id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getBranch_id() {
            return branch_id;
        }

        public void setBranch_id(String branch_id) {
            this.branch_id = branch_id;
        }

        public String getDesignation_name() {
            return designation_name;
        }

        public void setDesignation_name(String designation_name) {
            this.designation_name = designation_name;
        }

        public String getOt_period() {
            return ot_period;
        }

        public void setOt_period(String ot_period) {
            this.ot_period = ot_period;
        }

        public String getHourly_payment() {
            return hourly_payment;
        }

        public void setHourly_payment(String hourly_payment) {
            this.hourly_payment = hourly_payment;
        }

        public String getSalary_period_detail() {
            return salary_period_detail;
        }

        public void setSalary_period_detail(String salary_period_detail) {
            this.salary_period_detail = salary_period_detail;
        }

        public String getPermit_expire_date() {
            return permit_expire_date;
        }

        public void setPermit_expire_date(String permit_expire_date) {
            this.permit_expire_date = permit_expire_date;
        }

        public String getHoliday_payment_type() {
            return holiday_payment_type;
        }

        public void setHoliday_payment_type(String holiday_payment_type) {
            this.holiday_payment_type = holiday_payment_type;
        }

        public String getNationality() {
            return nationality;
        }

        public void setNationality(String nationality) {
            this.nationality = nationality;
        }

        public String getHand_phone() {
            return hand_phone;
        }

        public void setHand_phone(String hand_phone) {
            this.hand_phone = hand_phone;
        }

        public String getOt_factor() {
            return ot_factor;
        }

        public void setOt_factor(String ot_factor) {
            this.ot_factor = ot_factor;
        }

        public String getPassport() {
            return passport;
        }

        public void setPassport(String passport) {
            this.passport = passport;
        }

        public String getMedical_examination_fee_claim() {
            return medical_examination_fee_claim;
        }

        public void setMedical_examination_fee_claim(String medical_examination_fee_claim) {
            this.medical_examination_fee_claim = medical_examination_fee_claim;
        }

        public String getTelephone() {
            return telephone;
        }

        public void setTelephone(String telephone) {
            this.telephone = telephone;
        }

        public String getHoliday_hourly_rate() {
            return holiday_hourly_rate;
        }

        public void setHoliday_hourly_rate(String holiday_hourly_rate) {
            this.holiday_hourly_rate = holiday_hourly_rate;
        }

        public String getNotice_period() {
            return notice_period;
        }

        public void setNotice_period(String notice_period) {
            this.notice_period = notice_period;
        }
    }

}
