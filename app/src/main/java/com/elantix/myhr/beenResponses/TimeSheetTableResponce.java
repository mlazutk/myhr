package com.elantix.myhr.beenResponses;

import com.elantix.myhr.classes.Data;

import java.util.List;

/**
 * Created by misha on 01.03.2016.
 */
public class TimeSheetTableResponce {

    private boolean status;
    private List<Data> data;

    public boolean isStatus() {
        return status;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }
}
