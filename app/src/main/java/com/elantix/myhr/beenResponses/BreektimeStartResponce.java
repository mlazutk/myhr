package com.elantix.myhr.beenResponses;

/**
 * Created by misha on 23.02.2016.
 */
public class BreektimeStartResponce {

    private boolean status;
    private Data[] data;

    public boolean isStatus() {
        return status;
    }

    public Data[] getData() {
        return data;
    }

    public class Data {
        private int id;
        private int employee_id;
        private String started_at;
        private String finished_at;
        private String lat;
        private String lng;
        private String status;
        private String address;
        private String note;

        public int getId() {
            return id;
        }

        public int getEmployee_id() {
            return employee_id;
        }

        public String getStarted_at() {
            return started_at;
        }

        public String getFinished_at() {
            return finished_at;
        }

        public String getLat() {
            return lat;
        }

        public String getLng() {
            return lng;
        }

        public String getStatus() {
            return status;
        }

        public String getAddress() {
            return address;
        }

        public String getNote() {
            return note;
        }
    }


}
