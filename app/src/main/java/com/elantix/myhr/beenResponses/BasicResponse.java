package com.elantix.myhr.beenResponses;

public abstract class BasicResponse<D extends BasicResponse.BasicData> {

    private boolean status;
    private D data;

    public boolean isStatus() {
        return status;
    }

    public D getData() {
        return data;
    }


    public abstract class BasicData {


    }

}
