package com.elantix.myhr.beenResponses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by misha on 26.01.2016.
 */
public class ErrorResponse {
    @SerializedName("ErrorMessage")
    private String mErrorMessage;
    // Other fields

    public String getErrorMessage() {
        return mErrorMessage;
    }
    // Other methods
}
