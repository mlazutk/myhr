package com.elantix.myhr.beenResponses;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by misha on 17.02.2016.
 */
public class LeaveHistoryResponce {
    private boolean status;

    public boolean isStatus() {
        return status;
    }

    public List<Data> data;

    public List<Data> getDataList() {
        return data;
    }

    public void setDataList(List<Data> dataList) {
        data = dataList;
    }

    public class Data {
        int id;
        String name;
        String leave_name;
        String start_date;
        String end_date;
        String reason;
        String status;
        String statusLabel;
        // String yearTitle;
        int yearVisibility;
        String unpaid = "-1";
        String paid = "-1";


        public int getYearVisibility() {
            return yearVisibility;
        }

        public void setYearVisibility(int yearVisibility) {
            this.yearVisibility = yearVisibility;
        }

        public int getYearTitle() {
            return yearVisibility;
        }

        public void setYearTitle(int yearTitle) {
            this.yearVisibility = yearTitle;
        }

        public void setId(int id) {
            this.id = id;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setLeave_name(String leave_name) {
            this.leave_name = leave_name;
        }

        public void setStart_date(String start_date) {
            this.start_date = start_date;
        }

        public void setEnd_date(String end_date) {
            this.end_date = end_date;
        }

        public void setReason(String reason) {
            this.reason = reason;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public void setStatusLabel(String statusLabel) {
            this.statusLabel = statusLabel;
        }

        public void setUnpaid(String unpaid) {
            this.unpaid = unpaid;
        }

        public void setPaid(String paid) {
            this.paid = paid;
        }

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getLeave_name() {
            return leave_name;
        }

        public String getStart_date() {
            return start_date;
        }

        public String getEnd_date() {
            return end_date;
        }

        public String getReason() {
            return reason;
        }

        public String getStatus() {
            return status;
        }

        public String getStatusLabel() {
            return statusLabel;
        }

        public String getUnpaid() {
            return unpaid;
        }

        public String getPaid() {
            return paid;
        }

        @Override
        public String toString() {
            return "Data{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", leave_name='" + leave_name + '\'' +
                    ", start_date='" + start_date + '\'' +
                    ", end_date='" + end_date + '\'' +
                    ", reason='" + reason + '\'' +
                    ", status='" + status + '\'' +
                    ", statusLabel='" + statusLabel + '\'' +
                    ", yearVisibility=" + yearVisibility +
                    ", unpaid='" + unpaid + '\'' +
                    ", paid='" + paid + '\'' +
                    '}';
        }
    }
}
