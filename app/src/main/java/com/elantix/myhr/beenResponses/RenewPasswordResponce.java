package com.elantix.myhr.beenResponses;

/**
 * Created by misha on 19.02.2016.
 */
public class RenewPasswordResponce {

    private boolean status;

    private Data data;

    public boolean isStatus() {
        return status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ClassPojo [status = " + status + ", data = " + data + "]";
    }


    class Data {
        private String userId;

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        @Override
        public String toString() {
            return "ClassPojo [userId = " + userId + "]";
        }
    }

}
