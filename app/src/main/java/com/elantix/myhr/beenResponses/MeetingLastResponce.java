package com.elantix.myhr.beenResponses;

/**
 * Created by misha on 01.03.2016.
 */
public class MeetingLastResponce {
    private boolean status;
    private Data data;

    public boolean isStatus() {
        return status;
    }

    public Data getData() {
        return data;
    }

    public class Data {
        private String status;// "progress" or finished

        public String getStatus() {
            return status;
        }
    }
}
