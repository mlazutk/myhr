package com.elantix.myhr.beenResponses;

import java.util.List;

/**
 * Created by misha on 11.03.2016.
 */
public class ClaimeHistoryResponce {

    private boolean status;
    private List<Data> data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public class Data {
        private String photos;

        private String amount;

        private String id;

        private String status;

        private String description;

        private String name;

        private String created_at;

        private String employee_id;

        private String employee_name;

        private boolean visible_year;

        public boolean isVisible_year() {
            return visible_year;
        }

        public void setVisible_year(boolean visible_year) {
            this.visible_year = visible_year;
        }

        public String getPhotos() {
            return photos;
        }

        public void setPhotos(String photos) {
            this.photos = photos;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getEmployee_id() {
            return employee_id;
        }

        public void setEmployee_id(String employee_id) {
            this.employee_id = employee_id;
        }

        public String getEmployee_name() {
            return employee_name;
        }

        public void setEmployee_name(String employee_name) {
            this.employee_name = employee_name;
        }

        @Override
        public String toString() {
            return "ClassPojo [photos = " + photos + ", amount = " + amount + ", id = " + id + ", status = " + status + ", description = " + description + ", name = " + name + ", created_at = " + created_at + ", employee_id = " + employee_id + ", employee_name = " + employee_name + "]";
        }
    }


}
