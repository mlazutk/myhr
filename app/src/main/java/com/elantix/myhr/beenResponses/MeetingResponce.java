package com.elantix.myhr.beenResponses;

import com.elantix.myhr.classes.Data;

/**
 * Created by misha on 23.02.2016.
 */
public class MeetingResponce {
    private boolean status;
    private Data data;

    public boolean isStatus() {
        return status;
    }

    public Data getData() {
        return data;
    }

}
