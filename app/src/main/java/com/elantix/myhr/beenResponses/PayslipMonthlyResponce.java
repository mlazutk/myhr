package com.elantix.myhr.beenResponses;

import com.elantix.myhr.classes.Data;

import java.util.List;

/**
 * Created by misha on 03.03.2016.
 */
public class PayslipMonthlyResponce {
    private boolean status;
    private Data data;

    public boolean isStatus() {
        return status;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Data getData() {
        return data;
    }

    public class Data {
        private List<Month> mMonthList;

        public List<Month> getMonthList() {
            return mMonthList;
        }

        public void setMonthList(List<Month> monthList) {
            mMonthList = monthList;
        }

        public class Month {
            String totalAmount;
            String year;
            String month;
            String monthName;
            boolean isVisibleTitle;
            ContainerObj container;

            public boolean isVisibleTitle() {
                return isVisibleTitle;
            }

            public void setIsVisibleTitle(boolean isVisibleTitle) {
                this.isVisibleTitle = isVisibleTitle;
            }

            public String getTotalAmount() {
                return totalAmount;
            }

            public void setTotalAmount(String totalAmount) {
                this.totalAmount = totalAmount;
            }

            public String getYear() {
                return year;
            }

            public void setYear(String year) {
                this.year = year;
            }

            public String getMonth() {
                return month;
            }

            public void setMonth(String month) {
                this.month = month;
            }

            public String getMonthName() {
                return monthName;
            }

            public void setMonthName(String monthName) {
                this.monthName = monthName;
            }

            public ContainerObj getContainer() {
                return container;
            }

            public void setContainer(ContainerObj container) {
                this.container = container;
            }

            public class ContainerObj {
                String id;
                String payment_date;
                String employee_name;
                String salary_period;
                String net_total;
                String payment_mode;
                String reference;

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getPayment_date() {
                    return payment_date;
                }

                public void setPayment_date(String payment_date) {
                    this.payment_date = payment_date;
                }

                public String getEmployee_name() {
                    return employee_name;
                }

                public void setEmployee_name(String employee_name) {
                    this.employee_name = employee_name;
                }

                public String getSalary_period() {
                    return salary_period;
                }

                public void setSalary_period(String salary_period) {
                    this.salary_period = salary_period;
                }

                public String getNet_total() {
                    return net_total;
                }

                public void setNet_total(String net_total) {
                    this.net_total = net_total;
                }

                public String getPayment_mode() {
                    return payment_mode;
                }

                public void setPayment_mode(String payment_mode) {
                    this.payment_mode = payment_mode;
                }

                public String getReference() {
                    return reference;
                }

                public void setReference(String reference) {
                    this.reference = reference;
                }
            }
        }
    }

}
