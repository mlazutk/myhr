package com.elantix.myhr.beenResponses;

/**
 * Created by misha on 27.01.2016.
 */
public class LoginResponse  {
    private boolean status;
    private Data data;

    class Data {
        private int userId;

        public int getUserId() {
            return userId;
        }
    }

}
