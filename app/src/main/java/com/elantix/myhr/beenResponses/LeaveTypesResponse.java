package com.elantix.myhr.beenResponses;

/**
 * Created by misha on 28.01.2016.
 */
public class LeaveTypesResponse {


    private Data data[];

    public Data[] getData() {
        return data;
    }

    public class Data {
        int id;
        String leave_name;
        String unpaid="-1";
        String paid="-1";
        float quota_ballance;

        public float getQuota_ballance() {
            return quota_ballance;
        }

        public void setQuota_ballance(float quota_ballance) {
            this.quota_ballance = quota_ballance;
        }

        public String getPaid() {
            return paid;
        }

        public void setPaid(String paid) {
            this.paid = paid;
        }

        public String getUnpaid() {
            return unpaid;
        }

        public int getId() {
            return id;
        }

        public String getLeave_name() {
            return leave_name;
        }
    }

//    "data": [
//    {
//        "id": "1",
//            "name": "Angelina Phelps",
//            "leave_name": "Paid Annual Leave",
//            "unpaid": "0",
//            "start_date": "01/10/2015",
//            "end_date": "08/10/2015",
//            "reason": "Planed vacation",
//            "status": "1",
//            "statusLabel": "Pending"
//    },


}
