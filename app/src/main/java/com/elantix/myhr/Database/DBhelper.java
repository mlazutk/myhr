package com.elantix.myhr.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.elantix.myhr.L;
import com.elantix.myhr.activities.Utils;
import com.elantix.myhr.beenResponses.BreekTimeResponce;
import com.elantix.myhr.classes.Data;
import com.elantix.myhr.classes.Event;
import com.elantix.myhr.classes.TickEnd;
import com.elantix.myhr.classes.TickStart;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by misha on 26.02.2016.
 */
public class DBhelper extends SQLiteOpenHelper implements IDatabaseHandler {


    private static final String DATABASE_NAME = "MyHRdb";
    private static final int DATABASE_VERSION = 11;
    public static final String EMEETING_HISTORY_TABLE = "emeetinghistory";
    public static final String EMEETING_BREEK_TABLE = "emeeting_breek";
    public static final String EMEETING_START_DATE_TABLE = "emeeting_start_date";
    public static final String EMEETING_END_DATE_TABLE = "emeeting_end_date";
    public static final String HISTORY_TABLE = "historytable";

    public static final String EM_HIST_START_DAY_ID = "id";
    public static final String EM_HIST_START_DAY_DATE = "datetime";
    public static final String EM_HIST_START_DAY_STARTED = "started";

    public static final String EM_HIST_START_DAY_LAT = "lat";
    public static final String EM_HIST_START_DAY_LNG = "lng";
    public static final String EM_HIST_START_DAY_STATUS = "status";
    public static final String EM_HIST_START_DAY_ADRESS = "adress";
    public static final String EM_HIST_START_DAY_NOTE = "note";
    public static final String EM_HIST_START_DAY_EMPLOYEE_ID = "employee_id";


    public static final String EM_HIST_END_DAY_ID = "id";
    public static final String EM_HIST_END_DAY_DATE = "datetime";
    public static final String EM_HIST_END_DAY_FINISHED = "finished";
    public static final String EM_HIST_END_DAY_LAT = "lat";
    public static final String EM_HIST_END_DAY_LNG = "lng";
    public static final String EM_HIST_END_DAY_STATUS = "status";

    public static final String EM_HIST_END_DAY_ADRESS = "adress";
    public static final String EM_HIST_END_DAY_NOTE = "note";
    public static final String EM_HIST_END_DAY_EMPLOYEE_ID = "employee_id";


    public static final String EM_HIST_HISTORY_ID = "id";
    public static final String EM_HIST_HISTORY_DATE = "date";

    public static final String EM_HIST_BREEK_ID = "id";
    public static final String EM_HIST_BREEK_EMPLOYEE_ID = "employee_id";
    public static final String EM_HIST_BREEK_STARTED = "started_at";
    public static final String EM_HIST_BREEK_FINISHED = "finished_at";
    public static final String EM_HIST_BREEK_LAT = "lat";
    public static final String EM_HIST_BREEK_LNG = "lng";
    public static final String EM_HIST_BREEK_ADRESS = "adress";
    public static final String EM_HIST_BREEK_NOTE = "note";
    //================================================================== History Table============================
    public static final String EM_HIST_ID = "id";
    public static final String EM_HIST_EMPLOYEE_ID = "employee_id";
    public static final String EM_HIST_STARTED = "started_at";
    public static final String EM_HIST_FINISHED = "finished_at";
    public static final String EM_HIST_LAT = "lat";
    public static final String EM_HIST_LNG = "lng";
    public static final String EM_HIST_STATUS = "status";
    public static final String EM_HIST_ADRESS = "adress";
    public static final String EM_HIST_NOTE = "note";
    public static final String COLOR = "color";
    public static final String TITLE_EVENT = "title_event";
    //----------------------------------------------------------
    private static final String CREATE_HISTORY_TABLE = " CREATE TABLE " + HISTORY_TABLE + "(" +
            EM_HIST_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            EM_HIST_EMPLOYEE_ID + " varchar," +
            EM_HIST_STARTED + " varchar," +
            EM_HIST_FINISHED + " varchar," +
            EM_HIST_LAT + " varchar," +
            EM_HIST_LNG + " varchar," +
            EM_HIST_STATUS + " varchar," +
            EM_HIST_ADRESS + " varchar," +
            EM_HIST_NOTE + " varchar," +
            TITLE_EVENT + " varchar," +
            COLOR + " varchar" +
            ");";

    private static final String CREATE_TABLE_BREEK = " CREATE TABLE " + EMEETING_BREEK_TABLE + " (" +
            EM_HIST_BREEK_ID + " integer," +
            EM_HIST_BREEK_EMPLOYEE_ID + " varchar," +
            EM_HIST_BREEK_STARTED + " varchar," +
            EM_HIST_BREEK_FINISHED + " varchar," +
            EM_HIST_BREEK_LAT + " varchar," +
            EM_HIST_BREEK_LNG + " varchar," +
            EM_HIST_BREEK_ADRESS + " varchar," +
            EM_HIST_BREEK_NOTE + " varchar, " +
            COLOR + " varchar" +
            ");";


    private static final String CREATE_TABLE_START_DAY = " CREATE TABLE " + EMEETING_START_DATE_TABLE + "(\n" +
            EM_HIST_START_DAY_ID + " integer, " +
            EM_HIST_START_DAY_DATE + " varchar," +
            EM_HIST_START_DAY_LAT + " varchar," +
            EM_HIST_START_DAY_LNG + " varchar," +
            EM_HIST_START_DAY_STATUS + " varchar," +
            EM_HIST_START_DAY_ADRESS + " varchar, " +
            EM_HIST_START_DAY_NOTE + " varchar, " +
            EM_HIST_START_DAY_EMPLOYEE_ID + " varchar," +
            EM_HIST_START_DAY_STARTED + " varchar," +
            COLOR + " varchar" +
            ");";
    private static final String CREATE_TABLE_END_DAY = " CREATE TABLE " + EMEETING_END_DATE_TABLE + " (" +
            EM_HIST_END_DAY_ID + " integer," +
            EM_HIST_END_DAY_DATE + " varchar," +
            EM_HIST_END_DAY_LAT + " varchar," +
            EM_HIST_END_DAY_LNG + " varchar," +
            EM_HIST_END_DAY_STATUS + " varchar," +
            EM_HIST_END_DAY_ADRESS + " varchar," +
            EM_HIST_END_DAY_NOTE + " varchar," +
            EM_HIST_END_DAY_FINISHED + " varchar," +
            EM_HIST_END_DAY_EMPLOYEE_ID + " varchar," +
            COLOR + " varchar" +
            ");";


    private static final String CREATE_TABLE_MEETING_HISTORY =
            " CREATE TABLE " + EMEETING_HISTORY_TABLE + " (" +
                    EM_HIST_HISTORY_ID + " INTEGER PRIMARY KEY," +
                    EM_HIST_HISTORY_DATE + " TEXT " +
                    ");";


    Context context;

    public DBhelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_HISTORY_TABLE);
        // db.execSQL(CREATE_TABLE_MEETING_HISTORY);
        // db.execSQL(CREATE_TABLE_BREEK);
        // db.execSQL(CREATE_TABLE_START_DAY);
        // db.execSQL(CREATE_TABLE_END_DAY);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + HISTORY_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + EMEETING_HISTORY_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + EMEETING_BREEK_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + EMEETING_START_DATE_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + EMEETING_END_DATE_TABLE);

        // Create tables again
        onCreate(db);
    }


    @Override
    public void addTickStart(TickStart tickStart) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        L.i("addTickStart: ");
        L.i("EM_HIST_START_DAY_DATE: " + tickStart.getData().getDatetime());
        L.i("EM_HIST_START_DAY_LNG: " + tickStart.getData().getLat());
        L.i("EM_HIST_START_DAY_STATUS: " + tickStart.getData().getStatus());
        L.i("EM_HIST_START_DAY_ADRESS: " + tickStart.getData().getAddress());

        values.put(EM_HIST_START_DAY_EMPLOYEE_ID, String.valueOf(tickStart.getData().getEmployee_id()));
        values.put(EM_HIST_START_DAY_DATE, tickStart.getData().getDatetime());
        values.put(EM_HIST_START_DAY_STARTED, tickStart.getData().getStarted_at());
        values.put(EM_HIST_START_DAY_LAT, tickStart.getData().getLat());
        values.put(EM_HIST_START_DAY_LNG, tickStart.getData().getLng());
        values.put(EM_HIST_START_DAY_STATUS, tickStart.getData().getStatus());
        values.put(EM_HIST_START_DAY_ADRESS, tickStart.getData().getAddress());
        values.put(EM_HIST_START_DAY_NOTE, tickStart.getData().getNote());
        values.put(COLOR, "green");
        db.insert(EMEETING_START_DATE_TABLE, null, values);
        db.close();
    }

    @Override
    public void addEvent(Event event) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        L.i("addEvent: ");
        L.i("STARTED: " + event.getData().getStarted_at());
        L.i("FINISHED: " + event.getData().getFinished_at());
        L.i("LAT: " + event.getData().getLat());
        L.i("STATUS: " + event.getData().getStatus());
        L.i("ADRESS: " + event.getData().getAddress());
        L.i("TITLE_EVENT: " + event.getData().getTitle());
        L.i("COLOR: " + event.getData().getColor());


        values.put(EM_HIST_EMPLOYEE_ID, String.valueOf(event.getData().getEmployee_id()));
        // values.put(EM_HIST_START_DAY_DATE, event.getData().getDatetime());
        values.put(EM_HIST_STARTED, event.getData().getStarted_at());
        values.put(EM_HIST_FINISHED, event.getData().getFinished_at());
        values.put(EM_HIST_LAT, event.getData().getLat());
        values.put(EM_HIST_LNG, event.getData().getLng());
        values.put(EM_HIST_STATUS, event.getData().getStatus());
        values.put(EM_HIST_ADRESS, event.getData().getAddress());
        values.put(EM_HIST_NOTE, event.getData().getNote());
        values.put(TITLE_EVENT, event.getData().getTitle());
        values.put(COLOR, event.getData().getColor());
        long id = db.insert(HISTORY_TABLE, null, values);
        L.i("inserted row =  " + id);

        db.close();
    }

    @Override
    public List<Event> getListEvent(String employee_id) {
        List<Event> listEvent = new LinkedList<>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(HISTORY_TABLE, null, EM_HIST_EMPLOYEE_ID + "=?",
                new String[]{String.valueOf(employee_id)}, null, null, null, null);
        L.i("cursor count list event=  " + cursor.getCount());
        if (cursor != null && cursor.moveToFirst()) {
            do {
                Event event = new Event();
                Data data = event.getData();
                data.setEmployee_id(Integer.parseInt(cursor.getString(cursor.getColumnIndex(EM_HIST_EMPLOYEE_ID))));
                data.setStarted_at(cursor.getString(cursor.getColumnIndex(EM_HIST_STARTED)));
                data.setFinished_at(cursor.getString(cursor.getColumnIndex(EM_HIST_FINISHED)));
                data.setLat(cursor.getString(cursor.getColumnIndex(EM_HIST_LAT)));
                data.setLng(cursor.getString(cursor.getColumnIndex(EM_HIST_LNG)));
                data.setStatus(cursor.getString(cursor.getColumnIndex(EM_HIST_STATUS)));
                data.setAddress(cursor.getString(cursor.getColumnIndex(EM_HIST_ADRESS)));
                data.setNote(cursor.getString(cursor.getColumnIndex(EM_HIST_NOTE)));
                data.setColor(cursor.getString(cursor.getColumnIndex(COLOR)));
                data.setTitle(cursor.getString(cursor.getColumnIndex(TITLE_EVENT)));
                event.setData(data);
                listEvent.add(event);
            } while (cursor.moveToNext());
        }

        return listEvent;
    }

    @Override
    public void addTickEnd(TickEnd tickEnd) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(EM_HIST_END_DAY_EMPLOYEE_ID, tickEnd.getData().getEmployee_id());
        values.put(EM_HIST_END_DAY_DATE, tickEnd.getData().getDatetime());
        values.put(EM_HIST_END_DAY_FINISHED, tickEnd.getData().getFinished_at());
        values.put(EM_HIST_END_DAY_LAT, tickEnd.getData().getLat());
        values.put(EM_HIST_END_DAY_LNG, tickEnd.getData().getLng());
        values.put(EM_HIST_END_DAY_STATUS, tickEnd.getData().getStatus());
        values.put(EM_HIST_END_DAY_ADRESS, tickEnd.getData().getAddress());
        values.put(EM_HIST_END_DAY_NOTE, tickEnd.getData().getNote());
        values.put(COLOR, "red");
        db.insert(EMEETING_END_DATE_TABLE, null, values);
        db.close();
    }

    @Override
    public void addRecHistory(String today) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        //values.put(EM_HIST_HISTORY_ID, tickEnd.getData().getEmployee_id());
        values.put(EM_HIST_HISTORY_DATE, today);
        db.insert(EMEETING_HISTORY_TABLE, null, values);
        db.close();
    }

    @Override
    public TickStart getTickStart(String employee_id, String date) {
        SQLiteDatabase db = this.getReadableDatabase();
        date = date + "%";
        Cursor cursor = db.query(EMEETING_START_DATE_TABLE, null, EM_HIST_START_DAY_EMPLOYEE_ID +
                        "=?" + " and " + EM_HIST_START_DAY_DATE + " Like ?",
                new String[]{employee_id, date}, null, null, null, null);

        TickStart tickStart = null;
        if ((cursor != null) && cursor.moveToFirst()) {
            tickStart = new TickStart();
            Data data = tickStart.getData();
            data.setEmployee_id(Integer.parseInt(cursor.getString(cursor.getColumnIndex(EM_HIST_START_DAY_EMPLOYEE_ID))));
            data.setDatetime(cursor.getString(cursor.getColumnIndex(EM_HIST_START_DAY_DATE)));
            data.setLat(cursor.getString(cursor.getColumnIndex(EM_HIST_START_DAY_LAT)));
            data.setLng(cursor.getString(cursor.getColumnIndex(EM_HIST_START_DAY_LNG)));
            data.setStatus(cursor.getString(cursor.getColumnIndex(EM_HIST_START_DAY_STATUS)));
            data.setAddress(cursor.getString(cursor.getColumnIndex(EM_HIST_START_DAY_ADRESS)));
            data.setNote(cursor.getString(cursor.getColumnIndex(EM_HIST_START_DAY_NOTE)));
            data.setColor(cursor.getString(cursor.getColumnIndex(COLOR)));
            tickStart.setData(data);
        }
        return tickStart;
    }

    @Override
    public TickStart getTickStart(String employee_id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(EMEETING_START_DATE_TABLE, null, EM_HIST_START_DAY_EMPLOYEE_ID +
                "=?", new String[]{employee_id}, null, null, null, null);

        TickStart tickStart = null;
        if ((cursor != null) && cursor.moveToFirst()) {
            tickStart = new TickStart();
            Data data = tickStart.getData();
            data.setEmployee_id(Integer.parseInt(cursor.getString(cursor.getColumnIndex(EM_HIST_START_DAY_EMPLOYEE_ID))));
            data.setDatetime(cursor.getString(cursor.getColumnIndex(EM_HIST_START_DAY_DATE)));
            data.setLat(cursor.getString(cursor.getColumnIndex(EM_HIST_START_DAY_LAT)));
            data.setLng(cursor.getString(cursor.getColumnIndex(EM_HIST_START_DAY_LNG)));
            data.setStatus(cursor.getString(cursor.getColumnIndex(EM_HIST_START_DAY_STATUS)));
            data.setAddress(cursor.getString(cursor.getColumnIndex(EM_HIST_START_DAY_ADRESS)));
            data.setNote(cursor.getString(cursor.getColumnIndex(EM_HIST_START_DAY_NOTE)));
            data.setColor(cursor.getString(cursor.getColumnIndex(COLOR)));
            tickStart.setData(data);
        }
        return tickStart;
    }


    @Override
    public TickEnd getTickEnd(String employee_id, String date) {
        SQLiteDatabase db = this.getReadableDatabase();
        date = date + "%";
        Cursor cursor = db.query(EMEETING_END_DATE_TABLE, null, EM_HIST_END_DAY_EMPLOYEE_ID +
                        "=?" + " and " + EM_HIST_END_DAY_DATE + " Like ?",
                new String[]{employee_id, date}, null, null, null, null);
        TickEnd tickEnd = null;
        if ((cursor != null) && cursor.moveToFirst()) {
            tickEnd = new TickEnd();
            Data data = tickEnd.getData();
            data.setEmployee_id(Integer.parseInt(cursor.getString(cursor.getColumnIndex(EM_HIST_END_DAY_EMPLOYEE_ID))));
            data.setDatetime(cursor.getString(cursor.getColumnIndex(EM_HIST_END_DAY_DATE)));
            data.setLat(cursor.getString(cursor.getColumnIndex(EM_HIST_END_DAY_LAT)));
            data.setLng(cursor.getString(cursor.getColumnIndex(EM_HIST_END_DAY_LNG)));
            data.setStatus(cursor.getString(cursor.getColumnIndex(EM_HIST_END_DAY_STATUS)));
            data.setAddress(cursor.getString(cursor.getColumnIndex(EM_HIST_END_DAY_ADRESS)));
            data.setNote(cursor.getString(cursor.getColumnIndex(EM_HIST_END_DAY_NOTE)));
            data.setNote(cursor.getString(cursor.getColumnIndex(EM_HIST_END_DAY_NOTE)));
            data.setColor(cursor.getString(cursor.getColumnIndex(COLOR)));
            //cursor.close();
            tickEnd.setData(data);
        }
        return tickEnd;
    }


    @Override
    public void updateEvent(Event event) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
//        data.setStarted_at(cursor.getString(cursor.getColumnIndex(EM_HIST_STARTED)));
//        data.setFinished_at(cursor.getString(cursor.getColumnIndex(EM_HIST_FINISHED)));
//        data.setLat(cursor.getString(cursor.getColumnIndex(EM_HIST_LAT)));
//        data.setLng(cursor.getString(cursor.getColumnIndex(EM_HIST_LNG)));
//        data.setStatus(cursor.getString(cursor.getColumnIndex(EM_HIST_STATUS)));
//        data.setAddress(cursor.getString(cursor.getColumnIndex(EM_HIST_ADRESS)));
//        data.setNote(cursor.getString(cursor.getColumnIndex(EM_HIST_NOTE)));
//        data.setColor(cursor.getString(cursor.getColumnIndex(COLOR)));
//        data.setTitle(cursor.getString(cursor.getColumnIndex(TITLE_EVENT)));

        values.put(EM_HIST_FINISHED, event.getData().getFinished_at());
        // if(event.getData().getTitle().equalsIgnoreCase(Utils.TITLE_BREEK)){
        values.put(EM_HIST_STATUS, event.getData().getStatus());


        values.put(EM_HIST_NOTE, event.getData().getNote());
        Cursor cursor = db.query(HISTORY_TABLE, null, null,
                null, null, null, null, null);

        int updCount = db.update(HISTORY_TABLE, values, "id = ?",
                new String[]{String.valueOf(cursor.getCount())});
        L.i("updCount = " + updCount);
        db.close();
    }

    @Override
    public Event getLastRow(String employee_id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(HISTORY_TABLE, null, null, null, null, null, null, null);
        Event event = null;
        String title = null;
        if ((cursor != null) && cursor.moveToFirst()) {
            cursor.moveToLast();
            title = cursor.getString(cursor.getColumnIndex(TITLE_EVENT));
            L.i("title = " + title);
            event = new Event();
            Data data = event.getData();
            data.setEmployee_id(Integer.parseInt(cursor.getString(cursor.getColumnIndex(EM_HIST_EMPLOYEE_ID))));
            data.setStarted_at(cursor.getString(cursor.getColumnIndex(EM_HIST_STARTED)));
            data.setFinished_at(cursor.getString(cursor.getColumnIndex(EM_HIST_FINISHED)));
            data.setLat(cursor.getString(cursor.getColumnIndex(EM_HIST_LAT)));
            data.setLng(cursor.getString(cursor.getColumnIndex(EM_HIST_LNG)));
            data.setStatus(cursor.getString(cursor.getColumnIndex(EM_HIST_STATUS)));
            data.setAddress(cursor.getString(cursor.getColumnIndex(EM_HIST_ADRESS)));
            data.setNote(cursor.getString(cursor.getColumnIndex(EM_HIST_NOTE)));
            data.setColor(cursor.getString(cursor.getColumnIndex(COLOR)));
            data.setTitle(cursor.getString(cursor.getColumnIndex(TITLE_EVENT)));
            event.setData(data);
        }
        return event;
    }

    @Override
    public Event getEvent(int position) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(HISTORY_TABLE, null, EM_HIST_ID +
                        "=?",
                new String[]{String.valueOf(position)}, null, null, null, null);
        Event event = null;
        L.i("cursor = " + cursor);
        L.i("cursor count= " + cursor.getCount());
        if ((cursor != null) && cursor.moveToFirst()) {
            event = new Event();
            Data data = event.getData();
            data.setEmployee_id(Integer.parseInt(cursor.getString(cursor.getColumnIndex(EM_HIST_EMPLOYEE_ID))));
            data.setStarted_at(cursor.getString(cursor.getColumnIndex(EM_HIST_STARTED)));
            data.setFinished_at(cursor.getString(cursor.getColumnIndex(EM_HIST_FINISHED)));
            data.setLat(cursor.getString(cursor.getColumnIndex(EM_HIST_LAT)));
            data.setLng(cursor.getString(cursor.getColumnIndex(EM_HIST_LNG)));
            data.setStatus(cursor.getString(cursor.getColumnIndex(EM_HIST_STATUS)));
            data.setAddress(cursor.getString(cursor.getColumnIndex(EM_HIST_ADRESS)));
            data.setNote(cursor.getString(cursor.getColumnIndex(EM_HIST_NOTE)));
            data.setColor(cursor.getString(cursor.getColumnIndex(COLOR)));
            data.setTitle(cursor.getString(cursor.getColumnIndex(TITLE_EVENT)));
            event.setData(data);
        }
        return event;
    }

    @Override
    public TickEnd getTickEnd(String employee_id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(EMEETING_END_DATE_TABLE, null, EM_HIST_END_DAY_EMPLOYEE_ID +
                        "=?",
                new String[]{employee_id}, null, null, null, null);
        TickEnd tickEnd = null;
        if ((cursor != null) && cursor.moveToFirst()) {
            tickEnd = new TickEnd();
            Data data = tickEnd.getData();
            data.setEmployee_id(Integer.parseInt(cursor.getString(cursor.getColumnIndex(EM_HIST_END_DAY_EMPLOYEE_ID))));
            data.setDatetime(cursor.getString(cursor.getColumnIndex(EM_HIST_END_DAY_DATE)));
            data.setLat(cursor.getString(cursor.getColumnIndex(EM_HIST_END_DAY_LAT)));
            data.setLng(cursor.getString(cursor.getColumnIndex(EM_HIST_END_DAY_LNG)));
            data.setStatus(cursor.getString(cursor.getColumnIndex(EM_HIST_END_DAY_STATUS)));
            data.setAddress(cursor.getString(cursor.getColumnIndex(EM_HIST_END_DAY_ADRESS)));
            data.setNote(cursor.getString(cursor.getColumnIndex(EM_HIST_END_DAY_NOTE)));
            data.setNote(cursor.getString(cursor.getColumnIndex(EM_HIST_END_DAY_NOTE)));
            data.setColor(cursor.getString(cursor.getColumnIndex(COLOR)));
            //cursor.close();
            tickEnd.setData(data);
        }
        return tickEnd;
    }


    @Override
    public List<BreekTimeResponce> getAllBreekTime() {
        return null;
    }

    @Override
    public List<String> getHistoryDate() {
        List<String> historyList = new LinkedList<>();
        String selectQuery = "SELECT  * FROM " + EMEETING_HISTORY_TABLE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                historyList.add(cursor.getString(cursor.getColumnIndex(EM_HIST_HISTORY_DATE)));
            } while (cursor.moveToNext());
        }

        return historyList;
    }

    @Override
    public void deleteAll() {

    }


}



