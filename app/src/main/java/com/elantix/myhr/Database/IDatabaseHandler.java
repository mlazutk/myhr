package com.elantix.myhr.Database;

import com.elantix.myhr.beenResponses.BreekTimeResponce;
import com.elantix.myhr.classes.Event;
import com.elantix.myhr.classes.TickEnd;
import com.elantix.myhr.classes.TickStart;

import java.util.List;

/**
 * Created by misha on 27.02.2016.
 */
public interface IDatabaseHandler {
    public void addTickStart(TickStart tickStart);

    public void addTickEnd(TickEnd tickEnd);

    public TickStart getTickStart(String empl_id, String date);

    public TickEnd getTickEnd(String empl_id, String date);

    public List<String> getHistoryDate();

    public void addRecHistory(String today);

    public TickStart getTickStart(String employee_id);

    public TickEnd getTickEnd(String employee_id);

    public List<Event> getListEvent(String employee_id);

    public void addEvent(Event eventData);

    public Event getEvent(int pos);

    public void updateEvent(Event event);

    public Event getLastRow(String employee_id);

    public List<BreekTimeResponce> getAllBreekTime();

    //    public int getContactsCount();
//    public int updateBreekTime(Contact contact);
//    public void deleteBreekTime(Contact contact);
    public void deleteAll();
}