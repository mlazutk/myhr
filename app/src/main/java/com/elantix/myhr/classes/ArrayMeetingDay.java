package com.elantix.myhr.classes;

import java.util.HashMap;

/**
 * Created by misha on 25.02.2016.
 */
public class ArrayMeetingDay {
    private HashMap<String, MeetingDay> mArrayMeetingDay;


    public HashMap<String, MeetingDay> getMeetingDay() {
        return mArrayMeetingDay;
    }

    public void setMeetingDay(HashMap<String, MeetingDay> meetingDay) {
        mArrayMeetingDay = meetingDay;
    }
}
