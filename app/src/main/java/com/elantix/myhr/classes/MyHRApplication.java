package com.elantix.myhr.classes;

import android.app.Application;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.res.Configuration;

import com.elantix.myhr.L;


public class MyHRApplication extends Application implements ComponentCallbacks2 {

    @Override
    public void onCreate() {
        super.onCreate();
        L.d("PMIApplication created");
        init();
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        L.d("level ==  " + level);
        if ((level == TRIM_MEMORY_UI_HIDDEN) || ((level == TRIM_MEMORY_COMPLETE))) {
            //1 - not hidden,not blocked
            //2 - not hidden,blocked
            //3 - hidden,not blocked
            //4 - hidden,blocked

        }
    }


    private void init() {
        Context applicationContext = getApplicationContext();


    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }


}
