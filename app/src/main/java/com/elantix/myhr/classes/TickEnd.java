package com.elantix.myhr.classes;

/**
 * Created by misha on 24.02.2016.
 */
public class TickEnd {

    private boolean status;
    public Data data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Data getData() {
        if (data == null) data = new Data();
        return data;
    }


}

