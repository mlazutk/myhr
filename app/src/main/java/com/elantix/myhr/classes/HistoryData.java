package com.elantix.myhr.classes;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by misha on 27.02.2016.
 */
public class HistoryData {
    private boolean status;
    private List<Data> data = new LinkedList<>();

    public boolean isStatus() {
        return status;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }
}
