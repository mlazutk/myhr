package com.elantix.myhr.classes;

/**
 * Created by misha on 27.02.2016.
 */
public class Data {
    private int id;
    private int employee_id;
    private String started_at;
    private String finished_at;
    private String datetime;
    private String lat;
    private String lng;
    private String status;
    private String address;
    private String note;
    private String color;
    private String identificator;
    private String title;

    @Override
    public String toString() {
        return "Data{" +
                "id=" + id +
                ", employee_id=" + employee_id +
                ", started_at='" + started_at + '\'' +
                ", finished_at='" + finished_at + '\'' +
                ", datetime='" + datetime + '\'' +
                ", lat='" + lat + '\'' +
                ", lng='" + lng + '\'' +
                ", status='" + status + '\'' +
                ", address='" + address + '\'' +
                ", note='" + note + '\'' +
                ", color='" + color + '\'' +
                ", identificator='" + identificator + '\'' +
                ", title='" + title + '\'' +
                '}';
    }

    public String getIdentificator() {
        return identificator;
    }

    public void setIdentificator(String identificator) {
        this.identificator = identificator;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String type) {
        this.title = type;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setEmployee_id(int employee_id) {
        this.employee_id = employee_id;
    }

    public void setStarted_at(String started_at) {
        this.started_at = started_at;
    }

    public void setFinished_at(String finished_at) {
        this.finished_at = finished_at;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getId() {
        return id;
    }

    public int getEmployee_id() {
        return employee_id;
    }

    public String getStarted_at() {
        return started_at;
    }

    public String getFinished_at() {
        return finished_at;
    }

    public String getLat() {
        return lat;
    }

    public String getLng() {
        return lng;
    }

    public String getStatus() {
        return status;
    }

    public String getAddress() {
        return address;
    }

    public String getNote() {
        return note;
    }
}