package com.elantix.myhr.classes;

/**
 * Created by misha on 25.02.2016.
 */

import android.content.Context;

import com.elantix.myhr.L;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class JsonUtils {
    public static JSONObject mapToJson(Map<?, ?> data) {
        JSONObject object = new JSONObject();

        for (Map.Entry<?, ?> entry : data.entrySet()) {
            /*
             * Deviate from the original by checking that keys are non-null and
             * of the proper type. (We still defer validating the values).
             */
            String key = (String) entry.getKey();
            if (key == null) {
                throw new NullPointerException("key == null");
            }
            try {
                object.put(key, entry.getValue());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return object;
    }

    public static MeetingDay getMeetingDay(Context context, String id_employee) {
        Gson gson = new Gson();
        HashMap<String, String> rhm = MyHRPreferences.getInstance(context).getMapMeetingDays();
        L.i("rhm = " + rhm.toString());
        MeetingDay meetingDay = null;
        for (Map.Entry<?, ?> entry : rhm.entrySet()) {
            String key = (String) entry.getKey();
            String value = (String) entry.getValue();
            if (key.equalsIgnoreCase(id_employee)) {
                meetingDay = gson.fromJson(value, MeetingDay.class);
            }
            // L.i("rhm = " + rhm.toString());
            L.i("getMeetingDay:key = " + key);
            L.i("getMeetingDay:value = " + value);
        }
        return meetingDay;
    }


    public static void setMeetingDay(Context context, MeetingDay meetingDay, String id_employee) {
        Gson gson = new Gson();
        HashMap<String, String> previousHm = MyHRPreferences.getInstance(context).getMapMeetingDays();
        String previousHmString = gson.toJson(previousHm);
        L.i("previousHmString before set new= " + previousHmString);
        previousHm.put(id_employee, gson.toJson(meetingDay));


        L.i("previousHmString after set new= " + previousHmString);
        //===========================================
        MyHRPreferences.getInstance(context).storeMapMeetingDays(previousHm);
    }

    public static void clearAllMeetingDatas(Context context) {
        MyHRPreferences.getInstance(context).storeMapMeetingDays(null);

    }


}