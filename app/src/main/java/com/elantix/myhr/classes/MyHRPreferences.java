package com.elantix.myhr.classes;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import com.elantix.myhr.L;
import com.elantix.myhr.beenResponses.AuthorizedUser;

import java.util.HashMap;

/**
 * Created by misha on 18.02.2016.
 */
public class MyHRPreferences {
    public static final String AUTHORIZED_USER = "authorized_user";
    public static final String MEETING_DAY = "meeting_day";
    public static final String ARRAY_MEETING_DAY = "array_meeting_day";
    public static final String CURRENT_DATE = "current_date";
    private static MyHRPreferences sHRPreferences;
    private SharedPreferences mSharedPreferences;
    private static final String PREFERENCES_FILE = "myhr_pref";


    private MyHRPreferences(Context context) {
        mSharedPreferences = context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
    }

    public static MyHRPreferences getInstance(Context context) {
        if (sHRPreferences == null) {
            sHRPreferences = new MyHRPreferences(context);
        }
        return sHRPreferences;
    }

    public void storeUser(AuthorizedUser user) {
        Gson gson = new Gson();
        String jsonUser = gson.toJson(user);
        L.i("jsonUser stored = " + jsonUser);
        mSharedPreferences.edit()
                .putString(AUTHORIZED_USER, jsonUser)
                .apply();
    }

    public AuthorizedUser getUser() {
        Gson gson = new Gson();
        String jsonUser = mSharedPreferences.getString(AUTHORIZED_USER, null);
        AuthorizedUser user = gson.fromJson(jsonUser, AuthorizedUser.class);
        return user;
    }

//    public void storeMeetingDay(MeetingDay meetingDay) {
//        Gson gson = new Gson();
//        String jsonMeetingDay = gson.toJson(meetingDay);
//        L.i("jsonMeetingDay stored = " + jsonMeetingDay);
//        mSharedPreferences.edit()
//                .putString(MEETING_DAY, jsonMeetingDay)
//                .apply();
//    }

    public HashMap<String, String> getMapMeetingDays() {
        Gson gson = new Gson();
        String jsonMeetingDay = mSharedPreferences.getString(ARRAY_MEETING_DAY, null);
        HashMap<String, String> arrmeetingDay = gson.fromJson(jsonMeetingDay, HashMap.class);
        if (arrmeetingDay == null) arrmeetingDay = new HashMap<String, String>();
        return arrmeetingDay;
    }

    public void storeMapMeetingDays(HashMap<String, String> hm) {
        Gson gson = new Gson();
        String jsonMapMeetingDays = gson.toJson(hm);
        L.i("jsonArrayMeetingDay stored = " + jsonMapMeetingDays);
        mSharedPreferences.edit()
                .putString(ARRAY_MEETING_DAY, jsonMapMeetingDays)
                .apply();
    }


//    public MeetingDay getMeetingDay() {
//        Gson gson = new Gson();
//        String jsonMeetingDay = mSharedPreferences.getString(MEETING_DAY, null);
//        MeetingDay meetingDay = gson.fromJson(jsonMeetingDay, MeetingDay.class);
//        return meetingDay;
//    }

    public void storeDate(String date) {
        mSharedPreferences.edit()
                .putString(CURRENT_DATE, date)
                .apply();
    }

    public String getDate() {
        String date = mSharedPreferences.getString(CURRENT_DATE, "");
        return date;
    }


}
