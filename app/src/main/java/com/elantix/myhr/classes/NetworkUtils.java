package com.elantix.myhr.classes;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkUtils {

    public static boolean isOn(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        boolean isOn = (networkInfo != null && networkInfo.isConnected());
//        if (!isOn) {
//            Toast.makeText(context, "Network Connection Error", Toast.LENGTH_SHORT).show();
//        }
        return isOn;
    }

    public static boolean isWIFIOn(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        boolean isWIFIOn = (networkInfo != null && networkInfo.isConnected());
//        if (!isWIFIOn) {
//            Toast.makeText(context, "WIFI Connection Error", Toast.LENGTH_SHORT).show();
//        }
        return isWIFIOn;
    }

    public static boolean isMobileOn(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        boolean isMobileOn = (networkInfo != null && networkInfo.isConnected());
//        if (!isMobileOn) {
//            Toast.makeText(context, "Mobile Network Connection Error", Toast.LENGTH_SHORT).show();
//        }
        return isMobileOn;
    }
}
