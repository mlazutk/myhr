package com.elantix.myhr.classes;

import com.elantix.myhr.beenResponses.BreektimeStartResponce;
import com.elantix.myhr.beenResponses.StartMeetingResponce;

import java.util.ArrayList;

/**
 * Created by misha on 23.02.2016.
 */
public class MeetingDay {

    private TickStart mTickStart;
    private TickEnd mTickEnd;
    private ArrayList<BreektimeStartResponce> mBreektimeStartResponce;
    private StartMeetingResponce[] mStartMeetingResponces;

    public void setTickTodayStart(TickStart tickStart) {
        mTickStart = tickStart;
    }

    public void setTickEnd(TickEnd tickEnd) {
        mTickEnd = tickEnd;
    }

    public TickStart getTickTodayStart() {
        return mTickStart;
    }

    public TickEnd getTickEnd() {
        return mTickEnd;
    }

    public ArrayList<BreektimeStartResponce> getBreektimeStartResponce() {
        return mBreektimeStartResponce;
    }

    public void setBreektimeStartResponce(ArrayList<BreektimeStartResponce> breektimeStartResponce) {
        mBreektimeStartResponce = breektimeStartResponce;
    }

    public StartMeetingResponce[] getStartMeetingResponces() {
        return mStartMeetingResponces;
    }

    public void setStartMeetingResponces(StartMeetingResponce[] startMeetingResponces) {
        mStartMeetingResponces = startMeetingResponces;
    }
}
