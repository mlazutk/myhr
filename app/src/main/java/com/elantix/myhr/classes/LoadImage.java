package com.elantix.myhr.classes;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import com.elantix.myhr.L;

import java.io.InputStream;
import java.net.URL;

/**
 * Created by misha on 23.03.2016.
 */
public class LoadImage extends AsyncTask<String, String, Bitmap> {
    public ImageView mImageView;
    Bitmap bitmap;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    public LoadImage(ImageView imageView) {
        mImageView = imageView;
    }

    protected Bitmap doInBackground(String... args) {
        try {
           // bitmap = BitmapFactory.decodeStream((InputStream) new URL(args[0]).getContent());
            bitmap = BitmapUtils.loadBitmap(args[0]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    protected void onPostExecute(Bitmap image) {
        L.i("onPostExecute  " );
        if (image != null) {
            mImageView.setImageBitmap(image);
        } else {
            L.i("Bitmap = " + image);
        }
    }
}
