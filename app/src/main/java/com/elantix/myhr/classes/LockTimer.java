package com.elantix.myhr.classes;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.elantix.myhr.L;
import com.elantix.myhr.activities.MeetingFragment;
import com.elantix.myhr.activities.TimesheetFragment;
import com.elantix.myhr.activities.Utils;

public class LockTimer {
    public static LockTimer sLockTimer;

    private Handler mLockHandler;
    private Runnable mLockRunnable;
    private Context mContext;

    private LockTimer(Context context) {
        mContext = context;
        mLockHandler = new Handler() {
            public void handleMessage(Message msg) {
            }
        };
        mLockRunnable = new Runnable() {
            @Override
            public void run() {
                if (mContext == null)
                    return;
                TimesheetFragment.setCurrentTimeHeader();
                MeetingFragment.setCurrentTimeHeader();
                //repeat
                reset();
            }
        };
    }

    public static LockTimer getInstance(Context context) {
        if (sLockTimer == null) {
            sLockTimer = new LockTimer(context);
        }
        return sLockTimer;
    }

    public void reset() {
        mLockHandler.removeCallbacks(mLockRunnable);
        mLockHandler.postDelayed(mLockRunnable, 15 * 1 * 1000);
    }

    public void stop() {
        mLockHandler.removeCallbacks(mLockRunnable);
    }
}
